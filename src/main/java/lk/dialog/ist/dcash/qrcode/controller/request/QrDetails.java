package lk.dialog.ist.dcash.qrcode.controller.request;

public class QrDetails {
	String merchantName;
	String dateFrom;
	String dateTo;
	
	public QrDetails() {
		super();
	}
	public QrDetails(String merchantId, String dateFrom, String dateTo) {
		super();
		this.merchantName = merchantId;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
	}
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantId) {
		this.merchantName = merchantId;
	}
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	
	
}
