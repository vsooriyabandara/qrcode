package lk.dialog.ist.dcash.qrcode.controller.request;

public class GetQRRequest {

	private String merchantName=null;
	
	private String status=null;
	
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "GetQRRequest [merchantName=" + merchantName + ", status=" + status
				+ "]";
	}
	
	
}
