package lk.dialog.ist.dcash.qrcode.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import lk.dialog.ist.dcash.qrcode.service.impl.CustomBasicAuthenticationFilter;
import lk.dialog.ist.dcash.qrcode.service.impl.TokenAuthenticationFilter;

@Configuration
@EnableWebSecurity
@ComponentScan({"lk.dialog.ist.dcash.qrcode.service.impl"})
public class SecurityConfig extends WebSecurityConfigurerAdapter
{

	@Autowired
    private UserDetailsService participantService;

   
    
    @Bean
    public UserDetailsService userDetailsService() {
        return super.userDetailsService();
    }
  
	@Override
    public void configure(final AuthenticationManagerBuilder auth) throws Exception
    {
		
        auth.userDetailsService(this.participantService);
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception
    {
    	 http.csrf().disable();
        //Implementing Token based authentication in this filter
        final TokenAuthenticationFilter tokenFilter = new TokenAuthenticationFilter();
        http.addFilterBefore(tokenFilter, BasicAuthenticationFilter.class);

        //Creating token when basic authentication is successful and the same token can be used to authenticate for further requests
        final CustomBasicAuthenticationFilter customBasicAuthFilter = new CustomBasicAuthenticationFilter(this.authenticationManager() );
        http.addFilter(customBasicAuthFilter);

    }
   
}