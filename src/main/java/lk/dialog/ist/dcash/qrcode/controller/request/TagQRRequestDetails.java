package lk.dialog.ist.dcash.qrcode.controller.request;

public class TagQRRequestDetails {
	String qr_serial;
	String tag;
	
	public TagQRRequestDetails() {
		super();
	}
	
	public TagQRRequestDetails(String qr_serial, String tag) {
		super();
		this.qr_serial = qr_serial;
		this.tag = tag;
	}
	
	public String getQr_serial() {
		return qr_serial;
	}
	public void setQr_serial(String qr_serial) {
		this.qr_serial = qr_serial;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	
	
}
