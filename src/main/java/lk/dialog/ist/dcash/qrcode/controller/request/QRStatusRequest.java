package lk.dialog.ist.dcash.qrcode.controller.request;

import java.util.Date;

public class QRStatusRequest {
	
	private String merchantID = null;
	private String qrID = null;
	private Date modifiedDate;
	private String modifiedBy = null;
	private String modifiedReason = null;
	private String qrType = null;
	private String status = null;
		
	public String getMerchantID() {
		return merchantID;
	}
	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}
	public String getQrID() {
		return qrID;
	}
	public void setQrID(String qrID) {
		this.qrID = qrID;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getModifiedReason() {
		return modifiedReason;
	}
	public void setModifiedReason(String modifiedReason) {
		this.modifiedReason = modifiedReason;
	}
	public String getQrType() {
		return qrType;
	}
	public void setQrType(String qrType) {
		this.qrType = qrType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return "Request [qrID=" + qrID + ", merchantID=" + merchantID + ", qrType=" + qrType + ", status=" + status
				+ ", modifiedDate=" + modifiedDate + ", modifiedBy=" + modifiedBy + ", modifiedReason=" + modifiedReason
				+ "]";
	}

}
