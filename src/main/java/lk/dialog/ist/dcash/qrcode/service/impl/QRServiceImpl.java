package lk.dialog.ist.dcash.qrcode.service.impl;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.IOUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.xml.sax.InputSource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import lk.dialog.ist.alipay.config.AlipayConfig;
import lk.dialog.ist.alipay.util.AliPayRequest;
import lk.dialog.ist.alipay.util.AlipaySubmit;
import lk.dialog.ist.dcash.qrcode.controller.request.BulkQrRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.DisableQrRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.GetQRRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.QRStatusRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.QrDetails;
import lk.dialog.ist.dcash.qrcode.controller.request.QueryQRCodesRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.Request;
import lk.dialog.ist.dcash.qrcode.controller.request.TagQRRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.TagQRRequestDetails;
import lk.dialog.ist.dcash.qrcode.controller.request.TagQRtoApp;
import lk.dialog.ist.dcash.qrcode.controller.request.ValidateQRRequest;
import lk.dialog.ist.dcash.qrcode.controller.response.BulkQRResponse;
import lk.dialog.ist.dcash.qrcode.controller.response.QRCodeResponse;
import lk.dialog.ist.dcash.qrcode.controller.response.QueryQRCodes;
import lk.dialog.ist.dcash.qrcode.controller.response.QueryQRCodesResponse;
import lk.dialog.ist.dcash.qrcode.controller.response.ResponseData;
import lk.dialog.ist.dcash.qrcode.controller.response.ResponseDetails;
import lk.dialog.ist.dcash.qrcode.controller.response.ResponseObject;
import lk.dialog.ist.dcash.qrcode.controller.response.TagQRErrorResponse;
import lk.dialog.ist.dcash.qrcode.controller.response.TagQrResponse;
import lk.dialog.ist.dcash.qrcode.controller.response.TagQrResponseDetails;
import lk.dialog.ist.dcash.qrcode.db.modal.QRApplication;
import lk.dialog.ist.dcash.qrcode.db.modal.QRCode;
import lk.dialog.ist.dcash.qrcode.db.modal.QRMerchant;
import lk.dialog.ist.dcash.qrcode.db.repository.ApplicationRepository;
import lk.dialog.ist.dcash.qrcode.db.repository.MerchantRepository;
import lk.dialog.ist.dcash.qrcode.db.repository.QRCodeRepository;
import lk.dialog.ist.dcash.qrcode.service.QRService;
import lk.dialog.ist.dcash.qrcode.util.MerchantStatus;
import lk.dialog.ist.dcash.qrcode.util.MerchantType;
/*import lk.dialog.ist.dcash.qrcode.util.QRCategory;*/
import lk.dialog.ist.dcash.qrcode.util.QRStatus;
import lk.dialog.ist.dcash.qrcode.util.QRType;
import lk.dialog.ist.dcash.qrcode.util.QrImageProcessor2;



@Service
public class QRServiceImpl implements QRService {

	@Autowired
	QRCodeRepository qrRepository;

	@Autowired
	MerchantRepository merchantRepository;

	@Autowired
	QrImageProcessor2 qrImageProcessor;

	@Autowired
	ApplicationRepository appRepository;

	protected RestTemplate restTemplate;

	protected static ResourceBundle resourse = ResourceBundle.getBundle("data");

	private Logger logger = LoggerFactory.getLogger(getClass());

	private static final String FAILED = "failed";
	private static final String DESCRIPTION = "description";
	private static final String CODE = "code";
	private static final String SUCCESS = "success";
	private static final String INVALIDMERCHANTCODE = "error.InvalidMerchantID.code";
	private static final String INVALIDMERCHANDID = "error.InvalidMerchantID";
	
	private static DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Transactional
	@Override
	public ResponseObject addStaticQR(Request request, int size) {
		long startTime = System.currentTimeMillis();
		logger.info("QRServiceImpl Service STARTED addStaticQR | "
				+ request.toString());
		ResponseObject res = new ResponseObject();
		ResponseDetails resDetails = new ResponseDetails();
		String json="";
		try {
			QRMerchant merchant = merchantRepository.getMerchant(request
					.getMerchantID());
			if (null == merchant) {
				res.setStatus(resourse.getString(FAILED));
				resDetails.setCode(resourse.getString(INVALIDMERCHANTCODE));
				resDetails.setDescription(resourse.getString(INVALIDMERCHANDID));
				res.setMessage(resDetails);
				res.setData(null);
			} else {
				if (null == merchant.getLogo()) {
					request.setLogoRequired(false);
				}
				// check same content or not
				if (request.getQrType().equals("Auto Generated") || request.getQrType().equals("Alipay") ) {
					request.setContent("-");
					
				}
				List<QRCode> qr = qrRepository.checkMerchantQRContent(
						request.getMerchantID(), request.getContent());
				if (qr.isEmpty()) {
					try {
						int myAlias=	Integer.parseInt(request.geteZCashAlias());
							request.setQrAlias(myAlias+"");
						}
						catch(NumberFormatException e) {
							logger.debug("Alias is not a mobile number"+request.geteZCashAlias());
						}
					QRCode qrCode = new QRCode(request);
					DateFormat dateFormat = new SimpleDateFormat(
							"yyyy/MM/dd HH:mm:ss");
					Date date = new Date();
					qrCode.setAddedDate(dateFormat.parse(dateFormat
							.format(date)));
					// setting default in to active state
					qrCode.setStatus(QRStatus.valueOf(request.getStatus())
							.ordinal());
					request.setQrAlias(request.getQrAlias().trim());
					qrCode.seteZCashAlias(request.geteZCashAlias()!=null?request.geteZCashAlias():"-");
					
					
					// if qrCategory is Alipay need to generate QR from external ALI Pay URL
					String encodedString = "";
					byte[] decodedImg = null ;
					logger.info(" QRServiceImpl |addStaticQR | request.getQrType() "+request.getQrType());
					if((request.getQrType()!=null &&  request.getQrType().equalsIgnoreCase(QRType.ALIPAY.toString()))){
						// Need to get Merchant details to submit AliPay URL
						QRMerchant qrMerchant = merchantRepository.getMerchant(request.getMerchantID());
						if(qrMerchant!=null) {
						AliPayRequest aliPayRequest = new AliPayRequest();
						logger.debug("setStoreId "+ request.getStoreId() + " setStoreName  "+request.getStoreName() + "  qrType "+request.getQrType());
					
						
							aliPayRequest.setSecondaryMerchantName(request.getStoreName());
						
						aliPayRequest.setSecondaryMerchantId(qrMerchant.getMerchantId());
						aliPayRequest.setSecondaryMerchantIndustry(MerchantType.values()[qrMerchant.getType()].getDescription()); // no need to get merchant industry form user. It gets from Merchant
						aliPayRequest.setStoreId(request.getStoreId());
						aliPayRequest.setStoreName(request.getStoreName());
						aliPayRequest.setTransCurrency("USD");
						aliPayRequest.setCurrency("USD");
						aliPayRequest.setCountryCode("SL");
						aliPayRequest.setAddress(qrMerchant.getAddress());
						ObjectMapper msp = new ObjectMapper();
						String jsonString = msp.writeValueAsString(aliPayRequest);
						/*String biz_data = new String(
								"{'secondary_merchant_name':'EZCash','secondary_merchant_id':'MCM00012','secondary_merchant_industry':'5812',"
								+ "'store_id':'000000001','store_name':'Dialog-Parkland','trans_currency':'USD','currency':'USD',"
								+ "'country_code':'SL','address':'Dialog-Parkland'}");
						*/
							
						
								String biz_type = new String("OVERSEASHOPQRCODE");
								Map<String, String> sParaTemp = new HashMap<String, String>();
								sParaTemp.put("service", "alipay.commerce.qrcode.create");
								sParaTemp.put("partner", AlipayConfig.partner);
								sParaTemp.put("_input_charset", AlipayConfig.input_charset);
								sParaTemp.put("notify_url", AlipayConfig.notify_url);
								sParaTemp.put("biz_type", biz_type);
								sParaTemp.put("biz_data", jsonString);
								sParaTemp.put("timestamp", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now()));
								System.out.println(sParaTemp.values());
								AlipaySubmit alipaySubmit = new AlipaySubmit();
								logger.info(" QRServiceImpl |addStaticQR | BEFORE SUBMIT ");
								String sHtmlText = alipaySubmit.buildRequest("", "", sParaTemp);
								logger.info(" QRServiceImpl |addStaticQR | AFTER SUBMIT " +sHtmlText);
								DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
								InputSource src = new InputSource();
								 src.setCharacterStream(new StringReader(sHtmlText));
								 org.w3c.dom.Document doc = builder.parse(src);
						        String responseStatus = doc.getElementsByTagName("is_success").item(0).getTextContent();
						        logger.info(" QRServiceImpl |addStaticQR | RESPONSE STATUS " +responseStatus);
						        String url = doc.getElementsByTagName("qrcode_img_url").item(0).getTextContent();
						        String qrCodeUrl = doc.getElementsByTagName("qrcode").item(0).getTextContent();
						        logger.info(" QRServiceImpl |addStaticQR | URL " +url);
						        if(responseStatus.equalsIgnoreCase("T")) {
						        logger.debug("Successfully Generated."+url);
						        logger.info(" QRServiceImpl |addStaticQR | Successfully GENERATED " +url);
						       HttpURLConnection con = (HttpURLConnection)(new URL( url ).openConnection());
						       con.setConnectTimeout(12000);
						       con.setReadTimeout(12000);
						       con.setInstanceFollowRedirects( false );
						       logger.info("QRServiceImpl |addStaticQR | Before connect to get redirect URL " +url);
						       long startTimeRedirect = System.currentTimeMillis();
						       try {
						    	   
						       con.connect();
						       		long endTimeRedirect = System.currentTimeMillis();
						       		logger.info(" Redirect Time difference" + (endTimeRedirect-startTimeRedirect));
						       }
						       catch(SocketTimeoutException e) {
						    	   logger.info(" Redirect Time difference" + (System.currentTimeMillis()-startTimeRedirect));
						    	   logger.error(" Exception occured when try to get redirct URL "+e.getMessage() ,e);
						       }
						       logger.info("QRServiceImpl |addStaticQR | After Response Received " +url);
						       int responseCode = con.getResponseCode();
						       logger.info("QRServiceImpl |addStaticQR |  Response Received responseCode  " +responseCode);
						       String redirectedURL = con.getHeaderField( "Location" );
						       logger.info("QRServiceImpl |addStaticQR | Response Received redirectedURL  " +redirectedURL);
						       	ByteArrayOutputStream baos = new ByteArrayOutputStream();
						        String fetchedUrl = getFinalURL(url);
						        logger.info("QRServiceImpl |addStaticQR | Final URL  " +fetchedUrl);
						       
						        URLConnection conn2 =(new URL(fetchedUrl)).openConnection();
						        logger.info("QRServiceImpl |addStaticQR | GET Stream from the redirect URL " +fetchedUrl);
						        try {
						            IOUtils.copy(conn2.getInputStream(), baos);
						            System.out.println(baos.toByteArray().toString());
						            System.out.println("byte stream------"+org.apache.commons.codec.binary.Base64.encodeBase64String(baos.toByteArray()));
						            encodedString = org.apache.commons.codec.binary.Base64.encodeBase64String(baos.toByteArray());
						            logger.info("Successfully Generated. encodedString "+encodedString);
						            decodedImg = Base64.getDecoder().decode(encodedString);
						            }
						            catch(Exception e) {
						            	e.printStackTrace();
						            	logger.error("QRServiceImpl | addStaticQR | Exception Occured "+e.getMessage(),e);
						            }
						}
						        
						qrCode.setQrImage(decodedImg);
						qrCode.setQr_code_url(qrCodeUrl);
						}
						else {
							System.out.println("Failed");
							logger.debug(" Alipay QR generation failed");
						}
					}
					else {
					encodedString = qrImageProcessor.getBase64EncodedQRImage(qrCode.getQrContent(),merchant.getLogo(), request.getColor(),size, size, request.isLogoRequired());
					decodedImg = Base64.getDecoder().decode(encodedString);
					
					}
					
					if(request.getQrType().equalsIgnoreCase(QRType.ALIPAY.toString()) && encodedString!=null  || !request.getQrType().equalsIgnoreCase(QRType.ALIPAY.toString()) )
						qrCode = qrRepository.save(qrCode);
					
					qrCode.setQrId(getQRId(qrCode.getId()));

					if (request.getQrType().equals("Auto Generated") || request.getQrType().equals("Alipay")) {
						qrCode.setQrContent(getQRId(qrCode.getId()));
					}
			
					qrCode.setQrImage(decodedImg);
					if(request.getQrType().equalsIgnoreCase(QRType.ALIPAY.toString()) && encodedString!=null  || !request.getQrType().equalsIgnoreCase(QRType.ALIPAY.toString()) ) {
						qrRepository.save(qrCode);
					res.setStatus(resourse.getString(SUCCESS));
					resDetails.setCode(resourse.getString("add.qrcode.success.code"));
					resDetails.setDescription(resourse.getString("add.qrcode.success"));
					res.setMessage(resDetails);
					ResponseData resData = new ResponseData(qrCode.getQrId(),resourse.getString("qrcode.base.url")+"/image/" + qrCode.getId(),resourse.getString("qrcode.base.url")+"/qrpdf/" + qrCode.getId());
					json = new Gson().toJson(resData);
					res.setData(json);
					logger.debug("  Alipay QR Images saved to DB Success "+qrCode.getQrId());
					}
					else {
						res.setStatus(resourse.getString(FAILED));
						resDetails.setCode(resourse.getString("qr.contetnt.same.code"));
						resDetails.setDescription(resourse.getString("qr.contetnt.same"));
						res.setMessage(resDetails);
						res.setData(null);
					}
				} else {
					res.setStatus(resourse.getString(FAILED));
					resDetails.setCode(resourse.getString("qr.contetnt.same.code"));
					resDetails.setDescription(resourse.getString("qr.contetnt.same"));
					res.setMessage(resDetails);
					res.setData(null);
				}
			}
			long endTime = System.currentTimeMillis();
			logger.info("QRServiceImpl Service ENDED addStaticQR | "
					+ request.toString() + " | status=" + res.getStatus()
					+ " | message=" + res.getMessage() + " | avg_resp="
					+ (endTime - startTime));
			return res;
		} catch (Exception e) {
			logger.error("QRServiceImpl Service ERROR addStaticQR | "
					+ request.toString() + " | message=" + e ,e);
			res.setStatus(resourse.getString(FAILED));
			resDetails.setCode(resourse.getString("add.qrcode.failed.code"));
			resDetails.setDescription(resourse.getString("add.qrcode.failed"));
			res.setMessage(resDetails);
			res.setData(null);
			return res;
		}
	}

	@Override
	public ResponseObject getQRCodes(GetQRRequest request) {
		long startTime = System.currentTimeMillis();
		logger.info("QRServiceImpl Service STARTED getQRCodes | "
				+ request.toString());
		ResponseObject res = new ResponseObject();
		ResponseDetails resDetails = new ResponseDetails();
		String json="";
		int merchantType=0;
		try {
			QRMerchant merchant = merchantRepository.getMerchantbyName(request
					.getMerchantName().toUpperCase().trim());
			if (null == merchant) {
				res.setStatus(resourse.getString(FAILED));
				resDetails.setCode(resourse.getString("merchant.invalid.input.code"));
				resDetails.setDescription(resourse.getString("merchant.invalid.name"));
				res.setMessage(resDetails);
				res.setData(null);
			} else {
				// String status = "";
				
				merchantType = merchant.getType();
				
				int stat = 0;
				if (request.getStatus()
						.equalsIgnoreCase(QRStatus.ACTIVE.name())) {
					stat = 1;
				} else if (request.getStatus().equalsIgnoreCase(
						QRStatus.INACTIVE.name())) {
					stat = 2;
				} else {
					stat = 0;
				}
				List<QRCode> qrList = null;
				if (stat == QRStatus.ACTIVE.ordinal()
						|| stat == QRStatus.INACTIVE.ordinal()) {
					qrList = qrRepository.findByMemIdAndStatus(
							merchant.getMerchantId(), stat);
				} else {
					qrList = qrRepository.findByMemId(merchant.getMerchantId());
				}
				res.setStatus(resourse.getString(SUCCESS));
				resDetails.setCode(resourse.getString("load.qrcodes.success.code"));
				resDetails.setDescription(resourse.getString("load.qrcodes.success"));
				res.setMessage(resDetails);
				Iterator<QRCode> iterator = qrList.iterator();
				// int i = 1;
				List<QRCodeResponse> qrCodes = new ArrayList<QRCodeResponse>();
				while (iterator.hasNext()) {
					QRCode q = iterator.next();
					QRCodeResponse qrResponse = new QRCodeResponse(q);
					qrResponse.setNatureOfBusiness(MerchantType.values()[merchantType].toString());
					qrCodes.add(qrResponse);
					
				}
				json = new Gson().toJson(qrCodes);
				res.setData(json);
			}
			long endTime = System.currentTimeMillis();
			logger.info("QRServiceImpl Service ENDED getQRCodes | "
					+ request.toString() + " | status=" + res.getStatus()
					+ " | message=" + res.getMessage() + " | avg_resp="
					+ (endTime - startTime));
			return res;
		} catch (Exception e) {
			logger.error("QRServiceImpl Service ERROR getQRCodes | "
					+ request.toString() + " | message=" + e.getMessage());
			res.setStatus(resourse.getString(FAILED));
			resDetails.setCode(resourse.getString("load.qrcode.failed.code"));
			resDetails.setDescription(resourse.getString("load.qrcode.failed"));
			res.setMessage(resDetails);
			res.setData(null);
			return res;
		}
	}

	@Override
	public ResponseObject getQRCodesByDate(QrDetails request) {
		long startTime = System.currentTimeMillis();
		logger.info("QRServiceImpl Service STARTED getQRCodesByDate | "
				+ request.toString());
		ResponseObject res = new ResponseObject();
		ResponseDetails resDetails = new ResponseDetails();
		String json="";
		try {
			QRMerchant merchant = merchantRepository.getMerchantbyName(request
					.getMerchantName().toUpperCase().trim());
			if (null == merchant) {
				res.setStatus(resourse.getString(FAILED));
				resDetails.setCode(resourse.getString("merchant.invalid.input.code"));
				resDetails.setDescription(resourse.getString("merchant.invalid.name"));
				res.setMessage(resDetails);
				res.setData(null);
			} else {

				List<QRCode> qrList = null;
				if(request.getDateFrom().equals("")||request.getDateFrom().equals(null)||
						request.getDateTo().equals("")||request.getDateTo().equals(null)){
					qrList = qrRepository.findByMemId(merchant.getMerchantId());
				}else{

				String date = request.getDateFrom();
				String date2 = request.getDateTo();
				SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
				Calendar c = Calendar.getInstance();
				c.setTime(dt1.parse(date2));
				c.add(Calendar.DATE, 1);  // number of days to add
				date2 = dt1.format(c.getTime());
				qrList = qrRepository.getQRCodesByDate(
						merchant.getMerchantId(), dt1.parse(date),
						dt1.parse(date2));
				}
				
				Iterator<QRCode> iterator = qrList.iterator();
				// int i = 1;
				List<QRCodeResponse> qrCodes = new ArrayList<QRCodeResponse>();
				while (iterator.hasNext()) {
					QRCode q = iterator.next();
					QRCodeResponse qrResponse = new QRCodeResponse(q,merchant);
					qrCodes.add(qrResponse);
				}
				json = new Gson().toJson(qrCodes);
				res.setData(json);
				
				res.setStatus(resourse.getString(SUCCESS));
				resDetails.setCode(resourse.getString("load.qrcodes.success.code"));
				resDetails.setDescription(resourse.getString("load.qrcodes.success"));
				res.setMessage(resDetails);
			}
			long endTime = System.currentTimeMillis();
			logger.info("QRServiceImpl Service ENDED getQRCodesByDate | "
					+ request.toString() + " | status=" + res.getStatus()
					+ " | message=" + res.getMessage() + " | avg_resp="
					+ (endTime - startTime));
			return res;
		} catch (Exception e) {
			logger.error("QRServiceImpl Service ERROR getQRCodesByDate | "
					+ request.toString() + " | message=" + e.getMessage());
			res.setStatus(resourse.getString(FAILED));
			resDetails.setCode(resourse.getString("load.qrcode.failed.code"));
			resDetails.setDescription(resourse.getString("load.qrcode.failed"));
			res.setMessage(resDetails);
			res.setData(null);
			return res;
		}
	}

	public String getQRId(long idVal) {
		return "QR" + String.format("%06d", idVal);
	}

	@Override
	public ResponseObject getQRContent(ValidateQRRequest request) {
		long startTime = System.currentTimeMillis();
		logger.info("QRServiceImpl Service STARTED getQRContent | "
				+ request.toString());
		ResponseObject res = new ResponseObject();
		ResponseDetails resDetails = new ResponseDetails();
		String json="";
		try {
			QRMerchant merchant = merchantRepository.getMerchant(request
					.getMerchantID());
			if (null == merchant) {
				res.setStatus(resourse.getString(FAILED));
				resDetails.setCode(resourse.getString(INVALIDMERCHANTCODE));
				resDetails.setDescription(resourse.getString(INVALIDMERCHANDID));
				res.setMessage(resDetails);
				res.setData(null);
			} else {
				QRCode qrCode = qrRepository.checkQRId(request.getQrCodeID(),
						request.getMerchantID());
				if (null == qrCode) {
					res.setStatus(resourse.getString(FAILED));
					resDetails.setCode(resourse.getString("qr.invalid.qr.id.code"));
					resDetails.setDescription(resourse.getString("qr.invalid.qr.id"));
					res.setMessage(resDetails);
					res.setData(null);
				} else {
					res.setStatus(resourse.getString(SUCCESS));
					resDetails.setCode(resourse.getString("qr.code.retrieve.success.code"));
					resDetails.setDescription(resourse.getString("qr.code.retrieve.success"));
					res.setMessage(resDetails);
					ResponseData resData = new ResponseData();
					resData.setQR_ID(qrCode.getQrId());
					resData.setURL(resourse.getString("qrcode.base.url")+ "/image/" + qrCode.getId());
					String status = null;
					if (qrCode.getStatus() == QRStatus.ACTIVE.ordinal()) {
						status = QRStatus.ACTIVE.name();
					} else {
						status = QRStatus.INACTIVE.name();
					}
					resData.setQR_STATUS(status);
					json = new Gson().toJson(resData);
					res.setData(json);
				}
			}
			long endTime = System.currentTimeMillis();
			logger.info("QRServiceImpl Service ENDED getQRContent | "
					+ request.toString() + " | status=" + res.getStatus()
					+ " | message=" + res.getMessage() + " | avg_resp="
					+ (endTime - startTime));
			return res;
		} catch (Exception e) {
			logger.error("QRServiceImpl Service ERROR getQRContent | "
					+ request.toString() + " | message=" + e.getMessage());
			res.setStatus(resourse.getString(FAILED));
			resDetails.setCode(resourse.getString("qr.code.retrieve.failed.code"));
			resDetails.setDescription(resourse.getString("qr.code.retrieve.failed"));
			res.setMessage(resDetails);
			res.setData(null);
			return res;
		}
	}

	@Transactional
	@Override
	public ResponseObject tagQRToApp(TagQRtoApp request) {
		long startTime = System.currentTimeMillis();
		logger.info("QRServiceImpl Service STARTED tagQRToApp | "
				+ request.toString());
		ResponseObject res = new ResponseObject();
		ResponseDetails resDetails = new ResponseDetails();
		String json="";
		try {
			QRMerchant merchant = merchantRepository.getMerchant(request
					.getMerchantID());
			if (null == merchant) {
				res.setStatus(resourse.getString(FAILED));
				resDetails.setCode(resourse.getString(INVALIDMERCHANTCODE));
				resDetails.setDescription(resourse.getString(INVALIDMERCHANDID));
				res.setMessage(resDetails);
				res.setData(null);
			} else {
				QRCode qrCode = qrRepository.checkQRId(request.getQrID(),
						request.getMerchantID());
				if (null == qrCode) {
					res.setStatus(resourse.getString(FAILED));
					resDetails.setCode(resourse.getString("qr.invalid.qr.id.code"));
					resDetails.setDescription(resourse.getString("qr.invalid.qr.id"));
					res.setMessage(resDetails);
					res.setData(null);
				} else if (qrCode.getStatus() == QRStatus.INACTIVE.ordinal()) {
					res.setStatus(resourse.getString(FAILED));
					resDetails.setCode(resourse.getString("qr.disabled.code"));
					resDetails.setDescription(resourse.getString("qr.disabled"));
					res.setMessage(resDetails);
					res.setData(null);
				} else if (null == qrCode.getAppCode()
						|| qrCode.getAppCode() == "") {
					QRApplication app = appRepository.checkAppCode(request
							.getAppCode());
					if (app == null) {
						res.setStatus(resourse.getString(FAILED));
						resDetails.setCode(resourse.getString("app.invalid.appcode.code"));
						resDetails.setDescription(resourse.getString("app.invalid.appcode"));
						res.setMessage(resDetails);
						res.setData(null);
					} else {
						qrRepository.updateAppCode(request.getAppCode(),
								qrCode.getId());
						res.setStatus(resourse.getString(SUCCESS));
						resDetails.setCode(resourse.getString("app.tag.to.qr.success.code"));
						resDetails.setDescription(resourse.getString("app.tag.to.qr.success"));
						res.setMessage(resDetails);
						res.setData(null);
					}
				} else {
					res.setStatus(resourse.getString(FAILED));
					resDetails.setCode(resourse.getString("app.already.tag.to.qr.code"));
					resDetails.setDescription(resourse.getString("app.already.tag.to.qr"));
					res.setMessage(resDetails);
					res.setData(null);
				}
			}
			long endTime = System.currentTimeMillis();
			logger.info("QRServiceImpl Service ENDED tagQRToApp | "
					+ request.toString() + " | status=" + res.getStatus()
					+ " | message=" + res.getMessage() + " | avg_resp="
					+ (endTime - startTime));
			return res;
		} catch (Exception e) {
			logger.error("QRServiceImpl Service ERROR tagQRToApp | "
					+ request.toString() + " | message=" + e.getMessage());
			res.setStatus(resourse.getString(FAILED));
			resDetails.setCode(resourse.getString("app.tag.to.qr.failed.code"));
			resDetails.setDescription(resourse.getString("app.tag.to.qr.failed"));
			res.setMessage(resDetails);
			res.setData(null);
			return res;
		}
	}

	@Transactional
	@Override
	public ResponseObject disableQR(DisableQrRequest request) {
		long startTime = System.currentTimeMillis();
		logger.info("QRServiceImpl Service STARTED disableQR | "
				+ request.toString());
		ResponseObject res = new ResponseObject();
		ResponseDetails resDetails = new ResponseDetails();
		try {
			QRMerchant merchant = merchantRepository.getMerchant(request
					.getMerchantID());
			if (null == merchant) {
				res.setStatus(resourse.getString(FAILED));
				resDetails.setCode(resourse.getString(INVALIDMERCHANTCODE));
				resDetails.setDescription(resourse.getString(INVALIDMERCHANDID));
				res.setMessage(resDetails);
				res.setData(null);
			} else {
				QRCode qrCode = qrRepository.checkQRId(request.getQrID(),
						request.getMerchantID());
				if (null == qrCode) {
					res.setStatus(resourse.getString(FAILED));
					resDetails.setCode(resourse.getString("qr.invalid.qr.id.code"));
					resDetails.setDescription(resourse.getString("qr.invalid.qr.id"));
					res.setMessage(resDetails);
					res.setData(null);
				} else {
					if (qrCode.getStatus() == 2) {

						qrRepository.enableQRCode(
								MerchantStatus.ACTIVE.ordinal(),
								request.getQrID(), request.getMerchantID());
						res.setStatus(resourse.getString(SUCCESS));
						resDetails.setCode(resourse.getString("qr.status.update.success.no.app.code"));
						resDetails.setDescription(resourse.getString("qr.status.update.success.no.app"));
						res.setMessage(resDetails);
						res.setData(null);
					} else {
						qrRepository.disableQRCode(
								MerchantStatus.INACTIVE.ordinal(),
								request.getQrID(), request.getMerchantID());
						String appcode = qrCode.getAppCode();
						if (null == appcode || appcode == "") {
							res.setStatus(resourse.getString(SUCCESS));
							resDetails.setCode(resourse.getString("qr.status.update.success.no.app.code"));
							resDetails.setDescription(resourse.getString("qr.status.update.success.no.app"));
							res.setMessage(resDetails);
							res.setData(null);
						} else {
							QRApplication app = appRepository
									.checkAppCode(appcode);
							try {
								MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
								headers.add("Content-Type", "application/json");
								restTemplate = new RestTemplate(
										getClientHttpRequestFactory());
								HttpEntity<DisableQrRequest> request1 = new HttpEntity<>(
										request, headers);
								String result1 = restTemplate.postForObject(
										app.getNotifyURL(), request1,
										String.class);
								logger.info("QRServiceImpl Service disableQR"
										+ " | result=" + result1);
								res.setStatus(resourse.getString(SUCCESS));
								resDetails.setCode(resourse.getString("qr.status.update.success.with.app.code"));
								resDetails.setDescription(resourse.getString("qr.status.update.success.with.app"));
								res.setMessage(resDetails);
								res.setData(null);
							} catch (ResourceAccessException e) {
								logger.error(e.getMessage());
								res.setStatus(resourse.getString(SUCCESS));
								resDetails.setCode(resourse.getString("qr.app.notify.failed.code"));
								resDetails.setDescription(resourse.getString("qr.app.notify.failed"));
								res.setMessage(resDetails);
								res.setData(null);
							}
						}
					}
				}
			}
			long endTime = System.currentTimeMillis();
			logger.info("QRServiceImpl Service ENDED disableQR | "
					+ request.toString() + " | message=" + res.getMessage()
					+ " | avg_resp=" + (endTime - startTime));
			return res;
		} catch (Exception e) {
			logger.error("QRServiceImpl Service ERROR disableQR | "
					+ request.toString() + " | message=" + e.getMessage());
			res.setStatus(resourse.getString(FAILED));
			resDetails.setCode(resourse.getString("qr.status.update.failed.code"));
			resDetails.setDescription(resourse.getString("qr.status.update.failed"));
			res.setMessage(resDetails);
			res.setData(null);
			return res;
		}
	}

	@Transactional
	@Override
	public ResponseObject createBulkStaticQR(BulkQrRequest request) {
		long startTime = System.currentTimeMillis();
		logger.info("QRServiceImpl Service STARTED createBulkStaticQR | "
				+ request.toString());
		ResponseObject res = new ResponseObject();
		ResponseDetails resDetails = new ResponseDetails();
		String json="";
		byte[][] qrArray;
		qrArray = new byte[request.getNumberOfItems()][];
		String fileName = "";
		try {
			for (int i = 0; i < request.getNumberOfItems(); i++) {
				QRCode qrCode = new QRCode();
				qrCode.setAddedBy(request.getAddedBy());
				DateFormat dateFormat = new SimpleDateFormat(
						"yyyy/MM/dd HH:mm:ss");
				DateFormat dateFormat1 = new SimpleDateFormat("yyyyMMddHHmmss");
				Date date = new Date();
				qrCode.setAddedDate(dateFormat.parse(dateFormat.format(date)));
				fileName = dateFormat1.format(date) + "_"
						+ request.getNumberOfItems();
				// setting default in to active state
				qrCode.setStatus(QRStatus.ACTIVE.ordinal());
				qrCode.setQrContent("BULK_QR");
				qrCode.setMerchantId("-");
				qrCode = qrRepository.save(qrCode);
				String qrid = getQRId(qrCode.getId());
				// if logo required need to add logo
				byte[] decodedLogoImg = null;
				if (request.isLogoRequired()) {
					try {

						decodedLogoImg = Base64.getDecoder().decode(
								request.getLogo());
					} catch (IllegalArgumentException e) {
						res.setStatus(resourse.getString(FAILED));
						resDetails.setCode(resourse.getString("image.encoding.failed.code"));
						resDetails.setDescription(resourse.getString("image.encoding.failed"));
						res.setMessage(resDetails);
						res.setData(null);
						logger.error("ApplicationServiceImpl Service ERROR registerApp"
								+ " | added_by="
								+ request.getAddedBy()
								+ " | message=" + e.getMessage());
						return res;
					}
				}
				// update generated QR Code ID
				String encodedString = qrImageProcessor
						.getBase64EncodedQRImage(qrid,
								request.isLogoRequired() ? decodedLogoImg
										: null, request.getColor(), request
										.getSize(), request.getSize(), request
										.isLogoRequired());
				byte[] decodedImg = Base64.getDecoder().decode(encodedString);
				qrCode.setQrImage(decodedImg);
				qrCode.setQrId(qrid);
				qrCode.setQrContent(qrid);
				qrCode = qrRepository.save(qrCode);
				qrArray[i] = qrCode.getQrImage();
				res.setStatus(resourse.getString(SUCCESS));
				resDetails.setCode(resourse.getString("add.qrcode.success.code"));
				resDetails.setDescription(resourse.getString("add.qrcode.success"));
				res.setMessage(resDetails);
				ResponseData resData = new ResponseData();
				resData.setURL(resourse.getString("qrcode.base.url") + "/pdf/"+ fileName);
				json = new Gson().toJson(resData);
				res.setData(json);
				long endTime = System.currentTimeMillis();
				logger.info("QRServiceImpl Service ENDED createBulkStaticQR | "
						+ request.toString() + " | status=" + res.getStatus()
						+ " | message=" + res.getMessage() + " | avg_resp="
						+ (endTime - startTime));
			}
			generatePDF(qrArray, request.getNumberOfItems(), fileName);
			return res;
		} catch (Exception e) {
			logger.error("QRServiceImpl Service ERROR createBulkStaticQR | "
					+ request.toString() + " | message=" + e);
			res.setStatus(resourse.getString(FAILED));
			resDetails.setCode(resourse.getString("add.qrcode.failed.code"));
			resDetails.setDescription(resourse.getString("add.qrcode.failed"));
			res.setMessage(resDetails);
			res.setData(null);
			return res;
		}
	}

	@Override
	public ResponseObject updateQRDetails(Request qrRequest) {
		logger.info("QRServiceImpl Begin Method updateQRDetails : Request = " + qrRequest.toString());
		long startTime = System.currentTimeMillis();
		ResponseObject res = new ResponseObject();
		ResponseDetails resDetails = new ResponseDetails();
		String json = "";
		try {
			QRCode qrDetails = qrRepository.checkQRId(qrRequest.getQrID(), qrRequest.getMerchantID());
			if (null != qrDetails) {
				logger.info(" QRServiceImpl |updateStaticQR | request.getQrType() " + qrRequest.getQrType());
				if ((qrRequest.getQrType() != null
						&& qrRequest.getQrType().equalsIgnoreCase(QRType.ALIPAY.toString()))) {
					// Need to get Merchant details to submit AliPay URL
					QRMerchant qrMerchant = merchantRepository.getMerchant(qrRequest.getMerchantID());
					if (qrMerchant != null) {

						String sHtmlText = setAlipayModifyPayload(qrRequest, qrMerchant, qrDetails);
						DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
						InputSource src = new InputSource();
						src.setCharacterStream(new StringReader(sHtmlText));
						org.w3c.dom.Document doc = builder.parse(src);

						String responseStatus = doc.getElementsByTagName("is_success").item(0).getTextContent();
						logger.info(" QRServiceImpl |updateStaticQR | RESPONSE STATUS " + responseStatus);

						if ("T".equalsIgnoreCase(responseStatus)) {
							String qrcode = doc.getElementsByTagName("qrcode").item(0).getTextContent();
							logger.debug("Successfully Updated QR code in Alipay End. QRcode : " + qrcode);
							logger.info(" QRServiceImpl |updateStaticQR | Successfully Updated.");
						} else {
							res.setStatus(resourse.getString(FAILED));
							resDetails.setCode(resourse.getString("staticqr.update.fail.code"));
							resDetails.setDescription(resourse.getString("staticqr.update.fail"));
							res.setMessage(resDetails);
							res.setData(null);
							return res;
						}

					} else {
						res.setStatus(resourse.getString(FAILED));
						resDetails.setCode(resourse.getString("error.InvalidMerchantID.code"));
						resDetails.setDescription(resourse.getString("error.InvalidMerchantID"));
						res.setMessage(resDetails);
						res.setData(null);
						return res;
					}

				}
				qrDetails.setAlias(qrRequest.getQrAlias());
				qrDetails.setCounter(qrRequest.getCounter());
				qrDetails.setLocaton(qrRequest.getQrLocation());
				qrDetails.setLatitude(qrRequest.getQrLatitude());
				qrDetails.setLongitude(qrRequest.getQrLongitude());
				qrDetails.setModifiedDate(formatter.parse(formatter.format(new Date())));
				qrDetails.setModifiedBy(qrRequest.getModifiedBy());
				qrDetails.setModifiedReason(qrRequest.getModifiedReason());

				qrDetails = qrRepository.save(qrDetails);
				res.setStatus(resourse.getString("success"));
				resDetails.setCode(resourse.getString("staticqr.update.success.code"));
				resDetails.setDescription(resourse.getString("staticqr.update.success"));
				res.setMessage(resDetails);
				ResponseData resData = new ResponseData();
				resData.setQR_ID(qrDetails.getQrId());
				json = new Gson().toJson(resData);
				res.setData(json);
			} else {
				res.setStatus(resourse.getString(FAILED));
				resDetails.setCode(resourse.getString("qr.invalid.qr.id.code"));
				resDetails.setDescription(resourse.getString("qr.invalid.qr.id"));
				res.setMessage(resDetails);
				res.setData(null);
			}

		} catch (ConstraintViolationException e) {
			logger.error("QRServiceImpl Service ERROR updateQRDetails | " + " | message=" + e.getMessage());
			res.setStatus(resourse.getString(FAILED));
			resDetails.setCode(resourse.getString("error.ConstraintViolation.code"));
			resDetails.setDescription(resourse.getString("error.ConstraintViolation"));
			res.setMessage(resDetails);
			res.setData(null);
		} catch (Exception e) {
			logger.error("QRServiceImpl Service ERROR updateQRDetails | " + " | message=" + e.getMessage());
			res.setStatus(resourse.getString(FAILED));
			resDetails.setCode(resourse.getString("staticqr.update.fail.code"));
			resDetails.setDescription(resourse.getString("staticqr.update.fail"));
			res.setMessage(resDetails);
			res.setData(null);
		}
		long endTime = System.currentTimeMillis();
		logger.info("QRServiceImpl API Service ENDED updateQRDetails | " + " | status=" + res.getStatus()
				+ " | message=" + res.getMessage() + " | avg_resp=" + (endTime - startTime));
		return res;
	}

	@Transactional
	@Override
	public ResponseObject updateQRStatus(QRStatusRequest qrRequest) {
		logger.info("QRServiceImpl Begin Method updateQRStatus : Request = " + qrRequest.toString());
		long startTime = System.currentTimeMillis();
		ResponseObject response = new ResponseObject();
		ResponseDetails resDetails = new ResponseDetails();
		String json = "";
		try {
			QRCode qrDetails = qrRepository.checkQRId(qrRequest.getQrID(), qrRequest.getMerchantID());
			if (null != qrDetails) {
				logger.info(" QRServiceImpl |updateQRStatus | request.getQrType() " + qrRequest.getQrType());
				if ((qrRequest.getQrType() != null
						&& qrRequest.getQrType().equalsIgnoreCase(QRType.ALIPAY.toString()))) {
					QRMerchant qrMerchant = merchantRepository.getMerchant(qrRequest.getMerchantID());
					if (qrMerchant != null) {
						if (qrRequest.getStatus() != null) {
							String status = "";
							switch (QRStatus.valueOf(qrRequest.getStatus()).ordinal()) {
							case 1:
								status = "RESTART";
								break;
							case 2:
								status = "STOP";
								break;
							case 6:
								status = "DELETE";
								break;
							default:
								status = "NS";
							}
							if (!status.equalsIgnoreCase("NS")) {
								String sHtmlText = setAlipayModifyStatusPayload(qrDetails, status);
								DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
								InputSource src = new InputSource();
								src.setCharacterStream(new StringReader(sHtmlText));
								org.w3c.dom.Document doc = builder.parse(src);

								String responseStatus = doc.getElementsByTagName("is_success").item(0).getTextContent();
								logger.info(" QRServiceImpl |updateStaticQR | RESPONSE STATUS " + responseStatus);

								if ("T".equalsIgnoreCase(responseStatus)) {
									String qrcode = doc.getElementsByTagName("qrcode").item(0).getTextContent();
									logger.debug(
											"Successfully Updated QR code Status in Alipay End. QRcode : " + qrcode);
									logger.info(" QRServiceImpl |updateQRStatus | Successfully Updated.");
								} else {
									response.setStatus(resourse.getString(FAILED));
									resDetails.setCode(resourse.getString("staticqr.status.update.fail.code"));
									resDetails.setDescription(resourse.getString("staticqr.status.update.fail"));
									response.setMessage(resDetails);
									response.setData(null);
									return response;
								}
							} else {
								response.setStatus(resourse.getString(FAILED));
								resDetails.setCode(resourse.getString("qr.invalid.status.code"));
								resDetails.setDescription(resourse.getString("qr.invalid.status"));
								response.setMessage(resDetails);
								response.setData(null);
								return response;
							}
						}

					} else {
						response.setStatus(resourse.getString(FAILED));
						resDetails.setCode(resourse.getString("error.InvalidMerchantID.code"));
						resDetails.setDescription(resourse.getString("error.InvalidMerchantID"));
						response.setMessage(resDetails);
						response.setData(null);
						return response;
					}
				}
				qrDetails.setModifiedDate(formatter.parse(formatter.format(new Date())));
				qrDetails.setModifiedBy(qrRequest.getModifiedBy());
				qrDetails.setModifiedReason(qrRequest.getModifiedReason());
				qrDetails.setStatus(QRStatus.valueOf(qrRequest.getStatus()).ordinal());

				qrRepository.modifyQRCodeStatus(QRStatus.valueOf(qrRequest.getStatus()).ordinal(),
						qrDetails.getModifiedDate(), qrDetails.getModifiedBy(), qrDetails.getModifiedReason(),
						qrDetails.getQrId(), qrDetails.getMerchantId());

				response.setStatus(resourse.getString("success"));
				resDetails.setCode(resourse.getString("staticqr.status.update.success.code"));
				resDetails.setDescription(resourse.getString("staticqr.status.update.success"));
				response.setMessage(resDetails);
				ResponseData resData = new ResponseData(qrDetails.getQrCategory(), qrDetails.getMerchantId(),
						qrDetails.getQrId(), String.valueOf(qrDetails.getStatus()));
				json = new Gson().toJson(resData);
				response.setData(json);

			} else {
				response.setStatus(resourse.getString(FAILED));
				resDetails.setCode(resourse.getString("error.InvalidMerchantID.code"));
				resDetails.setDescription(resourse.getString("error.InvalidMerchantID"));
				response.setMessage(resDetails);
				response.setData(null);
				return response;
			}

		} catch (Exception e) {
			logger.error("QRServiceImpl Service ERROR updateQRStatus | " + " | message=" + e.getMessage());
			response.setStatus(resourse.getString(FAILED));
			resDetails.setCode(resourse.getString("staticqr.update.fail.code"));
			resDetails.setDescription(resourse.getString("staticqr.update.fail"));
			response.setMessage(resDetails);
			response.setData(null);
		}
		long endTime = System.currentTimeMillis();
		logger.info("QRServiceImpl API Service ENDED updateQRStatus | " + " | status=" + response.getStatus()
				+ " | message=" + response.getMessage() + " | avg_resp=" + (endTime - startTime));
		return response;
	}
	
	@Override
	public ResponseObject getQrCodesTagDetails(QrDetails request) {
		long startTime = System.currentTimeMillis();
		logger.info("QRServiceImpl Service STARTED getQrCodesTagDetails | "
				+ request.toString());
		ResponseObject res = new ResponseObject();
		ResponseDetails resDetails = new ResponseDetails();
		String json="";
		try {
			QRMerchant merchant = merchantRepository.getMerchantbyName(request
					.getMerchantName().toUpperCase().trim());
			if (null == merchant) {
				res.setStatus(resourse.getString(FAILED));
				resDetails.setCode(resourse.getString("merchant.invalid.input.code"));
				resDetails.setDescription(resourse.getString("merchant.invalid.name"));
				res.setMessage(resDetails);
				res.setData(null);
			} else {
				List<QRCode> qrList = null;
				qrList = qrRepository.findByMemId(merchant.getMerchantId());
				
				Iterator<QRCode> iterator = qrList.iterator();
				List<QRCodeResponse> qrCodes = new ArrayList<QRCodeResponse>();
				while (iterator.hasNext()) {
					QRCode q = iterator.next();
					QRCodeResponse qrResponse;
					String appName = null;
					if(q.getAppCode()!=null){
						QRApplication app = appRepository.checkAppCode(q.getAppCode());
						appName = app.getAppName();
						qrResponse = new QRCodeResponse(q,merchant,appName);
					}else{
						appName="-";
						qrResponse = new QRCodeResponse(q,merchant,appName);
					}
					qrCodes.add(qrResponse);
				}
				json = new Gson().toJson(qrCodes);
				res.setData(json);
				
				res.setStatus(resourse.getString(SUCCESS));
				resDetails.setCode(resourse.getString("load.qrcodes.success.code"));
				resDetails.setDescription(resourse.getString("load.qrcodes.success"));
				res.setMessage(resDetails);
			}
			long endTime = System.currentTimeMillis();
			logger.info("QRServiceImpl Service ENDED getQrCodesTagDetails | "
					+ request.toString() + " | status=" + res.getStatus()
					+ " | message=" + res.getMessage() + " | avg_resp="
					+ (endTime - startTime));
			return res;
		} catch (Exception e) {
			logger.error("QRServiceImpl Service ERROR getQrCodesTagDetails | "
					+ request.toString() + " | message=" + e.getMessage());
			res.setStatus(resourse.getString(FAILED));
			resDetails.setCode(resourse.getString("load.qrcode.failed.code"));
			resDetails.setDescription(resourse.getString("load.qrcode.failed"));
			res.setMessage(resDetails);
			res.setData(null);
			return res;
		}
	}
	
	@Override
	public ResponseObject getBulkQRDetails(QrDetails request) {
		long startTime = System.currentTimeMillis();
		logger.info("QRServiceImpl Service STARTED getBulkQRDetails | "
				+ request.toString());
		ResponseObject res = new ResponseObject();
		ResponseDetails resDetails = new ResponseDetails();
		String json="";
		try {
			List<QRCode> qrList = null;
			if (request.getDateFrom().equals("")
					|| request.getDateFrom().equals(null)
					|| request.getDateTo().equals("")
					|| request.getDateTo().equals(null)) {
				// qrList = qrRepository.findByMemId(merchant.getMerchantId());
			} else {

				String date = request.getDateFrom();
				String date2 = request.getDateTo();
				SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
				Calendar c = Calendar.getInstance();
				c.setTime(dt1.parse(date2));
				c.add(Calendar.DATE, 1);  // number of days to add
				date2 = dt1.format(c.getTime());
				qrList = qrRepository.getBulkQRCodesByDate(dt1.parse(date),dt1.parse(date2));

				Iterator<QRCode> iterator = qrList.iterator();
				List<BulkQRResponse> qrCodes = new ArrayList<BulkQRResponse>();
				while (iterator.hasNext()) {
					QRCode q = iterator.next();
					BulkQRResponse qrResponse= new BulkQRResponse(q);
					qrCodes.add(qrResponse);
				}

				json = new Gson().toJson(qrCodes);
				res.setData(json);
			}
			res.setStatus(resourse.getString(SUCCESS));
			resDetails.setCode(resourse.getString("load.qrcodes.success.code"));
			resDetails.setDescription(resourse.getString("load.qrcodes.success"));
			res.setMessage(resDetails);

			long endTime = System.currentTimeMillis();
			logger.info("QRServiceImpl Service ENDED getBulkQRDetails | "
					+ request.toString() + " | status=" + res.getStatus()
					+ " | message=" + res.getMessage() + " | avg_resp="
					+ (endTime - startTime));
			return res;
		} catch (Exception e) {
			logger.error("QRServiceImpl Service ERROR getBulkQRDetails | "
					+ request.toString() + " | message=" + e.getMessage());
			res.setStatus(resourse.getString(FAILED));
			resDetails.setCode(resourse.getString("load.qrcode.failed.code"));
			resDetails.setDescription(resourse.getString("load.qrcode.failed"));
			res.setMessage(resDetails);
			res.setData(null);
			return res;
		}
	}
	
	@Override
	public QueryQRCodesResponse getQueryQRCodes(QueryQRCodesRequest req) {
		QueryQRCodesResponse response = null;
		String json = "";
		try {
			long startTime = System.currentTimeMillis();
			logger.info("QRServiceImpl Service getQueryQRCodes Started | "
					+ req.toString());
			List<QueryQRCodes> qrList = null;
			qrList = qrRepository.queryQRCodes(String.valueOf(req.getCode_id()),
					req.getQr_serial().toLowerCase(), req.getName().toLowerCase(), req.getLocation().toLowerCase(),
					req.getCounter().toLowerCase(), req.getOther().toLowerCase());
			json = new Gson().toJson(qrList);
			response = new QueryQRCodesResponse(qrList.size(), json);

			long endTime = System.currentTimeMillis();
			logger.info("QRServiceImpl Service getQueryQRCodes Ended | "
					+ req.toString() + " | avg_resp=" + (endTime - startTime));
			return response;
		} catch (Exception e) {
			logger.error("QRServiceImpl Service getQueryQRCodes Error | "
					+ req.toString() + " | message=" + e.getMessage());
			return response;
		}
	}
	
	@Transactional
	@Override
	public TagQrResponse doTagQR(TagQRRequest req) {
		TagQrResponse response = new TagQrResponse();
		TagQrResponseDetails resDetails = new TagQrResponseDetails();
		boolean success = true;
		String json = "";
		try {
			long startTime = System.currentTimeMillis();
			logger.info("QRServiceImpl Service doTagQR Started | "
					+ req.toString() + " | size=" + req.getCodes().size());
			ArrayList<TagQRRequestDetails> qrList = req.getCodes();
			ArrayList<TagQRErrorResponse> errorArray = new ArrayList<TagQRErrorResponse>();
			Iterator<TagQRRequestDetails> iterator = qrList.iterator();
			while (iterator.hasNext()) {
				TagQRRequestDetails q = iterator.next();
				if (q.getTag() != null) {
					if(appRepository.checkAppCode(q.getTag().toLowerCase())!= null){
						int i = qrRepository.tagQRToApp(q.getTag().toLowerCase(),q.getQr_serial());
						if (i == 0) {
							success = false;
							logger.error("QRServiceImpl Service doTagQR Error | "
									+ req.toString() + " | message=Invalid qr serial" + q.getQr_serial());
							TagQRErrorResponse error = new TagQRErrorResponse(
									q.getQr_serial(), "Invalid qr serial");
							errorArray.add(error);
						}
					}else{
						success = false;
						logger.error("QRServiceImpl Service doTagQR Error | "
								+ req.toString() + " | message=Invalid appcode" + q.getTag());
						TagQRErrorResponse error = new TagQRErrorResponse(
								q.getQr_serial(), "Invalid appcode");
						errorArray.add(error);
					}
				} else {
					int i = qrRepository.untagQRToApp(null, q.getQr_serial());
					if (i == 0) {
						success = false;
						logger.error("QRServiceImpl Service doTagQR Error | "
								+ req.toString() + " | message=Invalid qr serial" + q.getQr_serial());
						TagQRErrorResponse error = new TagQRErrorResponse(
								q.getQr_serial(), "Invalid qr serial");
						errorArray.add(error);
					}
				}
			}

			if (success) {
				resDetails.setSucces(true);
				resDetails.setMessage("Codes updated");
			} else {
				resDetails.setSucces(false);
				resDetails.setErrors(errorArray);
			}
			json = new Gson().toJson(resDetails);
			response = new TagQrResponse(json);

			long endTime = System.currentTimeMillis();
			logger.info("QRServiceImpl Service doTagQR Ended | "
					+ req.toString() + " | avg_resp=" + (endTime - startTime));
			return response;
		} catch (Exception e) {
			logger.error("QRServiceImpl Service doTagQR Error | "
					+ req.toString() + " | message=" + e.getMessage());
			return response;
		}
	}

	public Document generatePDF(byte[][] qrArray, int size, String filename) {
		Document doc = new Document();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(doc, stream);
			// PdfWriter.getInstance(doc, new FileOutputStream("QR_Info.pdf"));
			doc.open();
			doc.add(new Paragraph("QR Code Inofrmation"));
			for (int i = 0; i < size; i++) {
				doc.newPage();
				BufferedImage img = ImageIO.read(new ByteArrayInputStream(
						qrArray[i]));
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ImageIO.write(img, "png", baos);
				Image iTextImage = Image.getInstance(baos.toByteArray());
				doc.add(iTextImage);
			}
		} catch (DocumentException | IOException e) {
			e.printStackTrace();
		} finally {
			doc.close();
		}
		try {
			FileOutputStream fos = new FileOutputStream("files\\" + filename
					+ ".pdf");
			fos.write(stream.toByteArray());
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return doc;
	}

	public static void fileUrl(String fAddress, String localFileName,
			String destinationDir) {

		// localFileName = "Hello World";
		OutputStream outStream = null;
		URLConnection uCon = null;

		InputStream is = null;
		try {
			URL url;
			byte[] buf;
			int byteRead, byteWritten = 0;
			url = new URL(fAddress);
			outStream = new BufferedOutputStream(new FileOutputStream(
					destinationDir + "\\" + localFileName));

			uCon = url.openConnection();
			is = uCon.getInputStream();
			buf = new byte[10000];
			while ((byteRead = is.read(buf)) != -1) {
				outStream.write(buf, 0, byteRead);
				byteWritten += byteRead;
			}
			System.out.println("Downloaded Successfully.");
			System.out.println("File name:\"" + localFileName
					+ "\"\nNo ofbytes :" + byteWritten);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
				outStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void fileDownload(String fAddress, String destinationDir) {
		int slashIndex = fAddress.lastIndexOf('/');
		int periodIndex = fAddress.lastIndexOf('.');

		String fileName = fAddress.substring(slashIndex + 1);

		if (periodIndex >= 1 && slashIndex >= 0
				&& slashIndex < fAddress.length() - 1) {
			fileUrl(fAddress, fileName, destinationDir);
		} else {
			System.err.println("path or file name.");
		}
	}

	private ClientHttpRequestFactory getClientHttpRequestFactory() {
		int timeout = Integer.parseInt(resourse
				.getString("notify.request.timeout"));
		HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
		clientHttpRequestFactory.setConnectTimeout(timeout);
		return clientHttpRequestFactory;
	}

	@Override
	public byte[] generateExcel(String mID, String from, String to) {
		byte[] bytes=null;
		List<QRCode> qrList = null;
		try {
			QRMerchant merchant = merchantRepository.getMerchantbyName(mID.toUpperCase().trim());
			if (null == merchant) {
				
			} else {
				if(from.equals("")||from.equals(null)||from.equals("-")||
						to.equals("")||to.equals(null)||to.equals("-")){
					qrList = qrRepository.findByMemId(merchant.getMerchantId());
				}else{
				SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
				Calendar c = Calendar.getInstance();
				c.setTime(dt1.parse(to));
				c.add(Calendar.DATE, 1);  // number of days to add
				String to2 = dt1.format(c.getTime());
				qrList = qrRepository.getQRCodesByDate(
						merchant.getMerchantId(), dt1.parse(from),
						dt1.parse(to2));
				}
			}
		
		if(true){
			
			
            HSSFWorkbook workBook = new HSSFWorkbook();
            HSSFSheet sheet = workBook.createSheet();
            
            HSSFRow reportHeader = sheet.createRow(0);
            reportHeader.createCell((short)3).setCellValue("Merchant Report");
            
            HSSFRow merchantName = sheet.createRow(2);
            merchantName.createCell((short)0).setCellValue("Merchant Name");
            merchantName.createCell((short)1).setCellValue(mID);
            
            HSSFRow dateFrom = sheet.createRow(3);
            dateFrom.createCell((short)0).setCellValue("From Date");
            dateFrom.createCell((short)1).setCellValue(from);
            
            HSSFRow dateTo = sheet.createRow(4);
            dateTo.createCell((short)0).setCellValue("To Date");
            dateTo.createCell((short)1).setCellValue(to);
            
            
            HSSFRow headingRow = sheet.createRow(7);
            headingRow.createCell((short)0).setCellValue("No");
            headingRow.createCell((short)1).setCellValue("QR Code ID");
            headingRow.createCell((short)2).setCellValue("QR Serial");
            headingRow.createCell((short)3).setCellValue("Merchant Alias");
            headingRow.createCell((short)4).setCellValue("Address");
            headingRow.createCell((short)5).setCellValue("Location");
            headingRow.createCell((short)6).setCellValue("Status");
            headingRow.createCell((short)7).setCellValue("Created Date");
            
            Iterator<QRCode> iterator = qrList.iterator();
            int index=1;
			int rowNum = 8;
			while (iterator.hasNext()) {
				QRCode q = iterator.next();
				Row row = sheet.createRow(rowNum);
				 row.createCell(0).setCellValue(index);
				 row.createCell(1).setCellValue(q.getQrId());
				 row.createCell(2).setCellValue(q.getQrContent());
				 row.createCell(3).setCellValue(merchant.getAlias());
				 row.createCell(4).setCellValue(merchant.getAddress());
				 row.createCell(5).setCellValue(q.getLocaton());
				 String status=null;
				 if(q.getStatus()==1){
					 status="Active";
				 }else{
					 status="Inactive";
				 }
				 row.createCell(6).setCellValue(status);
				 DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				 row.createCell(7).setCellValue(dateFormat.format(q.getAddedDate()));
				 rowNum++;index++;
			}
			
			Font font1 = workBook.createFont();
			font1.setBoldweight(Font.BOLDWEIGHT_BOLD);
			
			CellStyle reportNameStyle = workBook.createCellStyle();
			reportNameStyle.setFont(font1);
			reportNameStyle.setAlignment(CellStyle.ALIGN_CENTER);
			
			CellStyle headerStyle = workBook.createCellStyle();
			headerStyle.setFont(font1);
			headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
			headerStyle.setBorderBottom(CellStyle.BORDER_MEDIUM);
			headerStyle.setBorderTop(CellStyle.BORDER_MEDIUM);
			headerStyle.setBorderRight(CellStyle.BORDER_MEDIUM);
			headerStyle.setBorderLeft(CellStyle.BORDER_MEDIUM);
			
			
			CellStyle tableData = workBook.createCellStyle();
			tableData.setBorderBottom(CellStyle.BORDER_THIN);
			tableData.setBorderTop(CellStyle.BORDER_THIN);
			tableData.setBorderRight(CellStyle.BORDER_THIN);
			tableData.setBorderLeft(CellStyle.BORDER_THIN);
			
			reportHeader.getCell((short)3).setCellStyle(reportNameStyle);
			
			for (int i = 0; i < 8; i++) {// For each column
				sheet.autoSizeColumn(i);
			}
			for (int i = 0; i < headingRow.getLastCellNum(); i++) {// For each cell in the row
				headingRow.getCell(i).setCellStyle(headerStyle);
			}
			
			for (int k = 8; k < rowNum; k++) {// For each row
				for (int i = 0; i < sheet.getRow(k).getLastCellNum(); i++) {// For each cell in the row
					sheet.getRow(k).getCell(i).setCellStyle(tableData);
				}
			}
			
			for (int k = 2; k < 5; k++) {// For each row
				for (int i = 0; i < sheet.getRow(k).getLastCellNum(); i++) {// For each cell in the row
					sheet.getRow(k).getCell(i).setCellStyle(tableData);
				}
			}

            try{
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                try {
                	workBook.write(bos);
                } finally {
                    bos.close();
                }
                bytes = bos.toByteArray();
                
            }catch(FileNotFoundException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            }
            
        }
		} catch (Exception e) {
			logger.error("QRServiceImpl Service ERROR getQRCodes | "
					+ " | message=" + e.getMessage());
		}
		return bytes;
	}
	
	
	@Override
	public byte[] generateBulkQRExcelFile(String from, String to) {
		byte[] bytes = null;
		List<QRCode> qrList = null;
		try {

			if (from.equals("") || from.equals(null) || from.equals("-")
					|| to.equals("") || to.equals(null) || to.equals("-")) {
				//qrList = qrRepository.findByMemId(merchant.getMerchantId());
			} else {
				SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
				Calendar c = Calendar.getInstance();
				c.setTime(dt1.parse(to));
				c.add(Calendar.DATE, 1);  // number of days to add
				String to2 = dt1.format(c.getTime());
				qrList = qrRepository.getBulkQRCodesByDate(dt1.parse(from),dt1.parse(to2));
			}


				HSSFWorkbook workBook = new HSSFWorkbook();
				HSSFSheet sheet = workBook.createSheet();

				HSSFRow reportHeader = sheet.createRow(0);
				reportHeader.createCell((short) 3).setCellValue(
						"Bulk QR Report");


				HSSFRow dateFrom = sheet.createRow(2);
				dateFrom.createCell((short) 0).setCellValue("From Date");
				dateFrom.createCell((short) 1).setCellValue(from);

				HSSFRow dateTo = sheet.createRow(3);
				dateTo.createCell((short) 0).setCellValue("To Date");
				dateTo.createCell((short) 1).setCellValue(to);

				HSSFRow headingRow = sheet.createRow(6);
				headingRow.createCell((short) 0).setCellValue("No");
				headingRow.createCell((short) 1).setCellValue("QR Code ID");
				headingRow.createCell((short) 2).setCellValue("System");
				headingRow.createCell((short) 3).setCellValue("Customer ID");
				headingRow.createCell((short) 4).setCellValue("Tagged?");
				headingRow.createCell((short) 5).setCellValue("Generated By");
				headingRow.createCell((short) 6).setCellValue("Generated At");

				Iterator<QRCode> iterator = qrList.iterator();
				int index = 1;
				int rowNum = 7;
				while (iterator.hasNext()) {
					QRCode q = iterator.next();
					Row row = sheet.createRow(rowNum);
					row.createCell(0).setCellValue(index);
					row.createCell(1).setCellValue(q.getQrId());
					row.createCell(2).setCellValue("");
					row.createCell(3).setCellValue(q.getAlias());
					String tagged;
					if(q.getAppCode()==null){
						tagged="No";
					}else{
						tagged="Yes";
					}
					row.createCell(4).setCellValue(tagged);
					row.createCell(5).setCellValue(q.getAddedBy());
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
					row.createCell(6).setCellValue(dateFormat.format(q.getAddedDate()));
					rowNum++;
					index++;
				}

				Font font1 = workBook.createFont();
				font1.setBoldweight(Font.BOLDWEIGHT_BOLD);

				CellStyle reportNameStyle = workBook.createCellStyle();
				reportNameStyle.setFont(font1);
				reportNameStyle.setAlignment(CellStyle.ALIGN_CENTER);

				CellStyle headerStyle = workBook.createCellStyle();
				headerStyle.setFont(font1);
				headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
				headerStyle.setBorderBottom(CellStyle.BORDER_MEDIUM);
				headerStyle.setBorderTop(CellStyle.BORDER_MEDIUM);
				headerStyle.setBorderRight(CellStyle.BORDER_MEDIUM);
				headerStyle.setBorderLeft(CellStyle.BORDER_MEDIUM);

				CellStyle tableData = workBook.createCellStyle();
				tableData.setBorderBottom(CellStyle.BORDER_THIN);
				tableData.setBorderTop(CellStyle.BORDER_THIN);
				tableData.setBorderRight(CellStyle.BORDER_THIN);
				tableData.setBorderLeft(CellStyle.BORDER_THIN);

				reportHeader.getCell((short) 3).setCellStyle(reportNameStyle);

				for (int i = 0; i < 7; i++) {// For each column
					sheet.autoSizeColumn(i);
				}
				for (int i = 0; i < headingRow.getLastCellNum(); i++) {// For
																		// each
																		// cell
																		// in
																		// the
																		// row
					headingRow.getCell(i).setCellStyle(headerStyle);
				}

				for (int k = 7; k < rowNum; k++) {// For each row
					for (int i = 0; i < sheet.getRow(k).getLastCellNum(); i++) {// For
																				// each
																				// cell
																				// in
																				// the
																				// row
						sheet.getRow(k).getCell(i).setCellStyle(tableData);
					}
				}

				for (int k = 2; k < 4; k++) {// For each row
					for (int i = 0; i < sheet.getRow(k).getLastCellNum(); i++) {// For
																				// each
																				// cell
																				// in
																				// the
																				// row
						sheet.getRow(k).getCell(i).setCellStyle(tableData);
					}
				}

				try {
					ByteArrayOutputStream bos = new ByteArrayOutputStream();
					try {
						workBook.write(bos);
					} finally {
						bos.close();
					}
					bytes = bos.toByteArray();

				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

		
		} catch (Exception e) {
			logger.error("QRServiceImpl Service ERROR getQRCodes | "
					+ " | message=" + e.getMessage());
		}
		return bytes;
	}
	
	@Override
	public byte[] generateQrPDF(byte[] qrArray, String mName, String qrID,String location) {
		Document doc = new Document();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(doc, stream);
			// PdfWriter.getInstance(doc, new FileOutputStream("QR_Info.pdf"));
			doc.open();
			doc.add(new Paragraph(qrID));
			BufferedImage img = ImageIO.read(new ByteArrayInputStream(qrArray));
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(img, "png", baos);
			Image iTextImage = Image.getInstance(baos.toByteArray());
			doc.add(iTextImage);
			doc.add(new Paragraph(mName));
			doc.add(new Paragraph(location));
		} catch (DocumentException | IOException e) {
			e.printStackTrace();
		} finally {
			doc.close();
		}
		return stream.toByteArray();
	}
	
	private String setAlipayModifyPayload(Request qrReq, QRMerchant qrMerchant, QRCode qrCode) {
		String bizDataJsonString;
		Map<String, String> sParaTemp = new HashMap<String, String>();
		AlipaySubmit alipaySubmit = new AlipaySubmit();
		String sHtmlText = null;
		try {
			AliPayRequest aliPayRequest = new AliPayRequest();
			logger.debug("Begin method setAlipayPayload = request : " + qrReq.toString() + " | QR code URL : "
					+ qrCode.getQr_code_url());
			if (qrReq.getQrAlias() != null && (!qrReq.getQrAlias().isEmpty())) {
				aliPayRequest.setSecondaryMerchantName(qrReq.getQrAlias());
			} else {
				aliPayRequest.setSecondaryMerchantName(qrMerchant.getAlias());
			}			
			aliPayRequest.setSecondaryMerchantId(qrMerchant.getMerchantId());
			aliPayRequest.setSecondaryMerchantIndustry(MerchantType.values()[qrMerchant.getType()].getDescription());
			aliPayRequest.setStoreId(qrReq.getStoreId());
			aliPayRequest.setStoreName(qrReq.getStoreName());
			aliPayRequest.setTransCurrency(AlipayConfig.currency_type);
			aliPayRequest.setCurrency(AlipayConfig.currency_type);
			aliPayRequest.setCountryCode(AlipayConfig.country_code);
			aliPayRequest.setAddress(qrMerchant.getAddress());
			ObjectMapper msp = new ObjectMapper();

			bizDataJsonString = msp.writeValueAsString(aliPayRequest);

			String biz_type = new String("OVERSEASHOPQRCODE");
			sParaTemp.put("service", "alipay.commerce.qrcode.modify");
			sParaTemp.put("partner", AlipayConfig.partner);
			sParaTemp.put("_input_charset", AlipayConfig.input_charset);
			sParaTemp.put("notify_url", AlipayConfig.notify_url);
			sParaTemp.put("biz_type", biz_type);
			sParaTemp.put("biz_data", bizDataJsonString);
			sParaTemp.put("timestamp", formatter.format(new Date()));
			sParaTemp.put("qrcode", qrCode.getQr_code_url());

			logger.info(" QRServiceImpl |updateStaticQR | BEFORE SUBMIT " + sParaTemp.values());
			sHtmlText = alipaySubmit.buildRequest("", "", sParaTemp);
			logger.info(" QRServiceImpl |updateStaticQR | AFTER SUBMIT " + sHtmlText);

		} catch (JsonProcessingException e) {
			logger.debug("Error Occured in method setAlipayPayload. Error :" + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			logger.debug("Error Occured in method setAlipayPayload. Error :" + e.getMessage());
			e.printStackTrace();
		}
		logger.debug("End method setAlipayPayload");
		return sHtmlText;
	}

	private String setAlipayModifyStatusPayload(QRCode qrCode,String status) {
		Map<String, String> sParaTemp = new HashMap<String, String>();
		AlipaySubmit alipaySubmit = new AlipaySubmit();
		String sHtmlText = null;
		try {
			logger.debug("Begin method setAlipayPayload = QR code URL : " + qrCode.getQr_code_url());

			String biz_type = new String("OVERSEASHOPQRCODE");
			sParaTemp.put("service", "alipay.commerce.qrcode.modifyStatus");
			sParaTemp.put("partner", AlipayConfig.partner);
			sParaTemp.put("_input_charset", AlipayConfig.input_charset);
			sParaTemp.put("notify_url", AlipayConfig.notify_url);
			sParaTemp.put("biz_type", biz_type);
			sParaTemp.put("timestamp", formatter.format(new Date()));
			sParaTemp.put("qrcode", qrCode.getQr_code_url());
			sParaTemp.put("status", status);
			
			logger.info(" QRServiceImpl |updateStaticQR | BEFORE SUBMIT " + sParaTemp.values());
			sHtmlText = alipaySubmit.buildRequest("", "", sParaTemp);
			logger.info(" QRServiceImpl |updateStaticQR | AFTER SUBMIT " + sHtmlText);

		} catch (Exception e) {
			logger.debug("Error Occured in method setAlipayModifyStatusPayload. Error :" + e.getMessage());
			e.printStackTrace();
		}
		logger.debug("End method setAlipayPayload");
		return sHtmlText;
	}
	
	public static String getFinalURL(String url) throws IOException {
	    HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
	    con.setInstanceFollowRedirects(false);
	    con.connect();
	    con.getInputStream();

	    if (con.getResponseCode() == HttpURLConnection.HTTP_MOVED_PERM || con.getResponseCode() == HttpURLConnection.HTTP_MOVED_TEMP) {
	        String redirectUrl = con.getHeaderField("Location");
	        return getFinalURL(redirectUrl);
	    }
	    return url;
	}
}
