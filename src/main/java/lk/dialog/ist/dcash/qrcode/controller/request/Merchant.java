package lk.dialog.ist.dcash.qrcode.controller.request;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class Merchant {

	private String name;
	private String regName;
	private String alias;
	private String type;
	private String address;
	private String location;
	private String brc;
	private String nic;
	private String cpName;
	private String contactNumber;
	private String email;
	private String other;
	private String status;
	private String logo=null;
	private String latitude;
	private String longitude;
	private String addedBy;
	private String modifiedBy;
	@JsonIgnore
	private String key;
	private Date modifiedDate;
	private String modifiedReason;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRegName() {
		return regName;
	}
	public void setRegName(String regName) {
		this.regName = regName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAddedBy() {
		return addedBy;
	}
	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getModifiedReason() {
		return modifiedReason;
	}
	public void setModifiedReason(String modifiedReason) {
		this.modifiedReason = modifiedReason;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getBrc() {
		return brc;
	}
	public void setBrc(String brc) {
		this.brc = brc;
	}
	public String getNic() {
		return nic;
	}
	public void setNic(String nic) {
		this.nic = nic;
	}
	public String getCpName() {
		return cpName;
	}
	public void setCpName(String cpName) {
		this.cpName = cpName;
	}
	public String getOther() {
		return other;
	}
	public void setOther(String other) {
		this.other = other;
	}

	@Override
	public String toString() {
		return "Merchant [name=" + name + ", regName=" + regName + ", alias="
				+ alias + ", type=" + type + ", address=" + address
				+ ", location=" + location + ", brc=" + brc + ", nic=" + nic
				+ ", cpName=" + cpName + ", contactNumber=" + contactNumber
				+ ", email=" + email + ", other=" + other + ", status="
				+ status + ", logo=" + logo + ", latitude=" + latitude
				+ ", longitude=" + longitude + ", addedBy=" + addedBy
				+ ", key=" + key + ", modifiedBy=" + modifiedBy
				+ ", modifiedDate=" + modifiedDate + ", modifiedReason="
				+ modifiedReason + "]";
	}
}
