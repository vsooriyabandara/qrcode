package lk.dialog.ist.dcash.qrcode.db.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import lk.dialog.ist.dcash.qrcode.db.modal.DynamicQRCode;

/**
 * Dynamic QR Code Repository
 * @author Veranga Sooriyabandara
 * @since 13-11-2017
 * @version 1.0
 *
 */
public interface DynamicQRCodeRepository extends CrudRepository<DynamicQRCode,Long>{
	@Modifying
	@Query("update DynamicQRCode set generatedQRCode = ?1 where id = ?2")
	void updateFormattedId(String generatedQRCode, long id);
	
	@Modifying
	@Query("update DynamicQRCode set qrImage = ?1 where id = ?2")
	void updateQRImageURL(byte[] qrImage, long id);
	
	@Modifying
	@Query("update DynamicQRCode set qrImage = ?1 , qrContent =?2 ,requestTime =?3 where generatedQRCode = ?4")
	void updateQRContent(byte[] qrImage, String qrContent ,Date  requestTime , String generatedQRCode);
	
	
	@Query("select a from DynamicQRCode a where generatedQRCode=?1" )
	DynamicQRCode getDynamicQrCode(String generatedQRCode);
	
	@Query("select a from DynamicQRCode a where generatedQRCode=?1 and appCode=?2" )
	DynamicQRCode checkAvailability(String generatedQRCode , String appCode);
}
