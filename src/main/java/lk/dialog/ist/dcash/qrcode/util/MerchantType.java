package lk.dialog.ist.dcash.qrcode.util;

public enum MerchantType {
	NS(""),
	CAFE("5812"),
	GROCERY("5311"),
	HOTEL("7011"),
	TAXI("4121"),
	OTHER("5311");
	private String description;
		
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	MerchantType(String description){
		this.description = description;
	}
}
