package lk.dialog.ist.dcash.qrcode.controller;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.validation.ConstraintViolation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lk.dialog.ist.dcash.qrcode.controller.request.Merchant;
import lk.dialog.ist.dcash.qrcode.controller.response.Response;
import lk.dialog.ist.dcash.qrcode.controller.response.Response1;
import lk.dialog.ist.dcash.qrcode.controller.response.ResponseDetails;
import lk.dialog.ist.dcash.qrcode.controller.response.ResponseObject;
import lk.dialog.ist.dcash.qrcode.db.modal.QRMerchant;
import lk.dialog.ist.dcash.qrcode.service.MerchantService;
import lk.dialog.ist.dcash.qrcode.util.MerchantType;

@RestController
@CrossOrigin(origins = "*")
public class MerchantController {
	
	@Autowired
	MerchantService merchantService;
	
	protected static ResourceBundle resourse = ResourceBundle.getBundle("data");
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping(value = "/merchant/onBoardMerchant", method = RequestMethod.POST)
    public ResponseEntity<ResponseObject> onBoardMerchant(@RequestBody Merchant merchantRequest){
		long startTime = System.currentTimeMillis();
		logger.info("MerchantController API Service STARTED onBoardMerchant | " +merchantRequest.toString());
        ResponseEntity<ResponseObject> responseEntity = null;
        ResponseObject response = new ResponseObject();
        ResponseDetails resDetails = new ResponseDetails();
        try {  
        	BigDecimal lat;
        	try {
        		if(merchantRequest.getLatitude()!=null && merchantRequest.getLatitude() != "") {
        			lat=new BigDecimal(merchantRequest.getLatitude());
        		}
        		else {
        			lat = null;
        		}
        	}catch(NumberFormatException e) {
        		throw new Exception("lat");
        	}
        	BigDecimal lon;
        	try {
        		if(merchantRequest.getLongitude()!=null && merchantRequest.getLongitude() != "") {
        			lon = new BigDecimal(merchantRequest.getLongitude());
        		}
        		else {
        			lon = null;
        		}
        		
        	}catch(NumberFormatException e) {
        		throw new Exception("lon");
        	}
        	
        	QRMerchant merchant = new QRMerchant(merchantRequest);
        	merchant.setLatitude(lat);
        	merchant.setLongitude(lon);
        	String type=merchantRequest.getType();
        	int mType=0;
        	if(type.equalsIgnoreCase(MerchantType.CAFE.name())) {
        		mType =MerchantType.CAFE.ordinal();
        	}else if(type.equalsIgnoreCase(MerchantType.GROCERY.name())) {
        		mType =MerchantType.GROCERY.ordinal();
        	}else if(type.equalsIgnoreCase(MerchantType.HOTEL.name())) {
        		mType =MerchantType.HOTEL.ordinal();
        	}else if(type.equalsIgnoreCase(MerchantType.OTHER.name())) {
        		mType =MerchantType.OTHER.ordinal();
        	}
        	else {
        		throw new Exception("merchant_type");
        	}
        	merchant.setType(mType);
        	response = merchantService.addMerchant(merchant,merchantRequest.getLogo());
        }catch(javax.validation.ConstraintViolationException e) {
        	logger.error("MerchantController API Service ERROR onBoardMerchant | " +merchantRequest.toString()+
        			" | message=" + e.getMessage());
        	response.setStatus(resourse.getString("failed"));
        	resDetails.setCode(resourse.getString("merchant.invalid.input.code"));
			String inputErrors = "";
        	Set<ConstraintViolation<?>> list =e.getConstraintViolations();
        	for (ConstraintViolation<?> s : list) {
        		String condition =s.getPropertyPath().toString();
        		if(condition.equals("name")) {
        			inputErrors=inputErrors+resourse.getString("merchant.invalid.name")+". ";
        		}else if(condition.equals("regName")) {
        			inputErrors=inputErrors+resourse.getString("merchant.invalid.regname")+". ";
        		}else if(condition.equals("address")) {
        			inputErrors=inputErrors+resourse.getString("merchant.invalid.address")+". ";
        		}else if(condition.equals("contactNumber")) {
        			inputErrors=inputErrors+resourse.getString("merchant.invalid.contact.no")+". ";
        		}else if(condition.equals("email")) {
        			inputErrors=inputErrors+resourse.getString("merchant.invalid.email")+". ";
        		}else if(condition.equals("alias")) {
        			inputErrors=inputErrors+resourse.getString("merchant.invalid.alias")+". ";
        		}
        	}     
        	resDetails.setDescription(inputErrors);
        	response.setMessage(resDetails);
			response.setData(null);
        }catch(Exception e) {
        	logger.error("MerchantController API Service ERROR onBoardMerchant | " +merchantRequest.toString()+ 
        			" | message=" + e.getMessage());
        	response.setStatus(resourse.getString("failed"));
			switch (e.getMessage()) {
				case "lat":
					resDetails.setCode(resourse.getString("merchant.invalid.input.code"));
					resDetails.setDescription(resourse.getString("merchant.invalid.lat"));
					break;
				case "lon":
					resDetails.setCode(resourse.getString("merchant.invalid.input.code"));
					resDetails.setDescription(resourse.getString("merchant.invalid.lon"));
					break;
				case "merchant_type":
					resDetails.setCode(resourse.getString("merchant.invalid.input.code"));
					resDetails.setDescription(resourse.getString("merchant.invalid.type"));
					break;
				case "alias":
					resDetails.setCode(resourse.getString("merchant.invalid.input.code"));
					resDetails.setDescription(resourse.getString("merchant.invalid.alias"));
					break;
	            default :
	            	resDetails.setCode(resourse.getString("merchant.invalid.input.code"));
	            	resDetails.setDescription(resourse.getString("add.merchant.failed"));
	    			break;
			}
			response.setMessage(resDetails);
			response.setData(null);
        }
        responseEntity = ResponseEntity.status(HttpStatus.OK).body(response);
        long endTime = System.currentTimeMillis();
        logger.info("MerchantController API Service ENDED onBoardMerchant | " +merchantRequest.toString()+
        		" | status="+response.getStatus()+" | message="+response.getMessage()+" | avg_resp=" + (endTime - startTime));
        return responseEntity;
    }
	
	@RequestMapping(value = "/merchant/getMerchants", method = RequestMethod.POST)
	public ResponseEntity<ResponseObject> getMerchants(Merchant merchant) {
		long startTime = System.currentTimeMillis();
		ResponseObject response = new ResponseObject();
		ResponseDetails resDetails = new ResponseDetails();
		
		logger.info("QRController API Service STARTED getMerchants");
		ResponseEntity<ResponseObject> responseEntity = null;
		try {
			response = merchantService.getMerchants();
			logger.info(response.getData());
			
		} catch (Exception ex) {
			response.setStatus(resourse.getString("failed"));
			resDetails.setCode(resourse.getString("merchants.retreived.failed.code"));
        	resDetails.setDescription(resourse.getString("merchants.retreived.failed"));
        	response.setMessage(resDetails);
			response.setData(null);
			logger.error("QRController API Service ERROR getMerchants"
					+ " | message=" + ex.getMessage());
			//responseEntity = ResponseEntity.status(HttpStatus.OK).body(response);
		}
		responseEntity = ResponseEntity.status(HttpStatus.OK).body(response);
		long endTime = System.currentTimeMillis();
		logger.info("QRController API Service ENDED getMerchants"
				 + " | avg_resp=" + (endTime - startTime));
		return responseEntity;
	}
	
	@RequestMapping(value = "/merchant/getMerchantDetails", method = RequestMethod.POST)
	public ResponseEntity<ResponseObject> getMerchantDetails(@RequestBody Merchant merchant) {
		long startTime = System.currentTimeMillis();
		ResponseObject response = new ResponseObject();
		ResponseDetails resDetails = new ResponseDetails();
		
		logger.info("QRController API Service STARTED getMerchantDetails Name="+merchant.getName());
		ResponseEntity<ResponseObject> responseEntity = null;
		try {
			response = merchantService.getMerchantDetails(merchant.getName().trim());
			logger.info(merchant.toString());
			
		} catch (Exception ex) {
			logger.error("QRController API Service ERROR getMerchantDetails Name="+merchant.getName()
					+ " | message=" + ex.getMessage());
        	response.setStatus(resourse.getString("failed"));
        	resDetails.setCode(resourse.getString("merchant.details.retreived.failed.code"));
        	resDetails.setDescription(resourse.getString("merchant.details.retreived.failed"));
        	response.setMessage(resDetails);
			response.setData(null);
			//responseEntity = ResponseEntity.status(HttpStatus.OK).body(response);
		}
		responseEntity = ResponseEntity.status(HttpStatus.OK).body(response);
		long endTime = System.currentTimeMillis();
		logger.info("QRController API Service ENDED getMerchantDetails Name="+merchant.getName()
				 + " | avg_resp=" + (endTime - startTime));
		return responseEntity;
	}
	
	@RequestMapping(value = "/merchant/updateMerchant", method = RequestMethod.POST)
    public ResponseEntity<ResponseObject> updateMerchant(@RequestBody Merchant merchantRequest){
		long startTime = System.currentTimeMillis();
		logger.info("MerchantController API Service STARTED updateMerchant | " +merchantRequest.toString());
		ResponseObject response = new ResponseObject();
		ResponseDetails resDetails = new ResponseDetails();
        ResponseEntity<ResponseObject> responseEntity = null;

        try {  
        	BigDecimal lat;
        	try {
        		if(merchantRequest.getLatitude()!=null && merchantRequest.getLatitude() != "") {
        			lat=new BigDecimal(merchantRequest.getLatitude());
        		}
        		else {
        			lat = BigDecimal.ZERO;
        		}
        	}catch(NumberFormatException e) {
        		throw new Exception("lat");
        	}
        	BigDecimal lon;
        	try {
        		if(merchantRequest.getLongitude()!=null && merchantRequest.getLongitude() != "") {
        			lon = new BigDecimal(merchantRequest.getLongitude());
        		}
        		else {
        			lon = BigDecimal.ZERO;
        		}
        		
        	}catch(NumberFormatException e) {
        		throw new Exception("lon");
        	}
        	
        	QRMerchant merchant = new QRMerchant(merchantRequest);
        	merchant.setLatitude(lat);
        	merchant.setLongitude(lon);
        	merchant.setModifiedBy(merchantRequest.getModifiedBy());
        	/*int mType=0;
        	if(type.equalsIgnoreCase(MerchantType.CAFE.name())) {
        		mType =MerchantType.CAFE.ordinal();
        	}else if(type.equalsIgnoreCase(MerchantType.GROCERY.name())) {
        		mType =MerchantType.GROCERY.ordinal();
        	}else if(type.equalsIgnoreCase(MerchantType.HOTEL.name())) {
        		mType =MerchantType.HOTEL.ordinal();
        	}else if(type.equalsIgnoreCase(MerchantType.OTHER.name())) {
        		mType =MerchantType.OTHER.ordinal();
        	}
        	else {
        		throw new Exception("merchant_type");
        	}*/
        	merchant.setType(1);
        	response = merchantService.updateMerchant(merchant,merchantRequest.getLogo());
        }catch(javax.validation.ConstraintViolationException e) {
        	logger.error("MerchantController API Service ERROR updateMerchant | " +merchantRequest.toString()+
        			" | message=" + e.getMessage());
        	response.setStatus(resourse.getString("failed"));
        	resDetails.setCode(resourse.getString("merchant.invalid.input.code"));
			String inputErrors = "";
        	Set<ConstraintViolation<?>> list =e.getConstraintViolations();
        	for (ConstraintViolation<?> s : list) {
        		String condition =s.getPropertyPath().toString();
        		if(condition.equals("name")) {
        			inputErrors=inputErrors+resourse.getString("merchant.invalid.name")+". ";
        		}else if(condition.equals("regName")) {
        			inputErrors=inputErrors+resourse.getString("merchant.invalid.regname")+". ";
        		}else if(condition.equals("address")) {
        			inputErrors=inputErrors+resourse.getString("merchant.invalid.address")+". ";
        		}else if(condition.equals("contactNumber")) {
        			inputErrors=inputErrors+resourse.getString("merchant.invalid.contact.no")+". ";
        		}else if(condition.equals("email")) {
        			inputErrors=inputErrors+resourse.getString("merchant.invalid.email")+". ";
        		}else if(condition.equals("alias")) {
        			inputErrors=inputErrors+resourse.getString("merchant.invalid.alias")+". ";
        		}
        	}     
        	resDetails.setDescription(inputErrors);
        	response.setMessage(resDetails);
			response.setData(null);
        }catch(Exception e) {
        	logger.error("MerchantController API Service ERROR updateMerchant | " +merchantRequest.toString()+ 
        			" | message=" + e.getMessage());
        	response.setStatus(resourse.getString("failed"));
			switch (e.getMessage()) {
				case "lat":
					resDetails.setCode(resourse.getString("merchant.invalid.input.code"));
					resDetails.setDescription(resourse.getString("merchant.invalid.lat"));
					break;
				case "lon":
					resDetails.setCode(resourse.getString("merchant.invalid.input.code"));
					resDetails.setDescription(resourse.getString("merchant.invalid.lon"));
					break;
				case "alias":
					resDetails.setCode(resourse.getString("merchant.invalid.input.code"));
					resDetails.setDescription(resourse.getString("merchant.invalid.alias"));
					break;
	            default :
	            	resDetails.setCode(resourse.getString("merchant.details.update.failed.code"));
	            	resDetails.setDescription(resourse.getString("merchant.details.update.failed"));
	    			break;
			}
			response.setMessage(resDetails);
			response.setData(null);
        }
        responseEntity = ResponseEntity.status(HttpStatus.OK).body(response);
        long endTime = System.currentTimeMillis();
        logger.info("MerchantController API Service ENDED updateMerchant | " +merchantRequest.toString()+
        		" | status="+response.getStatus()+" | message="+response.getMessage()+" | avg_resp=" + (endTime - startTime));
        return responseEntity;
    }

}
