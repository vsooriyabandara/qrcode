package lk.dialog.ist.dcash.qrcode.controller.response;

public class TagQRErrorResponse {
	String qr_serial;
	String message;
	
	
	
	public TagQRErrorResponse() {
		super();
	}
	public TagQRErrorResponse(String qr_serial, String message) {
		super();
		this.qr_serial = qr_serial;
		this.message = message;
	}
	public String getQr_serial() {
		return qr_serial;
	}
	public void setQr_serial(String qr_serial) {
		this.qr_serial = qr_serial;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
