package lk.dialog.ist.dcash.qrcode.controller.request;

import java.math.BigDecimal;
import java.util.Date;

public class Request {
	private String qrID;
	private String merchantID;
	private String content;
	private String size;
	private String color;
	private String qrAlias;
	private String qrLocation;
	private String counter;
	private BigDecimal qrLatitude;
	private BigDecimal qrLongitude;
	private String qrType;
	private String status;
	private Date addedDate;
	private String addedBy;
	private Date modifiedDate;
	private String modifiedBy;
	private String modifiedReason;
	private boolean logoRequired;
	private String storeId; // new for AliPay Identify a store under merchant
	private String storeName; // new for AliPay
	private String storeAddress; // new for AliPay
	private String eZCashAlias; // new for AliPay

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getAddedDate() {
		return addedDate;
	}

	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}

	public String getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public boolean isLogoRequired() {
		return logoRequired;
	}

	public void setLogoRequired(boolean logoRequired) {
		this.logoRequired = logoRequired;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getQrAlias() {
		return qrAlias;
	}

	public void setQrAlias(String qrAlias) {
		this.qrAlias = qrAlias;
	}

	public String getQrLocation() {
		return qrLocation;
	}

	public void setQrLocation(String qrLocation) {
		this.qrLocation = qrLocation;
	}

	public String getCounter() {
		return counter;
	}

	public void setCounter(String counter) {
		this.counter = counter;
	}

	public BigDecimal getQrLatitude() {
		return qrLatitude;
	}

	public void setQrLatitude(BigDecimal qrLatitude) {
		this.qrLatitude = qrLatitude;
	}

	public BigDecimal getQrLongitude() {
		return qrLongitude;
	}

	public void setQrLongitude(BigDecimal qrLongitude) {
		this.qrLongitude = qrLongitude;
	}

	public String getQrType() {
		return qrType;
	}

	public void setQrType(String qrType) {
		this.qrType = qrType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getQrID() {
		return qrID;
	}

	public void setQrID(String qrID) {
		this.qrID = qrID;
	}

	public String getModifiedReason() {
		return modifiedReason;
	}

	public void setModifiedReason(String modifiedReason) {
		this.modifiedReason = modifiedReason;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getStoreAddress() {
		return storeAddress;
	}

	public void setStoreAddress(String storeAddress) {
		this.storeAddress = storeAddress;
	}

	
	public String geteZCashAlias() {
		return eZCashAlias;
	}

	public void seteZCashAlias(String eZCashAlias) {
		this.eZCashAlias = eZCashAlias;
	}

	@Override
	public String toString() {
		return "Request [qrID=" + qrID + ", merchantID=" + merchantID + ", content=" + content + ", size=" + size
				+ ", color=" + color + ", qrAlias=" + qrAlias + ", qrLocation=" + qrLocation + ", counter=" + counter
				+ ", qrLatitude=" + qrLatitude + ", qrLongitude=" + qrLongitude + ", qrType=" + qrType + ", status="
				+ status + ", addedDate=" + addedDate + ", addedBy=" + addedBy + ", modifiedDate=" + modifiedDate
				+ ", modifiedBy=" + modifiedBy + ", modifiedReason=" + modifiedReason + ", logoRequired=" + logoRequired
				+ "]";
	}

}
