package lk.dialog.ist.dcash.qrcode.controller.request;

public class DisableQrRequest {
	private String merchantID;
	private String QrID;
	private String reason;
	
	public String getMerchantID() {
		return merchantID;
	}
	public void setMerchantID(String merchantID) {
		
		this.merchantID = merchantID;
	}
	public String getQrID() {
		return QrID;
	}
	public void setQrID(String qrID) {
		QrID = qrID;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	@Override
	public String toString() {
		return "DisableQrRequest [merchantID=" + merchantID + ", QrID=" + QrID
				+ ", reason=" + reason + "]";
	}

	
}
