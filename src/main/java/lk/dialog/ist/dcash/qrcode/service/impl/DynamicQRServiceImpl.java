package lk.dialog.ist.dcash.qrcode.service.impl;

import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.http.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.google.gson.Gson;
import com.google.zxing.WriterException;
import com.sun.net.httpserver.Authenticator.Success;
import lk.dialog.ist.dcash.qrcode.controller.request.AcknowledgeDynamicQRCodeRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.DynamicQRCodeRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.GenerateDynamicQRRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.UpdateDynamicQRCodeRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.ValidateDynamicQRCodeRequest;
import lk.dialog.ist.dcash.qrcode.controller.response.AcknowledgeDynamicQRCodeResponse;
import lk.dialog.ist.dcash.qrcode.controller.response.DynamicQRResponse;
import lk.dialog.ist.dcash.qrcode.controller.response.ResponseData;
import lk.dialog.ist.dcash.qrcode.controller.response.ResponseDetails;
import lk.dialog.ist.dcash.qrcode.controller.response.ResponseObject;
import lk.dialog.ist.dcash.qrcode.controller.response.ValidateDynamicQRResponse;
import lk.dialog.ist.dcash.qrcode.db.modal.DynamicQRCode;
import lk.dialog.ist.dcash.qrcode.db.modal.QRApplication;
import lk.dialog.ist.dcash.qrcode.db.repository.ApplicationRepository;
import lk.dialog.ist.dcash.qrcode.db.repository.DynamicQRCodeRepository;
import lk.dialog.ist.dcash.qrcode.service.DynamicQRService;
import lk.dialog.ist.dcash.qrcode.util.InvalidArgumentException;
import lk.dialog.ist.dcash.qrcode.util.QRStatus;
import lk.dialog.ist.dcash.qrcode.util.QRType;
import lk.dialog.ist.dcash.qrcode.util.QrImageProcessor;
import lk.dialog.ist.dcash.qrcode.util.QrImageProcessor2;

/**
 * Dynamic QRService Implementation
 * 
 * @author Veranga Sooriyabandara
 * @since 13-12-2017
 * @version 1.0 jira id :
 *
 */
@Service
public class DynamicQRServiceImpl implements DynamicQRService {

//	@Value("${}")
	@Autowired
	DynamicQRCodeRepository dynamicQRCodeRepository;

	@Autowired
	ApplicationRepository applicationRepository;

	@Autowired
	QrImageProcessor qrImageProcessor;

	@Autowired
	QrImageProcessor2 qrImageProcessor2;

	private final Logger logger = LoggerFactory.getLogger(getClass());

	protected static ResourceBundle resourse = ResourceBundle.getBundle("data");
	
	
	
	private static final String EMPTY_QR_CODE="EMPTY_QR_CODE";
	
	
	
	private static final String EMPTY_COLOR_CODE="EMPTY_COLOR_CODE";
	
	private static final String QR_EXPIRED="QR_EXPIRED";
	
	private static final String INVALID_COLOR_CODE="INVALID_COLOR_CODE";
	
	private static final String EMPTY_APP_CODE_AND_QR_CODE="EMPTY_APP_CODE_AND_QR_CODE";

	private static final String DESCRIPTION="description";
	
	private static final String CODE="code";
	private static final String EMPTY_QR_CONTENT="EMPTY_QR_CONTENT";
	
	private static final String FAILED="failed";
	
	private static final String INVALIDQR="INVALID_QR";
	
	private static final String COLORCODEEMPTY = "qr.dynamic.dynamicQR.colorCodeEmpty.code";
	
	private static final String COLORCODEEMPTYINFO="qr.dynamic.dynamicQR.colorCodeEmpty.info";
	private static final String EMPTY_APP_CODE="EMPTY_APP_CODE";
	private static final String  FAILEDINFO="qr.dynamic.dynamicQR.addFailed.info";
	private static final String REQUEST_TIME_EMPTY="REQUEST_TIME_EMPTY";
	/**
	 * Used to add Dynamic QR
	 */
	
	@Override
	@Transactional
	public ResponseObject generateDynamicQRString(GenerateDynamicQRRequest request) {
		logger.info("DynamicQRServiceImpl | addDynamicQRCodes | request app code| " + request.getAppCode() + " | Qr Content |" + request.getQrContent());
		// Check the avilability of the App code
		ResponseObject res = new ResponseObject();
		ResponseDetails resDetails = new ResponseDetails();
		QRApplication qrApp = applicationRepository.checkAppCode(request.getAppCode().toUpperCase());
		if (qrApp == null) {
			logger.info("DynamicQRServiceImpl | addDynamicQRCodes | appCode is not registered  " );
			res.setStatus(resourse.getString(FAILED));
			resDetails.setDescription(resourse.getString("qr.dynamic.appNotAvailable.info"));
			res.setMessage(resDetails);
			return res;
		} else {
			try {			
				if(request.getSize()==null || request.getSize().isEmpty()) {
					request.setSize(250+"");
				}
				else {
				checkQRCodeSize(request.getSize());
				}
				if(request.getColor()==null || request.getColor().isEmpty()) {
					request.setColor("#000000");
				}
				else {
					checkQRColor(request.getColor());
				}
				checkQRCodeSize(request.getSize());
				checkQRColor(request.getColor());
				boolean check = false;
				for (QRType s : QRType.values()) {
					if (request.getQrType().equals(s.name())) {
						check = true;
						break;
					}
				}
				if (!check) {
					logger.info("DynamicQRServiceImpl | addDynamicQRCodes |  requestId " + request.getRequestId() + " | Error Message INVALID_QR_TYPE");
					throw new NumberFormatException("INVALID_QR_TYPE");
				}

				DynamicQRCode dynamicQR = new DynamicQRCode(request);
				dynamicQR.setTxStatus(QRStatus.PENDING.ordinal()+"");
				dynamicQR = dynamicQRCodeRepository.save(dynamicQR);
				long tempId = dynamicQR.getId();
				String qrid = getDynamicQRId(tempId);
				String encodedString = "";
				encodedString = qrImageProcessor2.getBase64EncodedQRImage(request.getQrContent(), null, request.getColor(), Integer.parseInt(request.getSize()), Integer.parseInt(request.getSize()), false);
				byte[] decodedImg = Base64.getDecoder().decode(encodedString);
				dynamicQR.setGeneratedQRCode(qrid);
				dynamicQR.setQrImage(decodedImg);
				dynamicQRCodeRepository.save(dynamicQR);
				res.setStatus(resourse.getString("success"));
				ResponseData resData = new ResponseData();
				resData.setQR_Code_ID( qrid);
				resData.setURL(resourse.getString("qrcode.base.url")+"/dynamicQR/" + dynamicQR.getId());
				if(request.isByteStreamRequired())
					resData.setByteStream(decodedImg);
				res.setData(new Gson().toJson(resData));
				logger.info("DynamicQRServiceImpl | addDynamicQRCodes | dynamic QR Code generated Successfully " );
				return res;
				
			} catch (ParseException e) {
				logger.error("DynamicQRServiceImpl | addDynamicQRCodes | failed to generate dynamic QR Code  " );
				res.setStatus(resourse.getString(FAILED));
				resDetails.setDescription(e.toString());
				return res;
			} catch (WriterException e) {
				logger.error("DynamicQRServiceImpl | addDynamicQRCodes | failed to generate dynamic QR Code  ");
				res.setStatus(resourse.getString(FAILED));
				resDetails.setDescription(e.toString());
				return res;
			} catch (Exception e) {
				logger.error("DynamicQRServiceImpl | addDynamicQRCodes | failed to generate dynamic QR Code  ");
				res.setStatus(resourse.getString(FAILED));
				resDetails.setDescription(e.toString());
				return res;
			}
		}
	}
	
	
	@Override
	@Transactional
	public ResponseObject generateDynamicQRID(DynamicQRCodeRequest request) {
		logger.info("DynamicQRServiceImpl | addDynamicQRCodes | request app code| " + request.getAppCode() + " | Qr Content |" + request.getQrContent());
		ResponseObject res = new ResponseObject();
		ResponseDetails resDetails = new ResponseDetails();
		String json="";
		// Check the avilability of the App code
		QRApplication qrApp = applicationRepository.checkAppCode(request.getAppCode().toUpperCase());
		if (qrApp == null) {
			logger.info("DynamicQRServiceImpl | addDynamicQRCodes | appCode is not registered  " + request.getAppCode() + " | requestId " + request.getRequestId() + " qrContent " + request.getQrContent());
			res.setStatus(resourse.getString(FAILED));
			resDetails.setCode(resourse.getString("qr.dynamic.appNotAvailable.code"));
			resDetails.setDescription(resourse.getString("qr.dynamic.appNotAvailable.info"));
			res.setMessage(resDetails);
			return res;
		} else {
			try {			
				if(request.getSize()==null || request.getSize().isEmpty()) {
					request.setSize(250+"");
				}
				else {
				checkQRCodeSize(request.getSize());
				}
				if(request.getColor()==null || request.getColor().isEmpty()) {
					request.setColor("#000000");
				}
				else {
					checkQRColor(request.getColor());
				}
				checkQRCodeSize(request.getSize());
				checkExpiration(request.getExpirationInSeconds());
				checkQRColor(request.getColor());
				boolean check = false;
				for (QRType s : QRType.values()) {
					if (request.getQrType().equals(s.name())) {
						check = true;
						break;
					}
				}
				if (!check) {
					logger.info("DynamicQRServiceImpl | addDynamicQRCodes |  requestId " + request.getRequestId() + " | Error Message INVALID_QR_TYPE");
					throw new NumberFormatException("INVALID_QR_TYPE");
				}

				// Need to create an object using AutoWired neet to be implemented
				DynamicQRCode dynamicQR = new DynamicQRCode(request);
				dynamicQR.setTxStatus(QRStatus.PENDING.ordinal()+"");
				dynamicQR = dynamicQRCodeRepository.save(dynamicQR);
				long tempId = dynamicQR.getId();
				String qrid = getDynamicQRId(tempId);
				String encodedString = "";

				encodedString = qrImageProcessor2.getBase64EncodedQRImage(qrid, null, request.getColor(), Integer.parseInt(request.getSize()), Integer.parseInt(request.getSize()), false);
				byte[] decodedImg = Base64.getDecoder().decode(encodedString);
				dynamicQR.setGeneratedQRCode(qrid);
				dynamicQR.setQrImage(decodedImg);
				dynamicQRCodeRepository.save(dynamicQR);
				
				ResponseData resData = new ResponseData();
				resData.setQR_Code_ID( qrid);
				resData.setURL(resourse.getString("qrcode.base.url")+"/dynamicQR/" + dynamicQR.getId());
				if(request.isByteStreamRequired())
					resData.setByteStream(decodedImg);
				Calendar cal = Calendar.getInstance();
				cal.setTime(request.getRequestTime());
				cal.add(Calendar.SECOND, Integer.parseInt(request.getExpirationInSeconds()));
				resData.setExpiryTime(cal.getTime().toString());
				json = new Gson().toJson(resData);
				res.setData(json);	
				
				res.setStatus(resourse.getString("success"));
				resDetails.setCode(resourse.getString("qr.dynamic.dynamicQR.addSuccess.code"));
				resDetails.setDescription(resourse.getString("qr.dynamic.dynamicQR.addSuccess.info"));
				res.setMessage(resDetails);
				
				expireQRCode(qrid, (Integer.parseInt(request.getExpirationInSeconds()) * 1000) + 1000);
				
				logger.info("DynamicQRServiceImpl | addDynamicQRCodes | dynamic QR Code generated Successfully " + request.getAppCode() + " | requestId " + request.getRequestId() + " | qrid " + qrid + " | URL " + dynamicQR.getId());
				return res;
				
			} catch (ParseException e) {
				res.setStatus(resourse.getString(FAILED));
				resDetails.setCode(resourse.getString("qr.dynamic.dynamicQR.addFailed.code"));
				resDetails.setDescription( resourse.getString(FAILEDINFO));
				res.setMessage(resDetails);
				res.setData(null);
				logger.error("DynamicQRServiceImpl | addDynamicQRCodes | failed to generate dynamic QR Code  " + request.getAppCode() + " | requestId " + request.getRequestId() + " | ParseException occure " + e.getMessage(), e);
				return res;
			} catch (WriterException e) {
				res.setStatus(resourse.getString(FAILED));
				resDetails.setCode(resourse.getString("qr.dynamic.dynamicQR.addFailed.code"));
				resDetails.setDescription(resourse.getString(FAILEDINFO));
				res.setMessage(resDetails);
				res.setData(null);
				logger.error("DynamicQRServiceImpl | addDynamicQRCodes | failed to generate dynamic QR Code  " + request.getAppCode() + " | requestId " + request.getRequestId() + " | WriterException occure " + e.getMessage(), e);
				return res;
			} catch (Exception e) {
				switch (e.getMessage()) {
				case "INVALIDE_REQUEST_ID":
					res.setStatus(resourse.getString(FAILED));
					resDetails.setCode(resourse.getString("qr.dynamic.dynamicQR.invalidRequestId.code"));
					resDetails.setDescription(resourse.getString("qr.dynamic.dynamicQR.invalidRequestId.info"));
					break;
				case INVALIDQR:
					res.setStatus(resourse.getString(FAILED));
					resDetails.setCode(resourse.getString("qr.dynamic.dynamicQR.invalidQR.code"));
					resDetails.setDescription( resourse.getString("qr.dynamic.dynamicQR.invalidQR.info"));
					break;
				case "EMPTY_QR_SIZE":
					res.setStatus(resourse.getString(FAILED));
					resDetails.setCode(resourse.getString("qr.dynamic.dynamicQR.emptyQR.code"));
					resDetails.setDescription(resourse.getString("qr.dynamic.dynamicQR.emptyQR.info"));
					break;
				case "INVALID_EXPIRY_TIME":
					res.setStatus(resourse.getString(FAILED));
					resDetails.setCode(resourse.getString("qr.dynamic.dynamicQR.invalidExpriyTime.code"));
					resDetails.setDescription(resourse.getString("qr.dynamic.dynamicQR.invalidExpriyTime.info"));
					break;
				case "EXPIRY_SECONDS_EMPTY":
					res.setStatus(resourse.getString(FAILED));
					resDetails.setCode(resourse.getString("qr.dynamic.dynamicQR.emptyExpiryTime.code"));
					resDetails.setDescription(resourse.getString("qr.dynamic.dynamicQR.emptyExpiryTime.info"));
					break;
				case EMPTY_COLOR_CODE:
					res.setStatus(resourse.getString(FAILED));
					resDetails.setCode(resourse.getString(COLORCODEEMPTY));
					resDetails.setDescription(resourse.getString(COLORCODEEMPTYINFO));
					break;
				case INVALID_COLOR_CODE:
					resDetails.setCode(resourse.getString(COLORCODEEMPTY));
					resDetails.setDescription(resourse.getString(COLORCODEEMPTYINFO));
					break;
				case "INVALID_QR_TYPE":
					res.setStatus(resourse.getString(FAILED));
					StringBuilder sb = new StringBuilder();
					for (QRType s : QRType.values()) {
						sb.append(s + " ,");
					}
					resDetails.setCode(resourse.getString("qr.dynamic.dynamicQR.invalidQRType.code"));
					resDetails.setDescription(resourse.getString("qr.dynamic.dynamicQR.invalidQRType.info") + " " + sb.toString());
					break;
				default:
					logger.error("DynamicQRServiceImpl | addDynamicQRCodes | failed to generate dynamic QR Code  " + request.getAppCode() + " | requestId " + request.getRequestId() + " | Exception occure " + e.getMessage(), e);
				}
				res.setMessage(resDetails);
				return res;
			}
		}
	}

	public String getDynamicQRId(long idVal) {
		return "DQR" + String.format("%06d", idVal);	
	}
	
	private void checkQRCodeSize(String size) {
		try {
			Integer.parseInt(size);
		} catch (Exception e) {
			throw new ParseException(INVALIDQR);
		}
	}
	private void checkQRColor(String color) {
		try {
			if (color.contains("#"))
				Integer.parseInt(color.substring(1, color.length() - 1), 16);
		} catch (NumberFormatException e) {
			throw new NumberFormatException(INVALID_COLOR_CODE);
		}
	}
	private void checkExpiration(String seconds) {
		try {
			Integer.parseInt(seconds);
		} catch (Exception e) {
			logger.info("DynamicQRServiceImpl | addDynamicQRCodes |  requestId " + seconds + " | Error Message INVALID_EXPIRY_TIME");
			throw new NumberFormatException("INVALID_EXPIRY_TIME");
		}
	}

	@Override
	public DynamicQRResponse updateExistingDynamicQRCode(UpdateDynamicQRCodeRequest request) {
		logger.info("DynamicQRServiceImpl | updateExistingDynamicQRCode | request QR Code " + request.getQrCode() + " | QR Content |" + request.getQrContent());
		DynamicQRResponse res = new DynamicQRResponse();
		// check qrcode availabilty , if available need to get information
		Map<String, String> message = new HashMap<>();
		Map<String, String> data = new HashMap<>();
		DynamicQRCode dynamicQR = dynamicQRCodeRepository.getDynamicQrCode(request.getQrCode());
		QRApplication qrApp = applicationRepository.checkAppCode(dynamicQR.getAppCode());
		if (dynamicQR != null) {
			// qr code is available to check validity get previous requested date time with
			try {
				if (request.getRequestTime() == null) {
					throw new NullPointerException(REQUEST_TIME_EMPTY);
				} else if (request.getQrContent() == null || request.getQrContent().length() == 0) {
					throw new NullPointerException(EMPTY_QR_CONTENT);
				} else if (request.getColor() == null || request.getColor().length() == 0) {
					throw new NullPointerException(EMPTY_COLOR_CODE);
				}
				if(request.getQrSize()==null || request.getQrSize().isEmpty()) {
					request.setQrSize(250+"");
				}
				else {
				checkQRCodeSize(request.getQrSize());
				}
				if(request.getColor()==null || request.getColor().isEmpty()) {
					request.setColor("#000000");
				}
				else {
				checkQRColor(request.getColor());
				}
				long expiryTime = dynamicQR.getExpiryTIme().getTime();
				long timeDifference = expiryTime - (new Date(System.currentTimeMillis()).getTime());
				if (timeDifference < 0) {
					dynamicQR.setTxStatus(QRStatus.EXPIRED.ordinal() + "");
					dynamicQRCodeRepository.save(dynamicQR);
					res.setStatus(resourse.getString(FAILED));
					message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.expiration.code"));
					message.put(DESCRIPTION, resourse.getString("qr.dynamic.dynamicQR.expiration.info"));
				} else {
					if (dynamicQR.getTxStatus().equalsIgnoreCase(QRStatus.PENDING.ordinal() + "")) {
						try {
							String qrid = request.getQrCode();
							String encodedString = "";
							if (request.isLogoRequired()) {
								encodedString = qrImageProcessor2.getBase64EncodedQRImage(qrid, qrApp.getLogo(), request.getColor(), Integer.parseInt(request.getQrSize()), Integer.parseInt(request.getQrSize()), true);
							} else
								encodedString = qrImageProcessor2.getBase64EncodedQRImage(qrid, null, request.getColor(), Integer.parseInt(request.getQrSize()), Integer.parseInt(request.getQrSize()), false);
							byte[] decodedImg = Base64.getDecoder().decode(encodedString);
							dynamicQR.setQrImage(decodedImg);
							dynamicQR.setQrContent(request.getQrContent());
							dynamicQR.setRequestTime(request.getRequestTime());
							dynamicQR.setExpiryTIme(new Date(System.currentTimeMillis() + (dynamicQR.getExpirationPeriodInSeconds()) * 1000));
							dynamicQRCodeRepository.save(dynamicQR);
							res.setData(data);
							res.setStatus(resourse.getString("success"));
							message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.updateSuccessCode.code"));
							message.put(DESCRIPTION, resourse.getString("qr.dynamic.dynamicQR.updateSuccessCode.info"));
							res.setMessage(message);
							data.put("QR_Code_ID", qrid);
							data.put("QR_Image_URL", resourse.getString("qr.dynamic.dynamicQR.imageURLPrefix") + dynamicQR.getId());
							Calendar cal = Calendar.getInstance();
							cal.setTime(request.getRequestTime());
							cal.add(Calendar.SECOND, dynamicQR.getExpirationPeriodInSeconds());
							res.setData(data);
							res.setExpiryTime(cal.getTime().toString());
							expireQRCode(qrid, ((dynamicQR.getExpirationPeriodInSeconds()) * 1000) + 1000);
							logger.info("DynamicQRServiceImpl | updateDynamicQRCodes | Response " + res.getExpiryTime() + " | " + res.getStatus());

							return res;
						} catch (Exception e) {
							res.setStatus(resourse.getString(FAILED));
							message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.updateFailed.code"));
							message.put(DESCRIPTION, resourse.getString("qr.dynamic.dynamicQR.updateFailed.info"));
							logger.error("DynamicQRServiceImpl | updateDynamicQRCodes | Exception Occured | " + e.getMessage(), e);
						}
					} else {
						message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.updateFailed.code"));
						message.put(DESCRIPTION, resourse.getString("qr.dynamic.dynamicQR.updateFailed.info") + " STATUS " + dynamicQR.getTxStatus());
						logger.info("DynamicQRServiceImpl | updateDynamicQRCodes | Updation Failed " + request.getQrCode());
					}

				}

			} catch (Exception e) {
				switch (e.getMessage()) {
				case EMPTY_QR_CONTENT:
					res.setStatus(resourse.getString(FAILED));
					message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.emptyQRContent.code"));
					message.put(DESCRIPTION, resourse.getString("qr.dynamic.dynamicQR.emptyQRContent.info"));
					logger.info("DynamicQRServiceImpl | updateDynamicQRCodes | Error Message  EMPTY_QR_CONTENT " + request.getQrCode());
					break;
				case REQUEST_TIME_EMPTY:
					res.setStatus(resourse.getString(FAILED));
					message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.invalidQRContent.code"));
					message.put(DESCRIPTION, resourse.getString("qr.dynamic.dynamicQR.invalidQRContent.info"));
					logger.info("DynamicQRServiceImpl | updateDynamicQRCodes | Error Message  REQUEST_TIME_EMPTY " + request.getQrCode());
					break;
				case INVALIDQR:
					res.setStatus(resourse.getString(FAILED));
					message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.invalidQR.code"));
					message.put(DESCRIPTION, resourse.getString("qr.dynamic.dynamicQR.invalidQR.info"));
					logger.info("DynamicQRServiceImpl | updateDynamicQRCodes | Error Message  INVALID_QR " + request.getQrCode());
					break;
				case EMPTY_COLOR_CODE:
					res.setStatus(resourse.getString(FAILED));
					message.put(CODE, resourse.getString(COLORCODEEMPTY));
					message.put(DESCRIPTION, resourse.getString(COLORCODEEMPTYINFO));
					logger.info("DynamicQRServiceImpl | updateDynamicQRCodes | Error Message  EMPTY_COLOR_CODE " + request.getQrCode());
					break;
				case INVALID_COLOR_CODE:
					res.setStatus(resourse.getString(FAILED));
					message.put(CODE, resourse.getString(COLORCODEEMPTY));
					message.put(DESCRIPTION, resourse.getString(COLORCODEEMPTYINFO));
					logger.info("DynamicQRServiceImpl | updateDynamicQRCodes | Error Message  INVALID_COLOR_CODE " + request.getQrCode());
					break;
				default:
					res.setStatus(resourse.getString(FAILED));
					logger.error("DynamicQRServiceImpl | updateDynamicQRCodes | Exception Occured  QR Code |" + request.getQrCode() + " |" + e.getMessage(), e);
				}
			}

		} else {
			logger.info("DynamicQRServiceImpl | updateDynamicQRCodes | dynamic qr code is not available | qr code " + request.getQrCode());
			message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.dynamicQRNotAvailable.code"));
			message.put(DESCRIPTION, resourse.getString("qr.dynamic.dynamicQR.dynamicQRNotAvailable.info"));
			res.setStatus(resourse.getString(FAILED));
		}
		res.setMessage(message);
		res.setData(data);
		return res;
	}

	/**
	 * This method is used to validate dynamic QR Code
	 */
	@Override
	public ValidateDynamicQRResponse validateDynamicQRCode(ValidateDynamicQRCodeRequest request) {
		ValidateDynamicQRResponse res = new ValidateDynamicQRResponse();
		Map<String, String> message = new HashMap<>();
		Map<String, String> data = new HashMap<>();
		logger.info("DynamicQRServiceImpl | validateDynamicQRCode | reqeust app Code " + request.getAppCode() + " | QR Code |" + request.getDynamiQRCode());
		try {
			if ((request.getAppCode() == null || request.getAppCode().isEmpty()) && (request.getDynamiQRCode() == null || request.getDynamiQRCode().isEmpty())) {
				throw new NullPointerException(EMPTY_APP_CODE_AND_QR_CODE);
			} else if (request.getAppCode() == null || request.getAppCode().isEmpty()) {
				throw new NullPointerException(EMPTY_APP_CODE);
			} else if (request.getDynamiQRCode() == null || request.getDynamiQRCode().isEmpty()) {
				throw new NullPointerException(EMPTY_QR_CODE);
			}
			// 1. Existance checking with app code+QR Code
			DynamicQRCode dynamicQR = dynamicQRCodeRepository.checkAvailability(request.getDynamiQRCode(), request.getAppCode());
			if (dynamicQR == null) {
				logger.info("DynamicQRServiceImpl | validateDynamicQRCode | reqeust app Code " + request.getAppCode() + " | QR Code |" + request.getDynamiQRCode() + " | Dynamic QR Code is not available ");
				res.setStatus(resourse.getString(FAILED));
				message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.validationFailed.code"));
				message.put(DESCRIPTION, resourse.getString("qr.dynamic.dynamicQR.validationFailed.info"));
				res.setMessage(message);
				return res;
			} else {
				// QR Code and App Code is available
				// Check expiration
				long dif =0L;
				if(dynamicQR.getExpiryTIme()!=null)
				 dif= dynamicQR.getExpiryTIme().getTime() - (new Date(System.currentTimeMillis()).getTime());
				if (dif < 0) {
					throw new InvalidArgumentException(QR_EXPIRED);
				} else {
					if (dynamicQR.getTxStatus().equals(QRStatus.PENDING.ordinal() + "")) {
						data.put("QR_Content", dynamicQR.getQrContent());
						res.setStatus(resourse.getString("success"));
						res.setQrContent(dynamicQR.getQrContent());
						message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.verified.code"));
						message.put(DESCRIPTION, "verified");
						res.setMessage(message);
						logger.info("DynamicQRServiceImpl | validateDynamicQRCode | reqeust app Code " + request.getAppCode() + " | QR Code |" + request.getDynamiQRCode() + " | QR Validation Success | Status " + dynamicQR.getTxStatus());
						return res;
					} else {
						res.setStatus(resourse.getString(FAILED));
						message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.validationFailed.code"));
						message.put(DESCRIPTION, resourse.getString("qr.dynamic.dynamicQR.verifcationFailed.InvalidStatus.info"));
						logger.info("DynamicQRServiceImpl | validateDynamicQRCode | reqeust app Code " + request.getAppCode() + " | QR Code |" + request.getDynamiQRCode() + " | QR Validation Fail | Status " + dynamicQR.getTxStatus());
						res.setMessage(message);
						return res;
					}
				}
			}
		} catch (Exception e) {

			switch (e.getMessage()) {
			case QR_EXPIRED:
				message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.expiration.code"));
				message.put(DESCRIPTION, resourse.getString("qr.dynamic.dynamicQR.expiration.info"));
				res.setStatus(resourse.getString(FAILED));
				logger.info("DynamicQRServiceImpl | validateDynamicQRCode | Error Message " + e.getMessage() + " | appCode " + request.getAppCode() + " | qrCode " + request.getDynamiQRCode());
				break;

			case EMPTY_QR_CODE:
				message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.emptyQRCode.code"));
				message.put(DESCRIPTION, resourse.getString("qr.dynamic.dynamicQR.emptyQRCode.info"));
				res.setStatus(resourse.getString(FAILED));
				logger.info("DynamicQRServiceImpl | validateDynamicQRCode | Error Message " + e.getMessage() + " | appCode " + request.getAppCode() + " | qrCode " + request.getDynamiQRCode());
				break;

			case EMPTY_APP_CODE:
				message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.emptyAppCode.code"));
				message.put(DESCRIPTION, resourse.getString("qr.dynamic.dynamicQR.emptyAppCode.info"));
				res.setStatus(resourse.getString(FAILED));
				logger.info("DynamicQRServiceImpl | validateDynamicQRCode | Error Message " + e.getMessage() + " | appCode " + request.getAppCode() + " | qrCode " + request.getDynamiQRCode());
				break;
			case EMPTY_APP_CODE_AND_QR_CODE:
				message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.emptyQRAndAPPCode.code"));
				message.put(DESCRIPTION, resourse.getString("qr.dynamic.dynamicQR.emptyQRAndAPPCode.info"));
				res.setStatus(resourse.getString(FAILED));
				logger.info("DynamicQRServiceImpl | validateDynamicQRCode | Error Message " + e.getMessage() + " | appCode " + request.getAppCode() + " | qrCode " + request.getDynamiQRCode());
				break;
			default :
				message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.validationFailed.code"));
				message.put(DESCRIPTION, resourse.getString("qr.dynamic.dynamicQR.verifcationFailed.InvalidStatus.info"));
				res.setStatus(resourse.getString(FAILED));
				logger.info("DynamicQRServiceImpl | validateDynamicQRCode | reqeust app Code " + request.getAppCode() + " | QR Code |" + request.getDynamiQRCode() + " | QR Validation Fail | Status ");
				res.setMessage(message);
				return res;
			}
		}
		res.setMessage(message);
		return res;
	}

	@Override
	public AcknowledgeDynamicQRCodeResponse acknowledegeDynamicQRCode(AcknowledgeDynamicQRCodeRequest request) {
		AcknowledgeDynamicQRCodeResponse res = new AcknowledgeDynamicQRCodeResponse();
		Map<String, String> message = new HashMap<>();
		logger.info("DynamicQRServiceImpl | acknowledegeDynamicQRCode | reqeust app Code " + request.getAppCode() + " | QR Code |" + request.getDynamiQRCode());
		try {
			if ((request.getAppCode() == null || request.getAppCode().isEmpty()) && (request.getDynamiQRCode() == null || request.getDynamiQRCode().isEmpty())) {
				throw new NullPointerException(EMPTY_APP_CODE_AND_QR_CODE);
			} else if (request.getDynamiQRCode() == null || request.getDynamiQRCode().isEmpty()) {
				throw new NullPointerException(EMPTY_QR_CODE);
			} else if (request.getAppCode() == null || request.getAppCode().isEmpty()) {
				throw new NullPointerException(EMPTY_APP_CODE);
			}
			boolean check = false;
			for (QRStatus s : QRStatus.values()) {
				if (request.getStatus().equalsIgnoreCase(s.name())) {
					check = true;
					break;
				}
			}
			if (!check) {
				throw new NullPointerException("INVALID_STATUS");
			}
			// 1. Existance checking with app code+QR Code
			DynamicQRCode dynamicQR = dynamicQRCodeRepository.checkAvailability(request.getDynamiQRCode(), request.getAppCode());
			if (dynamicQR == null) {
				res.setStatus(resourse.getString(FAILED));
				message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.acknowledgeFailed.code"));
				message.put(DESCRIPTION, resourse.getString("qr.dynamic.dynamicQR.acknowledgeFailed.info"));
				res.setMessage(message);
				logger.info("DynamicQRServiceImpl | acknowledegeDynamicQRCode | dynamicQR is not available for " + request.getDynamiQRCode() + " | " + request.getAppCode());
				return res;
			} else {
				// QR Code and App Code is available
				// Check expiration
				long dif = dynamicQR.getExpiryTIme().getTime() - (new Date(System.currentTimeMillis()).getTime());
				if (dif < 0) {
					throw new InvalidArgumentException(QR_EXPIRED);
				} else {
					if (!(request.getStatus().equalsIgnoreCase(QRStatus.DISCARD + "")) && dynamicQR.getTxStatus().equals(QRStatus.PENDING.ordinal() + "")) {
						res.setStatus(resourse.getString("success"));
						message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.acknowledge.code"));
						message.put(DESCRIPTION, resourse.getString("qr.dynamic.dynamicQR.acknowledge.info"));
						res.setMessage(message);
						dynamicQR.setTxStatus(QRStatus.CONSUMED.ordinal() + "");
						// update status as consumed
						dynamicQRCodeRepository.save(dynamicQR);
						return res;
					} else {

						if (dynamicQR.getTxStatus().equalsIgnoreCase(QRStatus.CONSUMED.ordinal() + "")) {
							throw new InvalidArgumentException("ALREADY_CONSUMED");
						} else if (dynamicQR.getTxStatus().equals(QRStatus.EXPIRED.ordinal())) {
							throw new InvalidArgumentException(QR_EXPIRED);
						} else if (dynamicQR.getTxStatus().equalsIgnoreCase(QRStatus.DISCARD.ordinal() + "")) {
							throw new InvalidArgumentException("ALREADY_DISCARD");
						} else if (request.getStatus().equalsIgnoreCase(QRStatus.DISCARD + "")) {
							// success message discard
							dynamicQR.setTxStatus(QRStatus.DISCARD.ordinal() + "");
							dynamicQRCodeRepository.save(dynamicQR);
							res.setStatus(resourse.getString("success"));
							message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.discardSuccess.code"));
							message.put(DESCRIPTION, resourse.getString("qr.dynamic.dynamicQR.discardSuccess.info"));
							res.setMessage(message);
							return res;
						}

					}
				}
			}
		} catch (Exception e) {
			switch (e.getMessage()) {
			case QR_EXPIRED:
				message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.expiration.code"));
				message.put(DESCRIPTION, resourse.getString("qr.dynamic.dynamicQR.expiration.info"));
				res.setStatus(resourse.getString(FAILED));
				res.setMessage(message);
				logger.info("DynamicQRServiceImpl | acknowledegeDynamicQRCode | Error Occured " + e.getMessage() + " | qrCode " + request.getDynamiQRCode(), e);
				break;
			case "INVALID_STATUS":
				StringBuilder bf = new StringBuilder();
				for (QRStatus s : QRStatus.values()) {
					bf.append(s.name()+" ");
				}
				message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.invalidStatus.code"));
				message.put("DESCRIPTION", "Valid Status are " +bf.toString());
				res.setStatus(resourse.getString(FAILED));
				res.setMessage(message);
				logger.info("DynamicQRServiceImpl | acknowledegeDynamicQRCode | Error Occured " + e.getMessage() + " | qrCode " + request.getDynamiQRCode(), e);
				break;
			case EMPTY_QR_CODE:
				message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.emptyQRCode.code"));
				message.put(DESCRIPTION, resourse.getString("qr.dynamic.dynamicQR.emptyQRCode.info"));
				res.setMessage(message);
				res.setStatus(resourse.getString(FAILED));
				logger.info("DynamicQRServiceImpl | acknowledegeDynamicQRCode | Error Occured " + e.getMessage() + " | qrCode " + request.getDynamiQRCode(), e);
				break;
			case EMPTY_APP_CODE:
				message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.emptyAppCode.code"));
				message.put(DESCRIPTION, resourse.getString("qr.dynamic.dynamicQR.emptyAppCode.info"));
				res.setStatus(resourse.getString(FAILED));
				res.setMessage(message);
				logger.info("DynamicQRServiceImpl | acknowledegeDynamicQRCode | Error Occured " + e.getMessage() + " | qrCode " + request.getDynamiQRCode(), e);
				break;
			case EMPTY_APP_CODE_AND_QR_CODE:
				message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.emptyQRAndAPPCode.code"));
				message.put(DESCRIPTION, resourse.getString("qr.dynamic.dynamicQR.emptyQRAndAPPCode.info"));
				res.setStatus(resourse.getString(FAILED));
				res.setMessage(message);
				logger.info("DynamicQRServiceImpl | acknowledegeDynamicQRCode | Error Occured " + e.getMessage() + " | qrCode " + request.getDynamiQRCode(), e);
				break;
			case "ALREADY_CONSUMED":
				message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.alreadyConsumed.code"));
				message.put(DESCRIPTION, resourse.getString("qr.dynamic.dynamicQR.alreadyConsumed.info"));
				res.setStatus(resourse.getString(FAILED));
				res.setMessage(message);
				break;
			case "ALREADY_DISCARD":
				message.put(CODE, resourse.getString("qr.dynamic.dynamicQR.alreadyDiscard.code"));
				message.put(DESCRIPTION, resourse.getString("qr.dynamic.dynamicQR.alreadyDiscard.info"));
				res.setStatus(resourse.getString(FAILED));
				res.setMessage(message);
				logger.info("DynamicQRServiceImpl | acknowledegeDynamicQRCode | Error Occured " + e.getMessage() + " | qrCode " + request.getDynamiQRCode(), e);
				break;
			default:
				res.setStatus(resourse.getString(FAILED));
				logger.error("DynamicQRServiceImpl | acknowledegeDynamicQRCode qrCode " + request.getDynamiQRCode() + " | " + request.getAppCode() + "| Exception Occured " + e.getMessage(), e);
			}
		}
		return res;
	}
	private void expireQRCode(String dynamicQRId, long delay) {
		ExpireQRTask expireQRTask = new ExpireQRTask(dynamicQRId);
		new Timer().schedule(expireQRTask, delay);
	}
	private class ExpireQRTask extends TimerTask {
		private String dynamicQRId;
		public ExpireQRTask(String dynamicQRId) {
			super();
			this.dynamicQRId = dynamicQRId;
		}
		@Override
		public void run() {
			DynamicQRCode dynamicQR = dynamicQRCodeRepository.getDynamicQrCode(dynamicQRId);
			long expiryTime = dynamicQR.getExpiryTIme().getTime();
			long timeDifference = expiryTime - (new Date(System.currentTimeMillis()).getTime());
			if (timeDifference < 0) {
				dynamicQR.setTxStatus(QRStatus.EXPIRED.ordinal() + "");
				dynamicQRCodeRepository.save(dynamicQR);
			}

		}

	}

}
