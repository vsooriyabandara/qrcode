package lk.dialog.ist.dcash.qrcode.controller.request;

public class ApplicationRequest {

	private String contactNo;
	private String email;
	private String appName;
	private String description;
	private String notifyURL;
	private String logo;
	private String addedBy;
	private String cpName;

	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getNotifyURL() {
		return notifyURL;
	}
	public void setNotifyURL(String notifyURL) {
		this.notifyURL = notifyURL;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getAddedBy() {
		return addedBy;
	}
	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}
	
	public String getCpName() {
		return cpName;
	}
	public void setCpName(String cpName) {
		this.cpName = cpName;
	}
	@Override
	public String toString() {
		return "ApplicationRequest [contactNo=" + contactNo + ", email="
				+ email + ", appName=" + appName + ", description="
				+ description + ", notifyURL=" + notifyURL + ", logo=" + logo
				+ ", addedBy=" + addedBy + ", cpName=" + cpName + "]";
	}
	
}
