package lk.dialog.ist.dcash.qrcode.db.modal;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

import lk.dialog.ist.dcash.qrcode.controller.request.Merchant;

@Entity
@Audited
public class QRMerchant {

	@Id
	// Fro mySql @GeneratedValue(strategy = GenerationType.AUTO, generator = "merchant_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "merchant_seq")
	@SequenceGenerator(name = "merchant_seq", sequenceName = "merchant_seq")
	private long id;
	
	@Column
	private String merchantId;
	
	@Column
	@NotNull
	@Size(min = 4,max=200)
	private String name;
	
	@Column
	@Size(min = 4,max=200)
	private String regName;
	
	@Column
	private int type;
	
	@Column
	@NotNull
	@Size(min=4,max = 150)
	private String address;
	
	@Column
	@NotNull
	@Size(min=8,max = 12)
	private String contactNumber;
	
	@Column
	@NotNull
	@Pattern(regexp = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}")
	@Size(max = 40)
	private String email;
	
	@Size(max = 40)
	private String alias;
	
	@Column
	@Digits(integer = 10, fraction = 10)
	private BigDecimal longitude;
	
	@Column
	@Digits(integer = 10, fraction = 10)
	private BigDecimal latitude;
	
	@Column
	private String keyVal;
	
	@Lob
	private byte[] logo;
	
	@Column
	private String status;
	
	//---------New columns---------------
	@Column 
	@Size(max = 150)
	private String location;
	
	@Column
	@Size(max = 12)
	private String nicNo;
	
	@Column
	private String brcNo;
	
	@Column
	@Size(max = 150)
	private String contactPerson;
	
	@Column
	@Size(max = 1000)
	private String other;
	
	//-------New Cloumns----------------
	
	@Column
	private String addedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date addedDate;
	
	@Column
	private String modifiedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedDate;
	
	@Column
	private String modifiedReason;
	
	public QRMerchant() {

	}
	public QRMerchant(Merchant merchant) {
		name = merchant.getName().trim();
		regName = merchant.getRegName();
		address = merchant.getAddress();
		contactNumber = merchant.getContactNumber();
		email = merchant.getEmail();
		alias = merchant.getAlias().trim();
		location = merchant.getLocation();
		brcNo = merchant.getBrc();
		nicNo =  merchant.getNic();
		contactPerson = merchant.getCpName();
		other = merchant.getOther();
		keyVal = merchant.getKey();
		status = merchant.getStatus();
		addedBy = merchant.getAddedBy();
		modifiedBy = merchant.getModifiedBy();
		modifiedDate = merchant.getModifiedDate();
		modifiedReason = merchant.getModifiedReason();
		
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRegName() {
		return regName;
	}
	public void setRegName(String regName) {
		this.regName = regName;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public BigDecimal getLongitude() {
		return longitude;
	}
	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}
	public BigDecimal getLatitude() {
		return latitude;
	}
	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}
	public String getKey() {
		return keyVal;
	}
	public void setKey(String keyVal) {
		this.keyVal = keyVal;
	}
	public byte[] getLogo() {
		return logo;
	}
	public void setLogo(byte[] logo) {
		this.logo = logo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAddedBy() {
		return addedBy;
	}
	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}
	public Date getAddedDate() {
		return addedDate;
	}
	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getModifiedReason() {
		return modifiedReason;
	}
	public void setModifiedReason(String modifiedReason) {
		this.modifiedReason = modifiedReason;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getNicNo() {
		return nicNo;
	}
	public void setNicNo(String nicNo) {
		this.nicNo = nicNo;
	}
	public String getBrcNo() {
		return brcNo;
	}
	public void setBrcNo(String brcNo) {
		this.brcNo = brcNo;
	}
	public String getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	public String getOther() {
		return other;
	}
	public void setOther(String other) {
		this.other = other;
	}

	

}
