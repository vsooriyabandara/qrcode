package lk.dialog.ist.dcash.qrcode.controller.response;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import lk.dialog.ist.dcash.qrcode.db.modal.QRApplication;
import lk.dialog.ist.dcash.qrcode.db.modal.QRCode;
import lk.dialog.ist.dcash.qrcode.db.modal.QRMerchant;

public class QRCodeResponse {
	long ID;
	String qrAlias;
	String Location;
	String createdDate;
	String merchantId;
	String qrCodeId;
	int status;
	BigDecimal  latitude;
	BigDecimal longitude;
	String counter;
	String qrId;
	String qrContent;
	String merchantAlias;
	String merchantAddress;
	String taggedAppName;
	String merchantName;
	String qrCategory;
	String storeName;
	String storeAddress;
	String storeType;
	String eZCashAlias;
	String natureOfBusiness;
	
	public QRCodeResponse() {
	}

	public QRCodeResponse(String qrAlias, String location,
			String createdDate, int status) {
		this.qrAlias = qrAlias;
		Location = location;
		this.createdDate = createdDate;
		this.status = status;
	}
	
	public QRCodeResponse(QRCode qr) {
		this.ID = qr.getId();
		this.qrAlias = qr.getAlias();
		Location = qr.getLocaton();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		this.createdDate = dateFormat.format(qr.getAddedDate());
		this.status = qr.getStatus();
		this.merchantId = qr.getMerchantId();
		this.qrCodeId = qr.getQrId();
		this.latitude = qr.getLatitude();
		this.longitude = qr.getLongitude();
		this.counter = qr.getCounter();
		this.qrId = qr.getQrId();
		this.qrContent = qr.getQrContent();
		this.qrCategory = qr.getQrCategory();
		this.storeAddress = qr.getStoreAddress();
		this.storeName = qr.getStoreName();
		this.storeType = qr.getMerchantIndustry();
		this.eZCashAlias = qr.geteZCashAlias();
		
	}
	
	public QRCodeResponse(QRCode qr,QRMerchant mer) {
		this.ID = qr.getId();
		this.qrAlias = qr.getAlias();
		Location = qr.getLocaton();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		this.createdDate = dateFormat.format(qr.getAddedDate());
		this.status = qr.getStatus();
		this.merchantId = qr.getMerchantId();
		this.qrCodeId = qr.getQrId();
		this.latitude = qr.getLatitude();
		this.longitude = qr.getLongitude();
		this.counter = qr.getCounter();
		this.qrId = qr.getQrId();
		this.qrContent = qr.getQrContent();
		this.merchantAddress =mer.getAddress();
		this.merchantAlias = mer.getAlias();
		this.storeAddress = qr.getStoreAddress();
		this.storeName = qr.getStoreName();
		this.storeType = qr.getMerchantIndustry();
		this.eZCashAlias = qr.geteZCashAlias();
		
	}
	
	public QRCodeResponse(QRCode qr,QRMerchant mer,String app) {
		this.qrAlias = qr.getAlias();
		this.Location = qr.getLocaton();
		this.qrCodeId = qr.getQrId();
		this.merchantName = mer.getName();
		this.taggedAppName = app;
	}

	public String getQrAlias() {
		return qrAlias;
	}

	public void setQrAlias(String qrAlias) {
		this.qrAlias = qrAlias;
	}

	public String getLocation() {
		return Location;
	}

	public void setLocation(String location) {
		Location = location;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getQrCodeId() {
		return qrCodeId;
	}

	public void setQrCodeId(String qrCodeId) {
		this.qrCodeId = qrCodeId;
	}

	public long getID() {
		return ID;
	}

	public void setID(long iD) {
		ID = iD;
	}

	public String getQrCategory() {
		return qrCategory;
	}

	public void setQrCategory(String qrCategory) {
		this.qrCategory = qrCategory;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getStoreAddress() {
		return storeAddress;
	}

	public void setStoreAddress(String storeAddress) {
		this.storeAddress = storeAddress;
	}

	public String getStoreType() {
		return storeType;
	}

	public void setStoreType(String storeType) {
		this.storeType = storeType;
	}

	public String geteZCashAlias() {
		return eZCashAlias;
	}

	public void seteZCashAlias(String eZCashAlias) {
		this.eZCashAlias = eZCashAlias;
	}

	public String getNatureOfBusiness() {
		return natureOfBusiness;
	}

	public void setNatureOfBusiness(String natureOfBusiness) {
		this.natureOfBusiness = natureOfBusiness;
	}
	
	
	
	
	
	
}
