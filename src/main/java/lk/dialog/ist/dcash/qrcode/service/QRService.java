package lk.dialog.ist.dcash.qrcode.service;

import lk.dialog.ist.dcash.qrcode.controller.request.BulkQrRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.DisableQrRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.GetQRRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.QRStatusRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.QrDetails;
import lk.dialog.ist.dcash.qrcode.controller.request.QueryQRCodesRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.Request;
import lk.dialog.ist.dcash.qrcode.controller.request.TagQRRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.TagQRtoApp;
import lk.dialog.ist.dcash.qrcode.controller.request.ValidateQRRequest;
import lk.dialog.ist.dcash.qrcode.controller.response.QueryQRCodesResponse;
import lk.dialog.ist.dcash.qrcode.controller.response.Response;
import lk.dialog.ist.dcash.qrcode.controller.response.Response1;
import lk.dialog.ist.dcash.qrcode.controller.response.ResponseObject;
import lk.dialog.ist.dcash.qrcode.controller.response.TagQrResponse;

public interface QRService {

	public ResponseObject addStaticQR(Request request,int size);
	
	public ResponseObject getQRCodes(GetQRRequest request);
	
	public ResponseObject getQRContent(ValidateQRRequest request);
	
	public ResponseObject tagQRToApp(TagQRtoApp request);
	
	public ResponseObject disableQR(DisableQrRequest request);

	public ResponseObject createBulkStaticQR(BulkQrRequest request);

	public ResponseObject updateQRDetails(Request merchantReq);

	public ResponseObject getQRCodesByDate(QrDetails request);

	public byte[] generateExcel(String mID, String from, String to);

	public ResponseObject getQrCodesTagDetails(QrDetails request);

	public ResponseObject getBulkQRDetails(QrDetails request);

	public byte[] generateBulkQRExcelFile(String from, String to);

	public QueryQRCodesResponse getQueryQRCodes(QueryQRCodesRequest req);

	public TagQrResponse doTagQR(TagQRRequest req);

	byte[] generateQrPDF(byte[] qrArray, String mName, String qrID,
			String location);
	
	public ResponseObject updateQRStatus(QRStatusRequest qrRequest);

}
