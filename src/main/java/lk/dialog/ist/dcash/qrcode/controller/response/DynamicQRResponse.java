package lk.dialog.ist.dcash.qrcode.controller.response;


import java.util.Map;
/**
 * Dyanic QR Response
 * @author Veranga Sooriyabandara
 * @since 14 -11-2017
 * @version 1.0
 *
 */
public class DynamicQRResponse {

	private String status;
	private Map<String, String> message; 
	private Map<String, ?> data;
	private String expiryTime;
	 
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Map<String, String> getMessage() {
		return message;
	}
	public void setMessage(Map<String, String> message) {
		this.message = message;
	}
	public Map<String, ?> getData() {
		return data;
	}
	public void setData(Map<String, ?> data) {
		this.data = data;
	}
	public String getExpiryTime() {
		return expiryTime;
	}
	public void setExpiryTime(String expiryTime) {
		this.expiryTime = expiryTime;
	}
	
	
	 
	 
	
	
	
}
