package lk.dialog.ist.dcash.qrcode.util;

public class InvalidArgumentException extends Exception {
	
	private static final long serialVersionUID = 4733155728364537565L;

	public InvalidArgumentException(String s) {
		super(s);
	}

}
