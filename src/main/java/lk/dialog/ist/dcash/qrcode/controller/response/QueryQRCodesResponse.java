package lk.dialog.ist.dcash.qrcode.controller.response;

public class QueryQRCodesResponse {
	private int count;
    private String codes;
    
    
    
	public QueryQRCodesResponse() {
		super();
	}

	public QueryQRCodesResponse(int count, String codes) {
		this.count = count;
		this.codes = codes;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getCodes() {
		return codes;
	}

	public void setCodes(String codes) {
		this.codes = codes;
	}
	
	
}
