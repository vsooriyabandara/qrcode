package lk.dialog.ist.dcash.qrcode.controller.request;
/**
 * 
 * @author Veranga Sooriyabandara
 * @since  20-11-2017
 * @version 1.0
 * jira id :  QR-73
 *
 */
public class AcknowledgeDynamicQRCodeRequest {
	
	private String appCode;
	private String dynamiQRCode;
	private String status;
	public String getAppCode() {
		return appCode;
	}
	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}
	public String getDynamiQRCode() {
		return dynamiQRCode;
	}
	public void setDynamiQRCode(String dynamiQRCode) {
		this.dynamiQRCode = dynamiQRCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	

}
