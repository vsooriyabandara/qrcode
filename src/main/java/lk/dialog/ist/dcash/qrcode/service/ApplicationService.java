package lk.dialog.ist.dcash.qrcode.service;

import lk.dialog.ist.dcash.qrcode.controller.request.ApplicationRequest;
import lk.dialog.ist.dcash.qrcode.controller.response.Response;
import lk.dialog.ist.dcash.qrcode.controller.response.Response1;
import lk.dialog.ist.dcash.qrcode.controller.response.ResponseObject;

public interface ApplicationService {

	public ResponseObject registerApp(ApplicationRequest appRequest);

	public ResponseObject getApplications();
}
