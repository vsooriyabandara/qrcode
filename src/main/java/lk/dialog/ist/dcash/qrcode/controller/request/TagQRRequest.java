package lk.dialog.ist.dcash.qrcode.controller.request;

import java.util.ArrayList;

public class TagQRRequest {
	ArrayList<TagQRRequestDetails> codes;
	
	public TagQRRequest() {
		super();
	}

	public TagQRRequest(ArrayList<TagQRRequestDetails> codes) {
		super();
		this.codes = codes;
	}

	public ArrayList<TagQRRequestDetails> getCodes() {
		return codes;
	}

	public void setCodes(ArrayList<TagQRRequestDetails> codes) {
		this.codes = codes;
	}

	@Override
	public String toString() {
		return "TagQRRequest [codes=" + codes + "]";
	}
	
}
