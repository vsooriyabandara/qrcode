package lk.dialog.ist.dcash.qrcode.controller.response;

import java.util.List;

import lk.dialog.ist.dcash.qrcode.db.modal.QRCode;

public class QRResponse {
	
	String success;
	String description;
	List<QRCode> qrcode;
	
	public List<QRCode> getQrcode() {
		return qrcode;
	}
	public void setQrcode(List<QRCode> qrcode) {
		this.qrcode = qrcode;
	}
	public String getSuccess() {
		return success;
	}
	public void setSuccess(String success) {
		this.success = success;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
