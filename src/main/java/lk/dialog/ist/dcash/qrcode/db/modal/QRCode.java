package lk.dialog.ist.dcash.qrcode.db.modal;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

import lk.dialog.ist.dcash.qrcode.controller.request.Request;

import java.math.BigDecimal;
import java.util.Date;

@Entity
@Audited
public class QRCode {
	 //@Id
	 //My SQL @GeneratedValue(strategy = GenerationType.AUTO, generator = "qr_code_seq")
	
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "qr_code_seq")
    @SequenceGenerator(name = "qr_code_seq", sequenceName = "qr_code_seq")
    private long id;
    
    @Column
    private String qrId;
    
    @Column
    @NotNull
    private String merchantId;
    
    @Column
    private String appCode;
    
    @Column
    @NotNull
    @Size(max = 500)
    private String qrContent;
    
    @Lob
    private byte[] qrImage;
    
    @Column
    private int status;
    
    
  //---------New columns---------------
    @Column
    private String alias;
    
    @Column
    private String locaton;
    
    @Column
    @Size(max = 40)
    private String counter;
    
    @Column
	@Digits(integer = 10, fraction = 10)
	private BigDecimal longitude;
	
	@Column
	@Digits(integer = 10, fraction = 10)
	private BigDecimal latitude;
    
  //-------New Cloumns----------------  
    
	@Temporal(TemporalType.TIMESTAMP)
    private Date addedDate;
    
    @Column
    private String addedBy;
    
	@Temporal(TemporalType.TIMESTAMP)
    private Date modifiedDate;
    
    @Column
    private String modifiedBy;
    
    @Column
    @Size(max = 500)
	private String modifiedReason;
    
    
    @Column
    private String qrCategory; // new for AliPay
    
    @Column
    private String storeId; // new for AliPay  Identify a store under merchant
    
    @Column
    private String storeName; // new for AliPay
    
    @Column
    private String transCurrency; // new for AliPay
    
    @Column
    private String storeAddress; // new for AliPay
    
    @Column
    private String merchantIndustry; // new for AliPay
    
    @Column
    private String qr_code_url;
    
    @Column
    private String eZCashAlias; // new for Alipay
    
	public QRCode() {
		
	}

	public QRCode(Request request) {
		this.merchantId = request.getMerchantID();
		//this.qrId = request.getQrid();
		this.qrContent = request.getContent();
		this.addedDate = request.getAddedDate();
		this.addedBy = request.getAddedBy();
		this.modifiedDate = request.getModifiedDate();
		this.modifiedBy = request.getModifiedBy();
		this.alias = request.getQrAlias().toUpperCase().trim();
		this.locaton = request.getQrLocation();
		this.counter = request.getCounter();
		this.longitude = request.getQrLongitude();
		this.latitude = request.getQrLatitude();
		this.storeId = request.getStoreId();
		this.storeName = request.getStoreName();
		this.storeAddress = request.getStoreAddress();
		this.merchantIndustry = request.getMerchantID();
		this.qrCategory = request.getQrType();
		this.eZCashAlias = request.geteZCashAlias();
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getQrId() {
		return qrId;
	}

	public void setQrId(String qrId) {
		this.qrId = qrId;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getQrContent() {
		return qrContent;
	}

	public void setQrContent(String qrContent) {
		this.qrContent = qrContent;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int i) {
		this.status = i;
	}

	public Date getAddedDate() {
		return addedDate;
	}

	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}

	public String getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public byte[] getQrImage() {
		return qrImage;
	}

	public void setQrImage(byte[] qrImage) {
		this.qrImage = qrImage;
	}

	public String getAppCode() {
		return appCode;
	}

	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getLocaton() {
		return locaton;
	}

	public void setLocaton(String locaton) {
		this.locaton = locaton;
	}

	public String getCounter() {
		return counter;
	}

	public void setCounter(String counter) {
		this.counter = counter;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public String getModifiedReason() {
		return modifiedReason;
	}

	public void setModifiedReason(String modifiedReason) {
		this.modifiedReason = modifiedReason;
	}

	public String getQrCategory() {
		return qrCategory;
	}

	public void setQrCategory(String qrCategory) {
		this.qrCategory = qrCategory;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getTransCurrency() {
		return transCurrency;
	}

	public void setTransCurrency(String transCurrency) {
		this.transCurrency = transCurrency;
	}

	public String getStoreAddress() {
		return storeAddress;
	}

	public void setStoreAddress(String storeAddress) {
		this.storeAddress = storeAddress;
	}

	public String getMerchantIndustry() {
		return merchantIndustry;
	}

	public void setMerchantIndustry(String merchantIndustry) {
		this.merchantIndustry = merchantIndustry;
	}

	public String getQr_code_url() {
		return qr_code_url;
	}

	public void setQr_code_url(String qr_code_url) {
		this.qr_code_url = qr_code_url;
	}

	public String geteZCashAlias() {
		return eZCashAlias;
	}

	public void seteZCashAlias(String eZCashAlias) {
		this.eZCashAlias = eZCashAlias;
	}

	
	
	
	
	
}
