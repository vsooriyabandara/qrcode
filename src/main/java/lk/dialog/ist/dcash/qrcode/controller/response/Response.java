package lk.dialog.ist.dcash.qrcode.controller.response;

import java.util.Map;

public class Response {
	
    private String status;
    private Map<String, String> message; 
    private Map<String, ?> data;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Map<String, String> getMessage() {
		return message;
	}
	public void setMessage(Map<String, String> message) {
		this.message = message;
	}
	public Map<String, ?> getData() {
		return data;
	}
	public void setData(Map<String, ?> data) {
		this.data = data;
	}
    
	
    
}
