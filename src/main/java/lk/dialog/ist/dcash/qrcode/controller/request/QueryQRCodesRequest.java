package lk.dialog.ist.dcash.qrcode.controller.request;

public class QueryQRCodesRequest {
	long code_id; //id
	String qr_serial ; //qr_id
	String name; 
	String location;
	String counter; 
	String other;
	
	
	public QueryQRCodesRequest() {
		this.qr_serial="" ;
		this.name=""; 
		this.location="";
		this.counter=""; 
		this.other="";
	}
	public long getCode_id() {
		return code_id;
	}
	public void setCode_id(long code_id) {
		this.code_id = code_id;
	}
	public String getQr_serial() {
		return qr_serial;
	}
	public void setQr_serial(String qr_serial) {
		this.qr_serial = qr_serial;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getCounter() {
		return counter;
	}
	public void setCounter(String counter) {
		this.counter = counter;
	}
	public String getOther() {
		return other;
	}
	public void setOther(String other) {
		this.other = other;
	}
	@Override
	public String toString() {
		return "QueryQRCodesRequest [code_id=" + code_id + ", qr_serial="
				+ qr_serial + ", name=" + name + ", location=" + location
				+ ", counter=" + counter + ", other=" + other + "]";
	}
	
	
	
}
