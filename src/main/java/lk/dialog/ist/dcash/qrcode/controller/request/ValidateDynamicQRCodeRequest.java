package lk.dialog.ist.dcash.qrcode.controller.request;

import java.util.Date;
/**
 * 
 * @author Veranga Sooriyabandara
 * @since 2017-11-16
 * @version 1.0
 * @jira id QR-61
 *
 */
public class ValidateDynamicQRCodeRequest {

	private String appCode;
	private String dynamiQRCode;
	
	public String getAppCode() {
		return appCode;
	}
	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}
	public String getDynamiQRCode() {
		return dynamiQRCode;
	}
	public void setDynamiQRCode(String dynamiQRCode) {
		this.dynamiQRCode = dynamiQRCode;
	}
}
