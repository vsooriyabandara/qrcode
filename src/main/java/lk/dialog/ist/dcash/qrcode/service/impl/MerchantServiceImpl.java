package lk.dialog.ist.dcash.qrcode.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.swing.text.StyledEditorKit.ItalicAction;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;

import lk.dialog.ist.dcash.qrcode.controller.response.Response;
import lk.dialog.ist.dcash.qrcode.controller.response.Response1;
import lk.dialog.ist.dcash.qrcode.controller.response.ResponseData;
import lk.dialog.ist.dcash.qrcode.controller.response.ResponseDetails;
import lk.dialog.ist.dcash.qrcode.controller.response.ResponseObject;
import lk.dialog.ist.dcash.qrcode.db.modal.QRMerchant;
import lk.dialog.ist.dcash.qrcode.db.repository.MerchantRepository;
import lk.dialog.ist.dcash.qrcode.db.repository.QRCodeRepository;
import lk.dialog.ist.dcash.qrcode.service.MerchantService;
import lk.dialog.ist.dcash.qrcode.util.MerchantStatus;

@Service
public class MerchantServiceImpl implements MerchantService{

	@Autowired
    MerchantRepository merchantRepository;
	
	@Autowired
    QRCodeRepository qrRepository;
	
	protected static ResourceBundle resourse = ResourceBundle.getBundle("data");
	
	@PersistenceContext
	private EntityManager em;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	private static final String FAILED="failed";
	private static final String DESCRIPTION = "description";
	private static final String CODE="code";

	@Transactional
	@Override
	public ResponseObject addMerchant(QRMerchant merchantReq,String logo) {
		long startTime = System.currentTimeMillis();
		logger.info("MerchantServiceImpl STARTED addMerchant | " +merchantReq.toString());
		ResponseObject res = new ResponseObject();
		ResponseDetails resDetails = new ResponseDetails();
		String json = "";
		try {
			QRMerchant merchant = merchantRepository.checkName(merchantReq.getName().toUpperCase());
			if(null == merchant) {
				merchantReq.setStatus(String.valueOf(MerchantStatus.valueOf(merchantReq.getStatus()).ordinal()));
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	        	Date date = new Date();
	        	merchantReq.setAddedDate(dateFormat.parse(dateFormat.format(date)));
	        	merchantReq.setAlias(merchantReq.getAlias().toUpperCase());
				
				//Saving Image
	        	if(null != logo) {
	        		byte[] decodedImg=null;
					try {
						decodedImg = Base64.getDecoder().decode(logo);				
					} catch (IllegalArgumentException e) {
						res.setStatus(resourse.getString(FAILED));
						resDetails.setCode(resourse.getString("image.encoding.failed.code"));
						resDetails.setDescription(resourse.getString("image.encoding.failed"));
						res.setMessage(resDetails);
						res.setData(null);
						logger.error("MerchantServiceImpl Service ERROR addMerchant | " +merchantReq.toString()+
			        			" | message=" + e.getMessage());
						return res;
					}
					merchantReq.setLogo(decodedImg);
	        	}
				QRMerchant qrm= merchantRepository.save(merchantReq);
				qrm.setMerchantId(getMCMId(qrm.getId()));
				//String memid =getMCMId(qrm.getId());
			
				//updating generated merchant id 
				merchantRepository.save(qrm);
				res.setStatus(resourse.getString("success"));
				resDetails.setCode(resourse.getString("add.merchant.success.code"));
				resDetails.setDescription(resourse.getString("add.merchant.success"));
				res.setMessage(resDetails);
				ResponseData resData = new ResponseData(qrm.getMerchantId());
				json = new Gson().toJson(resData);
				res.setData(json);
			}
			else {
				res.setStatus(resourse.getString(FAILED));
				resDetails.setCode(resourse.getString("error.aliasExists.code"));
				resDetails.setDescription(resourse.getString("error.nameExists"));
				res.setMessage(resDetails);
				res.setData(null);
			}		
		}catch(ConstraintViolationException e) {
			logger.error("MerchantServiceImpl Service ERROR addMerchant | " +merchantReq.toString()+
        			" | message=" + e.getMessage());
        	res.setStatus(resourse.getString(FAILED));
        	resDetails.setCode(resourse.getString("error.ConstraintViolation.code"));
        	resDetails.setDescription(resourse.getString("error.ConstraintViolation"));
			res.setMessage(resDetails);
			res.setData(null);
			//return res;
        }catch(javax.validation.ConstraintViolationException e) {
        	logger.error("MerchantServiceImpl Service ERROR addMerchant | " +merchantReq.toString()+
        			" | message=" + e.getMessage());
        	em.flush();
        }catch(Exception e) {
        	logger.error("MerchantServiceImpl Service ERROR addMerchant | " +merchantReq.toString()+
        			" | message=" + e.getMessage());
			res.setStatus(resourse.getString(FAILED));
			resDetails.setCode(resourse.getString("add.merchant.failed.code"));
			resDetails.setDescription(resourse.getString("add.merchant.failed"));
			res.setMessage(resDetails);
			res.setData(null);
		}
		long endTime = System.currentTimeMillis();
        logger.info("MerchantController API Service ENDED onBoardMerchan | " +merchantReq.toString()+
        		" | status="+res.getStatus()+" | message="+res.getMessage()+" | avg_resp=" + (endTime - startTime));
		return res;
	}
	
	@Override
	public ResponseObject getMerchants() {
		long startTime = System.currentTimeMillis();
		logger.info("MerchantServiceImpl STARTED getMerchants");
		ResponseObject res = new ResponseObject();
		ResponseDetails resDetails = new ResponseDetails();
		List<QRMerchant> merchants;
		String json="";
		try {
			merchants = merchantRepository.getAll();
			json = new Gson().toJson(merchants );
		
			res.setStatus(resourse.getString("success"));
			resDetails.setCode(resourse.getString("merchants.retreived.success.code"));
			resDetails.setDescription(resourse.getString("merchants.retreived.success"));
			res.setMessage(resDetails);
			res.setData(json);
		}catch(Exception e) {
        	logger.error("MerchantServiceImpl Service ERROR getMerchants"+e);
			res.setStatus(resourse.getString(FAILED));
			resDetails.setCode(resourse.getString("merchants.retreived.failed.code"));
			resDetails.setDescription(resourse.getString("merchants.retreived.failed"));
			res.setMessage(resDetails);
			res.setData(null);
		}
		long endTime = System.currentTimeMillis();
        logger.info("MerchantController API Service ENDED getMerchants"+
        		" | status="+res.getStatus()+" | message="+res.getMessage()+" | avg_resp=" + (endTime - startTime));
		return res;
	}
	
	@Override
	public ResponseObject getMerchantDetails(String Name) {
		long startTime = System.currentTimeMillis();
		logger.info("MerchantServiceImpl STARTED getMerchantDetails Name="+Name);
		ResponseObject res = new ResponseObject();
		ResponseDetails resDetails = new ResponseDetails();
		QRMerchant merchant;
		String json="";
		try {
			merchant = merchantRepository.checkName(Name.toUpperCase());
			if(merchant != null){
				json = new Gson().toJson(merchant);
				logger.info("-------------------"+json);
			
				res.setStatus(resourse.getString("success"));
				resDetails.setCode(resourse.getString("merchant.details.retreived.success.code"));
				resDetails.setDescription(resourse.getString("merchant.details.retreived.success"));
				res.setMessage(resDetails);
				res.setData(json);
			}else{
				logger.error("MerchantServiceImpl Service ERROR getMerchantDetails Name="+Name);
				res.setStatus(resourse.getString(FAILED));
				resDetails.setCode(resourse.getString("merchant.invalid.input.code"));
				resDetails.setDescription(resourse.getString("merchant.invalid.name"));
				res.setMessage(resDetails);
				res.setData(null);
			}
		}catch(Exception e) {
        	logger.error("MerchantServiceImpl Service ERROR getMerchantDetails Name="+Name);
			res.setStatus(resourse.getString(FAILED));
			resDetails.setCode(resourse.getString("merchant.details.retreived.failed.code"));
			resDetails.setDescription(resourse.getString("merchant.details.retreived.failed"));
			res.setMessage(resDetails);
			res.setData(null);
		}
		long endTime = System.currentTimeMillis();
        logger.info("MerchantController API Service ENDED getMerchantDetails Name="+Name+
        		" | status="+res.getStatus()+" | message="+res.getMessage()+" | avg_resp=" + (endTime - startTime));
		return res;
	}
	
	@Transactional
	@Override
	public ResponseObject updateMerchant(QRMerchant merchantReq,String logo) {
		long startTime = System.currentTimeMillis();
		logger.info("MerchantServiceImpl STARTED updateMerchant | " +merchantReq.toString());
		ResponseObject res = new ResponseObject();
		ResponseDetails resDetails = new ResponseDetails();
		String json="";
		try {
			QRMerchant merchant = merchantRepository.checkName(merchantReq.getName().toUpperCase());
			if(null != merchant) {
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	        	Date date = new Date();
				//merchant.setRegName(merchantReq.getRegName());
				merchant.setAlias(merchantReq.getAlias().toUpperCase());
				merchant.setAddress(merchantReq.getAddress());
				merchant.setLocation(merchantReq.getLocation());
				merchant.setNicNo(merchantReq.getNicNo());
				merchant.setContactPerson(merchantReq.getContactPerson());
				merchant.setContactNumber(merchantReq.getContactNumber());
				merchant.setEmail(merchantReq.getEmail());
				merchant.setOther(merchantReq.getOther());
				merchant.setStatus(String.valueOf(MerchantStatus.valueOf(merchantReq.getStatus()).ordinal()));
				merchant.setLatitude(merchantReq.getLatitude());
				merchant.setLongitude(merchantReq.getLongitude());
				merchant.setModifiedDate(dateFormat.parse(dateFormat.format(date)));
				merchant.setModifiedBy(merchantReq.getModifiedBy());
				merchant.setModifiedReason(merchantReq.getModifiedReason());
				
				//Saving Image
	        	if(null != logo && logo!="") {
	        		byte[] decodedImg=null;
					try {
						decodedImg = Base64.getDecoder().decode(logo);				
					} catch (IllegalArgumentException e) {
						res.setStatus(resourse.getString(FAILED));
						resDetails.setCode(resourse.getString("image.encoding.failed.code"));
						resDetails.setDescription(resourse.getString("image.encoding.failed"));
						res.setMessage(resDetails);
						res.setData(null);
						logger.error("MerchantServiceImpl Service ERROR updateMerchant | " +merchantReq.toString()+
			        			" | message=" + e.getMessage());
						return res;
					}
					merchant.setLogo(decodedImg);
	        	}
	        	merchant= merchantRepository.save(merchant);
				res.setStatus(resourse.getString("success"));
				resDetails.setCode(resourse.getString("merchant.details.update.success.code"));
				resDetails.setDescription(resourse.getString("merchant.details.update.success"));
				res.setMessage(resDetails);
				ResponseData resData = new ResponseData(merchant.getMerchantId());
				json = new Gson().toJson(resData);
				res.setData(json);
			}
			else {
				res.setStatus(resourse.getString(FAILED));
				resDetails.setCode(resourse.getString("error.aliasExists.code"));
				resDetails.setDescription(resourse.getString("error.nameExists"));
				res.setMessage(resDetails);
				res.setData(null);
			}		
		}catch(ConstraintViolationException e) {
			logger.error("MerchantServiceImpl Service ERROR updateMerchant | " +merchantReq.toString()+
        			" | message=" + e.getMessage());
        	res.setStatus(resourse.getString(FAILED));
        	resDetails.setCode(resourse.getString("error.ConstraintViolation.code"));
        	resDetails.setDescription(resourse.getString("error.ConstraintViolation"));
			res.setMessage(resDetails);
			res.setData(null);
			//return res;
        }catch(javax.validation.ConstraintViolationException e) {
        	logger.error("MerchantServiceImpl Service ERROR updateMerchant | " +merchantReq.toString()+
        			" | message=" + e.getMessage());
			res.setStatus(resourse.getString(FAILED));
			resDetails.setCode(resourse.getString("error.ConstraintViolation.code"));
			resDetails.setDescription(resourse.getString("error.ConstraintViolation"));
			res.setMessage(resDetails);
			res.setData(null);
        	logger.error("MerchantServiceImpl Service ERROR updateMerchant | " +merchantReq.toString()+
        			" | message=" + e.getMessage());
        	em.flush();
        }catch(Exception e) {
        	logger.error("MerchantServiceImpl Service ERROR updateMerchant | " +merchantReq.toString()+
        			" | message=" + e.getMessage());
			res.setStatus(resourse.getString(FAILED));
			resDetails.setCode(resourse.getString("merchant.details.update.failed.code"));
			resDetails.setDescription(resourse.getString("merchant.details.update.failed"));
			res.setMessage(resDetails);
			res.setData(null);
		}
		long endTime = System.currentTimeMillis();
        logger.info("MerchantController API Service ENDED updateMerchant | " +merchantReq.toString()+
        		" | status="+res.getStatus()+" | message="+res.getMessage()+" | avg_resp=" + (endTime - startTime));
		return res;
	}
	
	public String getMCMId(long idVal) {
		return "MCM"+String.format("%06d", idVal);
    }

}
