package lk.dialog.ist.dcash.qrcode.controller.request;

import java.util.Date;

public class DynamicQRCodeRequest {

	private String  appCode;
	private String requestId;
	private String qrContent;
	private String qrType;
	private String expirationInSeconds;
	private Date requestTime;
	private String size;
	private String color;
	private boolean logoRequired;
	private boolean byteStreamRequired;
	
		
	public String getAppCode() {
		return appCode;
	}
	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getQrContent() {
		return qrContent;
	}
	public void setQrContent(String qrContent) {
		this.qrContent = qrContent;
	}
	public String getQrType() {
		return qrType;
	}
	public void setQrType(String qrType) {
		this.qrType = qrType;
	}
	public String getExpirationInSeconds() {
		return expirationInSeconds;
	}
	public void setExpirationInSeconds(String expirationInSeconds) {
		this.expirationInSeconds = expirationInSeconds;
	}
	public Date getRequestTime() {
		return requestTime;
	}
	public void setRequestTime(Date requestTime) {
		this.requestTime = requestTime;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public boolean isLogoRequired() {
		return logoRequired;
	}
	public void setLogoRequired(boolean logoRequired) {
		this.logoRequired = logoRequired;
	}
	public boolean isByteStreamRequired() {
		return byteStreamRequired;
	}
	public void setByteStreamRequired(boolean byteStreamRequired) {
		this.byteStreamRequired = byteStreamRequired;
	}
	
	
	
	
	
}
