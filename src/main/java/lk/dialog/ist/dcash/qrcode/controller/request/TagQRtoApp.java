package lk.dialog.ist.dcash.qrcode.controller.request;

public class TagQRtoApp {

	private String merchantID;
	private String qrCodeID;
	private String appCode;
	
	public String getMerchantID() {
		return merchantID;
	}
	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}
	public String getQrID() {
		return qrCodeID;
	}
	public void setQrID(String qrID) {
		this.qrCodeID = qrID;
	}
	public String getAppCode() {
		return appCode;
	}
	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}
	
	
}
