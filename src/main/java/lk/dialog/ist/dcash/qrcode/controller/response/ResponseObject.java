package lk.dialog.ist.dcash.qrcode.controller.response;

public class ResponseObject {

	 	private String status;
	    private ResponseDetails message; 
	    private String data;
	    public ResponseObject() {
			super();
		}
		public ResponseObject(String status, ResponseDetails message, String data) {
			super();
			this.status = status;
			this.message = message;
			this.data = data;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public ResponseDetails getMessage() {
			return message;
		}
		public void setMessage(ResponseDetails message) {
			this.message = message;
		}
		public String getData() {
			return data;
		}
		public void setData(String data) {
			this.data = data;
		}
		@Override
		public String toString() {
			return "ResponseObject [status=" + status + ", message=" + message.toString()
					+ ", data=" + data + "]";
		}
	
	
}
