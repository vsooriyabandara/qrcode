package lk.dialog.ist.dcash.qrcode.controller.response;

public class QueryQRCodes {

	long code_id;
	String qr_serial;
	String name;  
	String location;   
	String counter;   
	String other;  
	String qr_code_image;
	
	public QueryQRCodes() {
		super();
	}

	public QueryQRCodes(long id, String qr_content,String location, String counter,String name, String other) {
		this.code_id =id;
		this.qr_serial = qr_content;
		this.name = name;
		this.location = location;
		this.counter = counter;
		this.other = other;
		this.qr_code_image="/image/"+id;
	}
}
