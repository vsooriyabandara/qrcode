package lk.dialog.ist.dcash.qrcode.controller.response;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import lk.dialog.ist.dcash.qrcode.db.modal.QRCode;

public class BulkQRResponse {
	long ID;
	String qrCodeId;
	String system;
	String qrAlias; //customer ID
	String tagged;
	String createdDate;
	String createdBy;
	
	public BulkQRResponse() {
	}
	
	public BulkQRResponse(QRCode qr) {
		this.ID = qr.getId();
		this.qrAlias = qr.getAlias();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		this.createdDate = dateFormat.format(qr.getAddedDate());
		this.qrCodeId = qr.getQrId();
		this.createdBy = qr.getAddedBy();
		if(qr.getAppCode()==null){
			this.tagged="No";
		}else{
			this.tagged="Yes";
		}
	}

	public long getID() {
		return ID;
	}

	public void setID(long iD) {
		ID = iD;
	}

	public String getQrCodeId() {
		return qrCodeId;
	}

	public void setQrCodeId(String qrCodeId) {
		this.qrCodeId = qrCodeId;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getQrAlias() {
		return qrAlias;
	}

	public void setQrAlias(String qrAlias) {
		this.qrAlias = qrAlias;
	}

	public String getTagged() {
		return tagged;
	}

	public void setTagged(String tagged) {
		this.tagged = tagged;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
}
