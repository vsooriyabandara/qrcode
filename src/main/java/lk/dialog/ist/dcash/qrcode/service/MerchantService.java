package lk.dialog.ist.dcash.qrcode.service;

import lk.dialog.ist.dcash.qrcode.controller.response.Response;
import lk.dialog.ist.dcash.qrcode.controller.response.Response1;
import lk.dialog.ist.dcash.qrcode.controller.response.ResponseObject;
import lk.dialog.ist.dcash.qrcode.db.modal.QRMerchant;

public interface MerchantService {
	
	public ResponseObject addMerchant(QRMerchant merchant , String logo);

	public ResponseObject getMerchants();

	public ResponseObject getMerchantDetails(String Name);

	public ResponseObject updateMerchant(QRMerchant merchantReq, String logo);
	
}
