package lk.dialog.ist.dcash.qrcode.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lk.dialog.ist.dcash.qrcode.controller.request.BulkQrRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.DisableQrRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.GetQRRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.LoginRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.QRStatusRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.QrDetails;
import lk.dialog.ist.dcash.qrcode.controller.request.QueryQRCodesRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.Request;
import lk.dialog.ist.dcash.qrcode.controller.request.TagQRRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.TagQRtoApp;
import lk.dialog.ist.dcash.qrcode.controller.request.ValidateQRRequest;
import lk.dialog.ist.dcash.qrcode.controller.response.LoginValidationResponse;
import lk.dialog.ist.dcash.qrcode.controller.response.QueryQRCodesResponse;
import lk.dialog.ist.dcash.qrcode.controller.response.ResponseDetails;
import lk.dialog.ist.dcash.qrcode.controller.response.ResponseObject;
import lk.dialog.ist.dcash.qrcode.controller.response.TagQrResponse;
import lk.dialog.ist.dcash.qrcode.db.modal.QRCode;
import lk.dialog.ist.dcash.qrcode.db.repository.MerchantRepository;
import lk.dialog.ist.dcash.qrcode.db.repository.QRCodeRepository;
import lk.dialog.ist.dcash.qrcode.service.QRService;
import lk.dialog.ist.dcash.qrcode.service.impl.CustomBasicAuthenticationFilter;
import lk.dialog.ist.dcash.qrcode.util.InvalidArgumentException;
import lk.dialog.ist.dcash.qrcode.util.MerchantStatus;
import lk.dialog.ist.dcash.qrcode.util.QRType;

@RestController
@CrossOrigin(origins = "*")
public class QRController {

	@Autowired
	QRService qrService;

	@Autowired
	QRCodeRepository qrCodeRepository;

	@Autowired
	MerchantRepository merchantRepository;
	
	@Autowired
	CustomBasicAuthenticationFilter customBasicAuthenticationFilter;

	protected static ResourceBundle resourse = ResourceBundle.getBundle("data");

	private Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping(value = "/qrcode/createStaticQR", method = RequestMethod.POST)
	public ResponseEntity<ResponseObject> createStaticQR(@RequestBody Request request) {
		long startTime = System.currentTimeMillis();
		logger.info("QRController API Service STARTED createStaticQR | "
				+ request.toString());
		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseDetails resDetails = new ResponseDetails();
		ResponseObject res = new ResponseObject();
		try {
			int size = 0;
			if (request.getSize() == null || request.getSize() == "") {
				size = Integer.parseInt(resourse
						.getString("static.qr.default.size"));
			} else {
				try {
					size = Integer.parseInt(request.getSize());
				} catch (NumberFormatException e) {
					throw new Exception("QR_Size");
				}
			}
			if (request.getColor() == null || request.getColor() == "") {
				request.setColor(resourse.getString("static.qr.default.color"));
			}
			
			if((request.getQrType().equals(QRType.ALIPAY.toString()) && ((request.getStoreAddress()== null || request.getStoreId()==null || request.getStoreName()==null || request.getMerchantID()==null)))) {
					throw new RuntimeException("MISSING_MANDOTORY");
			}
			res = qrService.addStaticQR(request, size);
		} catch (Exception e) {
			logger.error("QRController API Service ERROR createStaticQR | "
					+ request.toString() + " | message=" + e.getMessage());
			res.setStatus(resourse.getString("failed"));

			switch (e.getMessage()) {
			case "Content":
				resDetails.setCode(resourse.getString("qr.content.empty.code"));
				resDetails.setDescription(resourse.getString("qr.content.empty"));
				break;
			case "QR_Size":
				resDetails.setCode(resourse.getString("qr.invalid.size.code"));
				resDetails.setDescription(resourse.getString("qr.invalid.size"));
				break;
			case "MISSING_MANDATORY":
				resDetails.setCode(resourse.getString("qr.missing.mandatory.code"));
				resDetails.setDescription(resourse.getString("qr.missing.mandatory.message"));
				break;
			default:
				resDetails.setCode(resourse.getString("add.qrcode.failed.code"));
				resDetails.setDescription(resourse.getString("add.qrcode.failed"));
				break;
			}
			res.setMessage(resDetails);
			res.setData(null);
		}
		responseEntity = ResponseEntity.status(HttpStatus.OK).body(res);
		long endTime = System.currentTimeMillis();
		logger.info("QRController API Service ENDED createStaticQR | "+ request.toString() + " | status=" + res.getStatus()	+ " | message=" + res.getMessage() + " | avg_resp="	+ (endTime - startTime));
		return responseEntity;
	}

	@RequestMapping(value = "/qrcode/getQrCodesForMerchant", method = RequestMethod.POST)
	public ResponseEntity<ResponseObject> getQrCodesForMerchant(
			@RequestBody GetQRRequest request) {
		long startTime = System.currentTimeMillis();
		logger.info("QRController API Service STARTED getQrCodesForMerchant | "
				+ request.toString());
		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseDetails resDetails = new ResponseDetails();
		ResponseObject res = new ResponseObject();
		try {
			if (request.getMerchantName() == null
					|| request.getMerchantName().equals("")) {
				throw new Exception("Merchant_ID");
			} else if (request.getStatus() == null
					|| request.getStatus().equals("")) {
				throw new Exception("Status");
			} else {
				if (request.getStatus().equalsIgnoreCase(
						MerchantStatus.ACTIVE.name())
						|| request.getStatus().equalsIgnoreCase(
								MerchantStatus.INACTIVE.name())
						|| request.getStatus().equalsIgnoreCase("ALL")) {
				} else {
					throw new Exception("Status");
				}
				res = qrService.getQRCodes(request);
			}
		} catch (Exception e) {
			logger.error("QRController API Service ERROR getQrCodesForMerchant | "
					+ request.toString() + " | message=" + e.getMessage());
			res.setStatus(resourse.getString("failed"));
			switch (e.getMessage()) {
			case "Merchant_ID":
				resDetails.setCode(resourse.getString("error.InvalidMerchantID.code"));
				resDetails.setDescription(resourse.getString("error.InvalidMerchantID"));
				break;
			case "Status":
				resDetails.setCode(resourse.getString("qr.invalid.status.code"));
				resDetails.setDescription(resourse.getString("qr.invalid.status"));
				break;
			default:
				resDetails.setCode(resourse.getString("load.qrcode.failed.code"));
				resDetails.setDescription(resourse.getString("load.qrcode.failed"));
				break;
			}
			res.setMessage(resDetails);
			res.setData(null);
		}
		responseEntity = ResponseEntity.status(HttpStatus.OK).body(res);
		long endTime = System.currentTimeMillis();
		logger.info("QRController API Service ENDED getQrCodesForMerchant | "
				+ request.toString() + " | status=" + res.getStatus()
				+ " | message=" + res.getMessage() + " | avg_resp="
				+ (endTime - startTime));
		return responseEntity;
	}
	
	@RequestMapping(value = "/qrcode/getQrCodesByDate", method = RequestMethod.POST)
	public ResponseEntity<ResponseObject> getQrCodesByDate(
			@RequestBody QrDetails request) {
		long startTime = System.currentTimeMillis();
		logger.info("QRController API Service STARTED getQrCodesByDate | "
				+ request.toString());
		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseDetails resDetails = new ResponseDetails();
		ResponseObject res = new ResponseObject();
		try {
			if (request.getMerchantName() == null
					|| request.getMerchantName().equals("")) {
				throw new Exception("Merchant_Name");
			} else {
				res = qrService.getQRCodesByDate(request);
			}
		} catch (Exception e) {
			logger.error("QRController API Service ERROR getQrCodesByDate | "
					+ request.toString() + " | message=" + e.getMessage());
			res.setStatus(resourse.getString("failed"));
			switch (e.getMessage()) {
			case "Merchant_Name":
				resDetails.setCode(resourse.getString("merchant.invalid.input.code"));
				resDetails.setDescription(resourse.getString("merchant.invalid.name"));
				break;
			default:
				resDetails.setCode(resourse.getString("load.qrcode.failed.code"));
				resDetails.setDescription(resourse.getString("load.qrcode.failed"));
				break;
			}
			res.setMessage(resDetails);
			res.setData(null);
		}
		responseEntity = ResponseEntity.status(HttpStatus.OK).body(res);
		long endTime = System.currentTimeMillis();
		logger.info("QRController API Service ENDED getQrCodesByDate | "
				+ request.toString() + " | status=" + res.getStatus()
				+ " | message=" + res.getMessage() + " | avg_resp="
				+ (endTime - startTime));
		return responseEntity;
	}

	@RequestMapping(value = "/qrcode/validateQR", method = RequestMethod.POST)
	public ResponseEntity<ResponseObject> validateQR(
			@RequestBody ValidateQRRequest request) {
		long startTime = System.currentTimeMillis();
		logger.info("QRController API Service STARTED validateQR | "
				+ request.toString() + " | QR_id=" + request.getQrCodeID());
		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseDetails resDetails = new ResponseDetails();
		ResponseObject res = new ResponseObject();
		try {
			if (request.getMerchantID() == null
					|| request.getMerchantID() == "") {
				throw new Exception("Merchant_ID");
			} else if (request.getQrCodeID() == null
					|| request.getQrCodeID() == "") {
				throw new Exception("QR_ID");
			} else {
				res = qrService.getQRContent(request);
			}
		} catch (Exception e) {
			logger.error("QRController API Service ERROR validateQR | "
					+ request.toString() + " | message=" + e.getMessage());
			res.setStatus(resourse.getString("failed"));
			switch (e.getMessage()) {
			case "Merchant_ID":
				resDetails.setCode(resourse.getString("error.InvalidMerchantID.code"));
				resDetails.setDescription(resourse.getString("error.InvalidMerchantID"));
				break;
			case "QR_ID":
				resDetails.setCode(resourse.getString("qr.invalid.qr.id.code"));
				resDetails.setDescription(resourse.getString("qr.invalid.qr.id"));
				break;
			default:
				resDetails.setCode(resourse.getString("qr.code.retrieve.failed.code"));
				resDetails.setDescription(resourse.getString("qr.code.retrieve.failed"));
				break;
			}
			res.setMessage(resDetails);
			res.setData(null);
		}

		responseEntity = ResponseEntity.status(HttpStatus.OK).body(res);
		long endTime = System.currentTimeMillis();
		logger.info("QRController API Service ENDED validateQR | "
				+ request.toString() + " | status=" + res.getStatus()
				+ " | message=" + res.getMessage() + " | avg_resp="
				+ (endTime - startTime));
		return responseEntity;
	}

	@RequestMapping(value = "/qrcode/tagQRtoApp", method = RequestMethod.POST)
	public ResponseEntity<ResponseObject> tagQRtoApp(@RequestBody TagQRtoApp request) {
		long startTime = System.currentTimeMillis();
		logger.info("QRController API Service STARTED tagQRtoApp | "
				+ request.toString() + " | app_code=" + request.getAppCode());
		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseDetails resDetails = new ResponseDetails();
		ResponseObject res = new ResponseObject();
		try {
			if (request.getMerchantID() == null
					|| request.getMerchantID() == "") {
				throw new Exception("Merchant_ID");
			} else if (request.getQrID() == null || request.getQrID() == "") {
				throw new Exception("QR_ID");
			} else if (request.getAppCode() == null
					|| request.getAppCode() == "") {
				throw new Exception("AppCode");
			} else {
				res = qrService.tagQRToApp(request);
			}
		} catch (Exception e) {
			logger.error("QRController API Service ERROR tagQRtoApp | "
					+ request.toString() + " | message=" + e.getMessage());
			res.setStatus(resourse.getString("failed"));
			switch (e.getMessage()) {
			case "Merchant_ID":
				resDetails.setCode(resourse.getString("error.InvalidMerchantID.code"));
				resDetails.setDescription(resourse.getString("error.InvalidMerchantID"));
				break;
			case "QR_ID":
				resDetails.setCode(resourse.getString("qr.invalid.qr.id.code"));
				resDetails.setDescription(resourse.getString("qr.invalid.qr.id"));
				break;
			case "AppCode":
				resDetails.setCode(resourse.getString("app.invalid.appcode.code"));
				resDetails.setDescription(resourse.getString("app.invalid.appcode"));
				break;
			default:
				resDetails.setCode(resourse.getString("app.tag.to.qr.failed.code"));
				resDetails.setDescription(resourse.getString("app.tag.to.qr.failed"));
				break;
			}
			res.setMessage(resDetails);
			res.setData(null);
		}

		responseEntity = ResponseEntity.status(HttpStatus.OK).body(res);
		long endTime = System.currentTimeMillis();
		logger.info("QRController API Service ENDED tagQRtoApp | "
				+ request.toString() + " | status=" + res.getStatus()
				+ " | message=" + res.getMessage() + " | avg_resp="
				+ (endTime - startTime));
		return responseEntity;
	}

	@RequestMapping(value = "/qrcode/disableQR", method = RequestMethod.POST)
	public ResponseEntity<ResponseObject> disableQR(
			@RequestBody DisableQrRequest request) {
		long startTime = System.currentTimeMillis();
		logger.info("QRController API Service STARTED disableQR | "
				+ request.toString());
		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseDetails resDetails = new ResponseDetails();
		ResponseObject res = new ResponseObject();
		try {
			if (request.getMerchantID() == null
					|| request.getMerchantID() == "") {
				throw new Exception("Merchant_ID");
			} else if (request.getQrID() == null || request.getQrID() == "") {
				throw new Exception("QR_ID");
			} else if (request.getReason() == null || request.getReason() == "") {
				throw new Exception("Reason");
			} else {
				res = qrService.disableQR(request);
			}
		} catch (Exception e) {
			logger.error("QRController API Service ERROR disableQR | "
					+ request.toString() + " | message=" + e.getMessage());
			res.setStatus(resourse.getString("failed"));
			switch (e.getMessage()) {
			case "Merchant_ID":
				resDetails.setCode(resourse.getString("error.InvalidMerchantID.code"));
				resDetails.setDescription(resourse.getString("error.InvalidMerchantID"));
				break;
			case "QR_ID":
				resDetails.setCode(resourse.getString("qr.invalid.qr.id.code"));
				resDetails.setDescription(resourse.getString("qr.invalid.qr.id"));
				break;
			case "Reason":
				resDetails.setCode(resourse.getString("qr.invalid.reason.code"));
				resDetails.setDescription(resourse.getString("qr.invalid.reason"));
				break;
			default:
				resDetails.setCode(resourse.getString("qr.status.update.failed.code"));
				resDetails.setDescription(resourse.getString("qr.status.update.failed"));
				break;
			}
			res.setMessage(resDetails);
			res.setData(null);
		}

		responseEntity = ResponseEntity.status(HttpStatus.OK).body(res);
		long endTime = System.currentTimeMillis();
		logger.info("QRController API Service ENDED disableQR | "
				+ request.toString() + " | status=" + res.getStatus()
				+ " | message=" + res.getMessage() + " | avg_resp="
				+ (endTime - startTime));
		return responseEntity;
	}

	@RequestMapping(value = "qrcode/createBulkStaticQR", method = RequestMethod.POST)
	public ResponseEntity<ResponseObject> createBulkStaticQR(
			@RequestBody BulkQrRequest request) {
		long startTime = System.currentTimeMillis();
		logger.info("QRController API Service STARTED createBulkStaticQR | "
				+ request.toString());
		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseDetails resDetails = new ResponseDetails();
		ResponseObject res = new ResponseObject();
		try {
			if (request.getNumberOfItems() == 0) {
				throw new Exception("Num_Items");
			} else {
				res = qrService.createBulkStaticQR(request);
			}
			if(request.isLogoRequired() && request.getLogo()==null) {
				throw new InvalidArgumentException("Logo_Required");
			}
		} catch (Exception e) {
			logger.error("QRController API Service ERROR createBulkStaticQR | "
					+ request.toString() + " | message=" + e.getMessage());
			res.setStatus(resourse.getString("failed"));
			switch (e.getMessage()) {
			case "Num_Items":
				resDetails.setCode(resourse.getString("error.InvalidMerchantID.code"));
				resDetails.setDescription(resourse.getString("error.InvalidMerchantID"));
				break;
			case "Logo_Required":
				resDetails.setCode(resourse.getString("error.logoRequired.code"));
				resDetails.setDescription(resourse.getString("error.logoRequired."));
				break;
			default:
				resDetails.setCode(resourse.getString("qr.status.update.failed.code"));
				resDetails.setDescription(resourse.getString("qr.status.update.failed"));
				break;
			}
			res.setMessage(resDetails);
			res.setData(null);
		}
		responseEntity = ResponseEntity.status(HttpStatus.OK).body(res);
		long endTime = System.currentTimeMillis();
		logger.info("QRController API Service ENDED createBulkStaticQR | "
				+ request.toString() + " | status=" + res.getStatus()
				+ " | message=" + res.getMessage() + " | avg_resp="
				+ (endTime - startTime));
		return responseEntity;
	}
	
	@RequestMapping(value = "/qrcode/updateQRDetails", method = RequestMethod.POST)
	public ResponseEntity<ResponseObject> updateQRDetails(@RequestBody Request request) {
		long startTime = System.currentTimeMillis();
		logger.info("QRController API Service STARTED updateQRDetails | "
				+ request.toString());
		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseDetails resDetails = new ResponseDetails();
		ResponseObject res = new ResponseObject();
		try {
			if (request.getColor() == null || request.getColor() == "") {
				request.setColor(resourse.getString("static.qr.default.color"));
			}
			res = qrService.updateQRDetails(request);
		} catch (Exception e) {
			logger.error("QRController API Service ERROR updateQRDetails | "
					+ request.toString() + " | message=" + e.getMessage());
			res.setStatus(resourse.getString("failed"));

			switch (e.getMessage()) {
			default:
				resDetails.setCode(resourse.getString("staticqr.update.fail.code"));
				resDetails.setDescription(resourse.getString("staticqr.update.fail"));
				break;
			}
			res.setMessage(resDetails);
			res.setData(null);
		}
		responseEntity = ResponseEntity.status(HttpStatus.OK).body(res);
		long endTime = System.currentTimeMillis();
		logger.info("QRController API Service ENDED updateQRDetails | "
				+ request.toString() + " | status=" + res.getStatus()
				+ " | message=" + res.getMessage() + " | avg_resp="
				+ (endTime - startTime));
		return responseEntity;
	}

	@RequestMapping(value = "/image/{ref}", method = RequestMethod.GET)
	public ResponseEntity<?> viewQrImage(@PathVariable("ref") int ref) {
		long startTime = System.currentTimeMillis();
		logger.info("QRController API Service STARTED viewQrImage" + " | ref="
				+ ref);
		ResponseEntity<?> responseEntity = null;
		try {
			QRCode qr = qrCodeRepository.findOne((long) ref);
			byte[] imageBytes = qr.getQrImage();

			if (imageBytes != null) {
				responseEntity = ResponseEntity.ok()
						.lastModified(System.currentTimeMillis())
						.header("max-age", "86400")
						.header("Content-Type", "image/png").body(imageBytes);
			} else {
				responseEntity = ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body("");
			}

		} catch (Exception ex) {
			logger.error("QRController API Service ERROR viewQrImage"
					+ " | message=" + ex.getMessage());
			responseEntity = ResponseEntity.status(HttpStatus.NOT_FOUND).body(
					"");
		}
		long endTime = System.currentTimeMillis();
		logger.info("QRController API Service ENDED viewQrImage" + " | ref="
				+ ref + " | avg_resp=" + (endTime - startTime));
		return responseEntity;
	}

	public static byte[] bufferedImageToByteArray(BufferedImage image,
			String format) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(image, format, baos);
		return baos.toByteArray();
	}

	@RequestMapping(value = "/pdf/{ref}", method = RequestMethod.GET)
	public ResponseEntity<?> viewQrInfoPDF(@PathVariable("ref") String ref) {
		long startTime = System.currentTimeMillis();
		logger.info("QRController API Service STARTED viewQrInfoPDF"
				+ " | ref=" + ref);
		ResponseEntity<?> responseEntity = null;
		try {
			File file = new File("files\\" + ref + ".pdf");
			byte[] fileBytes = read(file);

			if (fileBytes != null) {
				responseEntity = ResponseEntity.ok()
						.lastModified(System.currentTimeMillis())
						.header("Content-Type", "application/pdf")
						.body(fileBytes);
			} else {
				responseEntity = ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body("");
			}

		} catch (Exception ex) {
			logger.error("QRController API Service ERROR viewQrInfoPDF"
					+ " | message=" + ex.getMessage());
			responseEntity = ResponseEntity.status(HttpStatus.NOT_FOUND).body(
					"");
		}
		long endTime = System.currentTimeMillis();
		logger.info("QRController API Service ENDED viewQrInfoPDF" + " | ref="
				+ ref + " | avg_resp=" + (endTime - startTime));
		return responseEntity;
	}
	
	@RequestMapping(value = "/qrcode/getExcelFile/{mID}/{from}/{to}", method = RequestMethod.GET)
	public ResponseEntity<?> getExcelFile(@PathVariable String mID,@PathVariable String from,@PathVariable String to) {
		long startTime = System.currentTimeMillis();
		logger.info("QRController API Service STARTED getExcelFile"
				+ " | ref=" );
		ResponseEntity<?> responseEntity = null;
		try {
			byte[] fileBytes = qrService.generateExcel(mID,from,to);

			if (fileBytes != null) {
				responseEntity = ResponseEntity.ok()
						.lastModified(System.currentTimeMillis())
						.header("Content-Type", "application/vnd.ms-excel")
						.header("Content-Disposition","attachment; filename="+mID+".xls")
						.body(fileBytes);
			} else {
				responseEntity = ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body("");
			}

		} catch (Exception ex) {
			logger.error("QRController API Service ERROR getExcelFile"
					+ " | message=" + ex.getMessage());
			responseEntity = ResponseEntity.status(HttpStatus.NOT_FOUND).body(
					"");
		}
		long endTime = System.currentTimeMillis();
		logger.info("QRController API Service ENDED getExcelFile" + " | ref="
				+ " | avg_resp=" + (endTime - startTime));
		return responseEntity;
	}
	
	@RequestMapping(value = "/qrcode/getQrCodesTagDetails", method = RequestMethod.POST)
	public ResponseEntity<ResponseObject> getQrCodesTagDetails(@RequestBody QrDetails request) {
		long startTime = System.currentTimeMillis();
		logger.info("QRController API Service STARTED getQrCodesTagDetails | "
				+ request.toString());
		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseDetails resDetails = new ResponseDetails();
		ResponseObject res = new ResponseObject();
		try {
			if (request.getMerchantName() == null
					|| request.getMerchantName().equals("")) {
				throw new Exception("Merchant_Name");
			} else {
				res = qrService.getQrCodesTagDetails(request);
			}
		} catch (Exception e) {
			logger.error("QRController API Service ERROR getQrCodesTagDetails | "
					+ request.toString() + " | message=" + e.getMessage());
			res.setStatus(resourse.getString("failed"));
			switch (e.getMessage()) {
			case "Merchant_Name":
				resDetails.setCode(resourse.getString("merchant.invalid.input.code"));
				resDetails.setDescription(resourse.getString("merchant.invalid.name"));
				break;
			default:
				resDetails.setCode(resourse.getString("load.qrcode.failed.code"));
				resDetails.setDescription(resourse.getString("load.qrcode.failed"));
				break;
			}
			res.setMessage(resDetails);
			res.setData(null);
		}
		responseEntity = ResponseEntity.status(HttpStatus.OK).body(res);
		long endTime = System.currentTimeMillis();
		logger.info("QRController API Service ENDED getQrCodesTagDetails | "
				+ request.toString() + " | status=" + res.getStatus()
				+ " | message=" + res.getMessage() + " | avg_resp="
				+ (endTime - startTime));
		return responseEntity;
	}
	
	@RequestMapping(value = "/qrcode/getBulkQRDetails", method = RequestMethod.POST)
	public ResponseEntity<ResponseObject> getBulkQRDetails(@RequestBody QrDetails request) {
		long startTime = System.currentTimeMillis();
		logger.info("QRController API Service STARTED getBulkQRDetails | "
				+ request.toString());
		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseDetails resDetails = new ResponseDetails();
		ResponseObject res = new ResponseObject();
		try {
			res = qrService.getBulkQRDetails(request);	
		} catch (Exception e) {
			logger.error("QRController API Service ERROR getBulkQRDetails | "
					+ request.toString() + " | message=" + e.getMessage());
			res.setStatus(resourse.getString("failed"));
			switch (e.getMessage()) {
			default:
				resDetails.setCode(resourse.getString("load.qrcode.failed.code"));
				resDetails.setDescription(resourse.getString("load.qrcode.failed"));
				break;
			}
			res.setMessage(resDetails);
			res.setData(null);
		}
		responseEntity = ResponseEntity.status(HttpStatus.OK).body(res);
		long endTime = System.currentTimeMillis();
		logger.info("QRController API Service ENDED getBulkQRDetails | "
				+ request.toString() + " | status=" + res.getStatus()
				+ " | message=" + res.getMessage() + " | avg_resp="
				+ (endTime - startTime));
		return responseEntity;
	}
	
	@RequestMapping(value = "/qrcode/getBulkQRExcelFile/{from}/{to}", method = RequestMethod.GET)
	public ResponseEntity<?> getBulkQRExcelFile(@PathVariable String from,@PathVariable String to) {
		long startTime = System.currentTimeMillis();
		logger.info("QRController API Service STARTED getExcelFile"
				+ " | ref=" );
		ResponseEntity<?> responseEntity = null;
		try {
			byte[] fileBytes = qrService.generateBulkQRExcelFile(from, to);

			if (fileBytes != null) {
				responseEntity = ResponseEntity.ok()
						.lastModified(System.currentTimeMillis())
						.header("Content-Type", "application/vnd.ms-excel")
						.header("Content-Disposition","attachment; filename="+from+"-"+to+".xls")
						.body(fileBytes);
			} else {
				responseEntity = ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body("");
			}

		} catch (Exception ex) {
			logger.error("QRController API Service ERROR getExcelFile"
					+ " | message=" + ex.getMessage());
			responseEntity = ResponseEntity.status(HttpStatus.NOT_FOUND).body(
					"");
		}
		long endTime = System.currentTimeMillis();
		logger.info("QRController API Service ENDED getExcelFile" + " | ref="
				+ " | avg_resp=" + (endTime - startTime));
		return responseEntity;
	}


	public byte[] read(File file) throws IOException {
		ByteArrayOutputStream ous = null;
		InputStream ios = null;
		try {
			byte[] buffer = new byte[4096];
			ous = new ByteArrayOutputStream();
			ios = new FileInputStream(file);
			int read = 0;
			while ((read = ios.read(buffer)) != -1) {
				ous.write(buffer, 0, read);
			}
		} finally {
			try {
				if (ous != null)
					ous.close();
			} catch (IOException e) {}
			try {
				if (ios != null)
					ios.close();
			} catch (IOException e) {}
		}
		return ous.toByteArray();
	}
	
	@RequestMapping(value = "/codes/query", method = RequestMethod.POST)
	public ResponseEntity<QueryQRCodesResponse> query(
			@RequestBody QueryQRCodesRequest req) {
		long startTime = System.currentTimeMillis();
		logger.info("QRController API Service STARTED query | "
				+ req.toString());
		ResponseEntity<QueryQRCodesResponse> responseEntity = null;
		try {
			QueryQRCodesResponse res = qrService.getQueryQRCodes(req);
			responseEntity = ResponseEntity.status(HttpStatus.OK).body(res);
		} catch (Exception ex) {
			logger.error("QRController API Service ERROR query" + " | message="
					+ ex.getMessage());
		}
		long endTime = System.currentTimeMillis();
		logger.info("QRController API Service ENDED query" + " | "
				+ req.toString() + " | avg_resp=" + (endTime - startTime));
		return responseEntity;
	}
	
	@RequestMapping(value = "/codes/tag", method = RequestMethod.POST)
	public ResponseEntity<TagQrResponse> tag(@RequestBody TagQRRequest req) {
		long startTime = System.currentTimeMillis();
		logger.info("QRController API Service STARTED tag | " + req.toString());
		ResponseEntity<TagQrResponse> responseEntity = null;
		try {
			TagQrResponse res = qrService.doTagQR(req);
			responseEntity = ResponseEntity.status(HttpStatus.OK).body(res);
		} catch (Exception ex) {
			logger.error("QRController API Service ERROR tag" + " | message="
					+ ex.getMessage());
		}
		long endTime = System.currentTimeMillis();
		logger.info("QRController API Service ENDED tag" + " | "
				+ req.toString() + " | avg_resp=" + (endTime - startTime));
		return responseEntity;
	}
	
	@RequestMapping(value = "/qrpdf/{ref}/{qrID}/{location}/{name}", method = RequestMethod.GET)
	public ResponseEntity<?> getQrPDF(@PathVariable("ref") int ref,@PathVariable("name") String name,@PathVariable("qrID") String qrID,@PathVariable("location") String location) {
		long startTime = System.currentTimeMillis();
		logger.info("QRController API Service STARTED viewQrImage" + " | ref="
				+ ref);
		ResponseEntity<?> responseEntity = null;
		try {
			QRCode qr = qrCodeRepository.findOne((long) ref);
			byte[] pdf = qrService.generateQrPDF(qr.getQrImage(),name,qrID,location);

			if (pdf != null) {
				responseEntity = ResponseEntity.ok()
						.lastModified(System.currentTimeMillis())
						.header("Content-Type", "application/pdf")
						.header("Content-Disposition","attachment; filename="+qrID+".pdf")
						.body(pdf);
			} else {
				responseEntity = ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body("");
			}

		} catch (Exception ex) {
			logger.error("QRController API Service ERROR viewQrImage"
					+ " | message=" + ex.getMessage());
			responseEntity = ResponseEntity.status(HttpStatus.NOT_FOUND).body(
					"");
		}
		long endTime = System.currentTimeMillis();
		logger.info("QRController API Service ENDED viewQrImage" + " | ref="
				+ ref + " | avg_resp=" + (endTime - startTime));
		return responseEntity;
	}

		@RequestMapping(value = "/qrcode/validateUserLogin", method = RequestMethod.POST)
    public ResponseEntity<LoginValidationResponse> validateLogin(@RequestBody LoginRequest request) {
		long startTime = System.currentTimeMillis();
		logger.info("validateUserLogin  API Service Started");
		ResponseEntity<LoginValidationResponse> responseEntity = null;
		Map<String, String> message = new HashMap<>();
    	LoginValidationResponse res = new LoginValidationResponse();
    	HttpHeaders responseHeaders = new HttpHeaders();
        try {
        	 ResourceBundle resourse = ResourceBundle.getBundle("data");
        	String storedUserName =  resourse.getString("qr.admin.userName");
        	String storedPassword =  resourse.getString("qr.admin.password");
        	if(storedUserName.equalsIgnoreCase(request.getUserName()) && storedPassword.equals(request.getPassword())) {
        		res.setStatus("valid");
        		message.put("code", "200");
    			message.put("description", "......");
    			res.setMessage(message);
    			responseHeaders.set("access_token", "123");
    			responseHeaders.setExpires(100000);
        	}
        	else {
        		 res.setStatus("invalid");
        		 message.put("code", "200");
     			message.put("description", "Invalid Username or Password");
     			res.setMessage(message);
        	}
		}catch(Exception e) {
			res.setStatus("invalid");
			message.put("code", "201");
			message.put("description", "Exception Occured.Try again later");
			res.setMessage(message);
			logger.error("validateUserLogin  API exception occured | appCode "+request.getUserName() ,e);
		}
        responseEntity = new ResponseEntity<>(res, responseHeaders, HttpStatus.OK);
        long endTime = System.currentTimeMillis();
        logger.info("validateUserLogin  API Service Ended | avg_resp= "+(endTime-startTime));
        return responseEntity;
    }
		
	@RequestMapping(value = "/qrcode/updateQRStatus", method = RequestMethod.POST)
	public ResponseEntity<ResponseObject> updateQRStatus(@RequestBody QRStatusRequest request) {
		long startTime = System.currentTimeMillis();
		logger.info("QRController API Service START method updateQRStatus. request = " + request.toString());
		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseDetails resDetails = new ResponseDetails();
		ResponseObject response = new ResponseObject();
		try {
			
			response = qrService.updateQRStatus(request);
		} catch (Exception e) {
			logger.error("QRController API Service ERROR updateQRStatus | " + request.toString() + " | message="
					+ e.getMessage());
			response.setStatus(resourse.getString("failed"));

			switch (e.getMessage()) {
			default:
				resDetails.setCode(resourse.getString("staticqr.status.update.success.code"));
				resDetails.setDescription(resourse.getString("staticqr.status.update.success"));
				break;
			}
			response.setMessage(resDetails);
			response.setData(null);
		}
		responseEntity = ResponseEntity.status(HttpStatus.OK).body(response);
		long endTime = System.currentTimeMillis();
		logger.info("QRController API Service END method updateQRStatus. request = " + request.toString() + " | status="
				+ response.getStatus() + " | message=" + response.getMessage() + " | avg_resp=" + (endTime - startTime));
		return responseEntity;
	}	
}
