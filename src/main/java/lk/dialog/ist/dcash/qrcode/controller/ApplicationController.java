package lk.dialog.ist.dcash.qrcode.controller;

import java.util.ResourceBundle;
import java.util.Set;

import javax.validation.ConstraintViolation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lk.dialog.ist.dcash.qrcode.controller.request.ApplicationRequest;
import lk.dialog.ist.dcash.qrcode.controller.response.ResponseDetails;
import lk.dialog.ist.dcash.qrcode.controller.response.ResponseObject;
import lk.dialog.ist.dcash.qrcode.service.ApplicationService;
import lk.dialog.ist.dcash.qrcode.util.InvalidArgumentException;

@RestController
@CrossOrigin(origins = "*")
public class ApplicationController {

	@Autowired
	ApplicationService appService;

	protected static ResourceBundle resource = ResourceBundle.getBundle("data");
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping(value = "/app/appRegistration", method = RequestMethod.POST)
	public ResponseEntity<ResponseObject> appRegistration(@RequestBody ApplicationRequest appRequest) {
		long startTime = System.currentTimeMillis();
		logger.info("ApplicationController API Service STARTED appRegistration | " +appRequest.toString());
		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseObject response = new ResponseObject();
		ResponseDetails resDetails = new ResponseDetails();

		try {
			if (appRequest.getAppName() == null || appRequest.getAppName().isEmpty()) {
				throw new InvalidArgumentException("App_Name");
			}else if(appRequest.getContactNo()==null || appRequest.getContactNo().isEmpty()) {
				throw new InvalidArgumentException("Contact_No");
			}else if(appRequest.getNotifyURL()==null || appRequest.getNotifyURL().isEmpty()) {
				throw new InvalidArgumentException("NotifyURL");
			}
			response = appService.registerApp(appRequest);			
		} catch (javax.validation.ConstraintViolationException e) {
			logger.error("ApplicationController API Service ERROR appRegistration | " +appRequest.toString()+
        			" | message=" + e.getMessage());
			response.setStatus(resource.getString("failed"));
			resDetails.setCode(resource.getString("app.reg.invalid.input.code"));
			StringBuilder inputErrors = new StringBuilder();
        	Set<ConstraintViolation<?>> list =e.getConstraintViolations();
        	for (ConstraintViolation<?> s : list) {
        		String condition =s.getPropertyPath().toString();
        		if(condition.equals("appName")) {
        			inputErrors.append((resource.getString("app.registration.invalidName"))+". ");
        		}else if(condition.equals("contactNo")) {
        			inputErrors.append((resource.getString("app.registration.invalidContactNo"))+". ");
        		}else if(condition.equals("email")) {
        			inputErrors.append((resource.getString("app.registration.invalid.email"))+". ");
        		}else if(condition.equals("description")) {
        			inputErrors.append((resource.getString("app.registration.invalid.description"))+". ");
        		}else if(condition.equals("notifyURL")) {
        			inputErrors.append((resource.getString("app.registration.invalid.notifyURL"))+". ");
        		}
        	}     
        	resDetails.setDescription(inputErrors.toString());
        	response.setMessage(resDetails);
			response.setData(null);
		} catch (Exception e) {
			logger.error("ApplicationController API Service ERROR appRegistration | " +appRequest.toString()+
        			" | message=" + e.getMessage());
			response.setStatus(resource.getString("failed"));
			switch (e.getMessage()) {
				case "App_Name":
					resDetails.setCode(resource.getString("app.reg.invalid.input.code"));
					resDetails.setDescription(resource.getString("app.registration.invalidName"));
					break;			
				case "Contact_No":
					resDetails.setCode(resource.getString("app.reg.invalid.input.code"));
					resDetails.setDescription(resource.getString("app.registration.invalidContactNo"));
					break;
				case "NotifyURL":
					resDetails.setCode(resource.getString("app.reg.invalid.input.code"));
					resDetails.setDescription(resource.getString("app.registration.invalid.notifyURL"));
					break;
	            default :
	            	resDetails.setCode(resource.getString("app.registration.failed.code"));
	            	resDetails.setDescription(resource.getString("app.registration.failed"));
	    			break;
			}		
			response.setMessage(resDetails);
			response.setData(null);
		}
		responseEntity = ResponseEntity.status(HttpStatus.OK).body(response);
		long endTime = System.currentTimeMillis();
        logger.info("ApplicationController API Service ENDED appRegistration | " +appRequest.toString()+
        		" | status="+response.getStatus()+" | message="+response.getMessage()+" | avg_resp=" + (endTime - startTime));
		return responseEntity;
	}
	
	@RequestMapping(value = "/app/getApplications", method = RequestMethod.POST)
	public ResponseEntity<ResponseObject> getApplications() {
		long startTime = System.currentTimeMillis();
		logger.info("ApplicationController API Service STARTED getApplications | " );
		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseObject response = new ResponseObject();
		ResponseDetails resDetails = new ResponseDetails();
		
		try {
			response = appService.getApplications();		
		} catch (javax.validation.ConstraintViolationException e) {
			logger.error("ApplicationController API Service ERROR getApplications | " +
        			" | message=" + e.getMessage());
			response.setStatus(resource.getString("failed"));
			resDetails.setCode(resource.getString("error.ConstraintViolation.code"));
			resDetails.setDescription(resource.getString("error.ConstraintViolation"));
        	response.setMessage(resDetails);
			response.setData(null);
		} catch (Exception e) {
			logger.error("ApplicationController API Service ERROR getApplications | "+
        			" | message=" + e.getMessage());
			response.setStatus(resource.getString("failed"));
			switch (e.getMessage()) {
	            default :
	            	resDetails.setCode(resource.getString("app.retreived.failed.code"));
	            	resDetails.setDescription(resource.getString("app.retreived.failed"));
	    			break;
			}		
			response.setMessage(resDetails);
			response.setData(null);
		}
		responseEntity = ResponseEntity.status(HttpStatus.OK).body(response);
		long endTime = System.currentTimeMillis();
        logger.info("ApplicationController API Service ENDED getApplications | "+
        		" | status="+response.getStatus()+" | message="+response.getMessage()+" | avg_resp=" + (endTime - startTime));
		return responseEntity;
	}
}
