package lk.dialog.ist.dcash.qrcode.db.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import lk.dialog.ist.dcash.qrcode.db.modal.QRApplication;

public interface ApplicationRepository extends CrudRepository<QRApplication, Long>{

	@Modifying
	@Query("update QRApplication set appCode = ?1 where id = ?2")
	void updateAppCode(String appcode, long id);
	
	@Query("select a from QRApplication a where lower(a.appName)=lower(?1)" )
	QRApplication checkAppName(String appName);
	
	
	@Query("select a from QRApplication a where upper(a.appCode) = upper(?1)" )
	QRApplication checkAppCode(String appCode);
	
	@Query("select a from QRApplication a order by a.id desc" )
	List<QRApplication> getApps();
	
	
}
