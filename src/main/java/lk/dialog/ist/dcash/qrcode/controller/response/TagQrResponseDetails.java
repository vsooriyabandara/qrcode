package lk.dialog.ist.dcash.qrcode.controller.response;

import java.util.ArrayList;

public class TagQrResponseDetails {
	boolean succes;
	String message;
	ArrayList<TagQRErrorResponse> errors;

	public TagQrResponseDetails() {
		super();
	}

	public TagQrResponseDetails(boolean succes, String message,
			ArrayList<TagQRErrorResponse> errors) {
		super();
		this.succes = succes;
		this.message = message;
		this.errors = errors;
	}

	public boolean getSucces() {
		return succes;
	}

	public void setSucces(boolean succes) {
		this.succes = succes;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ArrayList<TagQRErrorResponse> getErrors() {
		return errors;
	}

	public void setErrors(ArrayList<TagQRErrorResponse> errors) {
		this.errors = errors;
	}

}
