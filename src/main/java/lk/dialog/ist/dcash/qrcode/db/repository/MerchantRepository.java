package lk.dialog.ist.dcash.qrcode.db.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import lk.dialog.ist.dcash.qrcode.db.modal.QRMerchant;

public interface MerchantRepository extends CrudRepository<QRMerchant, Long>{

	@Modifying
	@Query("update QRMerchant set merchantId = ?1 where id = ?2")
	void updateMemID(String firstname, long id);
	
	@Query("select m from QRMerchant m where m.merchantId = ?1")
	QRMerchant getMerchant(String merchantId);
	
	@Query("select m from QRMerchant m where UPPER(m.name) = ?1")
	QRMerchant getMerchantbyName(String merchantName);

	@Query("select m from QRMerchant m where UPPER(m.name) = ?1")
	QRMerchant checkName(String name);
	
	@Query("select m.name from QRMerchant m order by m.name")
	List<QRMerchant> getAll();
}
