package lk.dialog.ist.dcash.qrcode.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;

import lk.dialog.ist.dcash.qrcode.controller.request.ApplicationRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.QrDetails;
import lk.dialog.ist.dcash.qrcode.controller.response.BulkQRResponse;
import lk.dialog.ist.dcash.qrcode.controller.response.Response;
import lk.dialog.ist.dcash.qrcode.controller.response.Response1;
import lk.dialog.ist.dcash.qrcode.controller.response.ResponseData;
import lk.dialog.ist.dcash.qrcode.controller.response.ResponseDetails;
import lk.dialog.ist.dcash.qrcode.controller.response.ResponseObject;
import lk.dialog.ist.dcash.qrcode.db.modal.QRApplication;
import lk.dialog.ist.dcash.qrcode.db.modal.QRCode;
import lk.dialog.ist.dcash.qrcode.db.repository.ApplicationRepository;
import lk.dialog.ist.dcash.qrcode.service.ApplicationService;

@Service
public class ApplicationServiceImpl implements ApplicationService{

	@Autowired
	ApplicationRepository appRepository;
	
	@PersistenceContext
	private EntityManager em;
	
	protected static ResourceBundle resourse = ResourceBundle.getBundle("data");
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	private static final String FAILED="failed";
	
	@Transactional
	@Override
	public ResponseObject registerApp(ApplicationRequest appRequest)  {
		long startTime = System.currentTimeMillis();
		logger.info("ApplicationServiceImpl Service STARTED registerApp"+ " | app_name=" +appRequest.getAppName());
		ResponseObject res = new ResponseObject();
		ResponseDetails resDetails = new ResponseDetails();
		String json="";
		
		try {
			QRApplication qrApp =appRepository.checkAppName(appRequest.getAppName());
			QRApplication application = null;
			if(null == qrApp) {
				application = new QRApplication(appRequest);
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	        	Date date = new Date();
	        	application.setAddedDate(dateFormat.parse(dateFormat.format(date)));
	        	
	        	//Saving Image
	        	if(null != appRequest.getLogo()) {
	        		byte[] decodedImg=null;
					try {
						decodedImg = Base64.getDecoder().decode(appRequest.getLogo());				
					} catch (IllegalArgumentException e) {
						res.setStatus(resourse.getString(FAILED));
						resDetails.setCode(resourse.getString("image.encoding.failed.code"));
						resDetails.setDescription(resourse.getString("image.encoding.failed"));
						res.setData(null);
						logger.error("ApplicationServiceImpl Service ERROR registerApp" + " | app_name=" +appRequest.getAppName()+
			        			" | message=" + e.getMessage());
						return res;
					}
					application.setLogo(decodedImg);
	        	}
	        	application.setAppCode(appRequest.getAppName().toLowerCase());
				QRApplication app= appRepository.save(application);
				//app.setAppCode(getAppId(app.getId()));
				//String appCode = getAppId(app.getId());
				//appRepository.save(app);
				res.setStatus(resourse.getString("success"));
				resDetails.setCode(resourse.getString("app.registration.success.code"));
				resDetails.setDescription(resourse.getString("app.registration.success"));
				res.setMessage(resDetails);
				ResponseData resData = new ResponseData(app.getAppCode(),app.getAppName());
				json = new Gson().toJson(resData);
				res.setData(json);
			}
			else {
				res.setStatus(resourse.getString(FAILED));
				resDetails.setCode(resourse.getString("app.exists.code"));
				resDetails.setDescription(resourse.getString("app.exists"));
				res.setMessage(resDetails);
				res.setData(null);
			}
		}catch(ConstraintViolationException e) {
			logger.error("ApplicationServiceImpl Service ERROR registerApp" + " | app_name=" +appRequest.getAppName()+
        			" | message=" + e.getMessage());
        	res.setStatus(resourse.getString(FAILED));
        	resDetails.setCode(resourse.getString("error.ConstraintViolation.code"));
        	resDetails.setDescription(resourse.getString("error.ConstraintViolation"));
			res.setMessage(resDetails);
			res.setData(null);
        }catch(javax.validation.ConstraintViolationException e) {
        	logger.error("ApplicationServiceImpl Service ERROR registerApp" + " | app_name=" +appRequest.getAppName()+
        			" | message=" + e.getMessage());
        	em.flush();
        }catch(Exception e) {
        	logger.error("ApplicationServiceImpl Service ERROR registerApp" + " | app_name=" +appRequest.getAppName()+
        			" | message=" + e.getMessage());
			res.setStatus(resourse.getString(FAILED));
			resDetails.setCode(resourse.getString("app.registration.failed.code"));
			resDetails.setDescription(resourse.getString("app.registration.failed"));
			res.setMessage(resDetails);
			res.setData(null);
		}
		long endTime = System.currentTimeMillis();
        logger.info("ApplicationServiceImpl Service ENDED registerApp" + " | app_name=" +appRequest.getAppName()+
        		" | status="+res.getStatus()+" | message="+res.getMessage()+" | avg_resp=" + (endTime - startTime));
		return res;
	}
	
	@Override
	public ResponseObject getApplications() {
		long startTime = System.currentTimeMillis();
		logger.info("ApplicationServiceImpl Service STARTED getApplications | ");
		ResponseObject res = new ResponseObject();
		ResponseDetails resDetails = new ResponseDetails();
		String json = "";
		try {
			List<QRApplication> appList = null;

			appList = appRepository.getApps();

			json = new Gson().toJson(appList);
			res.setData(json);
			
			res.setStatus(resourse.getString("success"));
			resDetails.setCode(resourse.getString("app.retreived.success.code"));
			resDetails.setDescription(resourse.getString("app.retreived.success"));
			res.setMessage(resDetails);

			long endTime = System.currentTimeMillis();
			logger.info("ApplicationServiceImpl Service ENDED getApplications | "
					+ " | status=" + res.getStatus()
					+ " | message=" + res.getMessage() + " | avg_resp="
					+ (endTime - startTime));
			return res;
		} catch (Exception e) {
			logger.error("ApplicationServiceImpl Service ERROR getApplications | "
					+ " | message=" + e.getMessage());
			res.setStatus(resourse.getString(FAILED));
			resDetails.setCode(resourse.getString("app.retreived.failed.code"));
			resDetails.setDescription(resourse.getString("app.retreived.failed"));
			res.setMessage(resDetails);
			res.setData(null);
			return res;
		}
	}
	
	public String getAppId(long idVal) {
    	return "APP"+String.format("%06d", idVal);
    }
}
