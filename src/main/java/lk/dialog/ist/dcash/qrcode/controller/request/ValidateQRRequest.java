package lk.dialog.ist.dcash.qrcode.controller.request;

public class ValidateQRRequest {

	private String merchantID=null;
	private String qrCodeID = null;
	
	public String getMerchantID() {
		return merchantID;
	}
	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}
	public String getQrCodeID() {
		return qrCodeID;
	}
	public void setQrCodeID(String qrCodeID) {
		this.qrCodeID = qrCodeID;
	}
	@Override
	public String toString() {
		return "ValidateQRRequest [merchantID=" + merchantID + ", qrCodeID="
				+ qrCodeID + "]";
	}
	
	
}
