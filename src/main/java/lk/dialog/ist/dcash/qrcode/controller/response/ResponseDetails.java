package lk.dialog.ist.dcash.qrcode.controller.response;

public class ResponseDetails {
	private String Code;
	private String Description;
	
	public ResponseDetails() {
		super();
	}
	public ResponseDetails(String code, String description) {
		super();
		Code = code;
		Description = description;
	}
	public String getCode() {
		return Code;
	}
	public void setCode(String code) {
		Code = code;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	@Override
	public String toString() {
		return "ResponseDetails [Code=" + Code + ", Description=" + Description
				+ "]";
	}
	
	

}
