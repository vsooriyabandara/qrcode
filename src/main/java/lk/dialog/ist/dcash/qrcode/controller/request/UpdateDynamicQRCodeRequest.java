package lk.dialog.ist.dcash.qrcode.controller.request;

import java.util.Date;

/**
 * This request is used to update existing dyanmic QR Code
 * @author Veranga Sooriyabandara
 * @since 14 -11 -2017
 * @version 1.0
 * *
 */
public class UpdateDynamicQRCodeRequest {

	private String qrCode;
	private String qrContent;
	private Date  requestTime;
	private String qrSize;
	private String color;
	private boolean logoRequired;
	
	public String getQrCode() {
		return qrCode;
	}
	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}
	public String getQrContent() {
		return qrContent;
	}
	public void setQrContent(String qrContent) {
		this.qrContent = qrContent;
	}
	public Date getRequestTime() {
		return requestTime;
	}
	public void setRequestTime(Date requestTime) {
		this.requestTime = requestTime;
	}
	public String getQrSize() {
		return qrSize;
	}
	public void setQrSize(String qrSize) {
		this.qrSize = qrSize;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public boolean isLogoRequired() {
		return logoRequired;
	}
	public void setLogoRequired(boolean logoRequired) {
		this.logoRequired = logoRequired;
	}
	
	
	
	
	 
	 
}
