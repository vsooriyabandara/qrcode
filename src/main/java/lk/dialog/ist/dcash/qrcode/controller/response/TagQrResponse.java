package lk.dialog.ist.dcash.qrcode.controller.response;

public class TagQrResponse {
	String result;
	
	public TagQrResponse() {
		super();
	}

	public TagQrResponse(String result) {
		super();
		this.result = result;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
	
	
}
