package lk.dialog.ist.dcash.qrcode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
@EnableAutoConfiguration
public class SmartQRCodeApplication  extends SpringBootServletInitializer {

    /*public static void main(String[] args) {
        System.setProperty("user.timezone","+5:30");
        SpringApplication.run(SmartQRCodeApplication.class, args);
    }*/
	
	 @Override
	    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	        return application.sources(SmartQRCodeApplication.class);
	    }
	 
	    public static void main(String[] args) throws Exception {
	        SpringApplication.run(SmartQRCodeApplication.class, args);
	    }
}
