package lk.dialog.ist.dcash.qrcode.util;


import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

/**
 * Created by Harsha on 29/10/2017.
 */
@org.springframework.stereotype.Component
public class QrImageProcessor3 {
    private static final Logger log = LoggerFactory.getLogger(QrImageProcessor3.class);
    private Map<AppCode, String> imageNameMap = new HashMap<AppCode, String>(){{
        put(AppCode.BOOST, "static/boost.png");
        put(AppCode.EZCASH, "static/ezcash.png");
    }};

    public String getBase64EncodedQRImage(String qrText, String bitmap,String colorName,int height,int width) throws WriterException {
        log.info("getBase64EncodedQRImage service initiated");
        String imageName;
        //Color color = Color.getColor(colorName.toLowerCase());
        Color color = Color.RED;
        
        String fileType = "png";
        String imageString = null;
        try {
            Map<EncodeHintType, Object> hintMap = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");
            hintMap.put(EncodeHintType.MARGIN, 1); /* default = 4 */
            hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix byteMatrix = qrCodeWriter.encode(qrText, BarcodeFormat.QR_CODE, width,
            		height, hintMap);
            int CrunchifyWidth = byteMatrix.getWidth();
            int CrunchifyHeight = byteMatrix.getHeight();
            BufferedImage image = new BufferedImage(CrunchifyWidth, CrunchifyHeight,
                    BufferedImage.TYPE_INT_RGB);
            image.createGraphics();
            //ClassLoader classLoader = getClass().getClassLoader();
            //log.info("IMAGE NAME "+imageName);
           // log.info(classLoader.getResource(imageName).toString());
            //File file = new File(classLoader.getResource(imageName).getFile());
            //BitMap bitmap = bitmap;
            byte[] decodedImg = Base64.getDecoder().decode(bitmap);      
            
            BufferedImage logoImage = ImageIO.read(new ByteArrayInputStream(decodedImg));
            //BufferedImage logoImage = ImageIO.read(classLoader.getResource(imageName).openStream());
            log.info("FILE READ SUCCCESS..");
            // Calculate the delta height and width between QR code and logo
            int deltaHeight = image.getHeight() - logoImage.getHeight();
            int deltaWidth = image.getWidth() - logoImage.getWidth();
            // Initialize combined image
            BufferedImage combined = new BufferedImage(image.getHeight(), image.getWidth(), BufferedImage.TYPE_INT_ARGB);
            Graphics2D graphics = (Graphics2D) combined.getGraphics();
            graphics.setColor(Color.WHITE);
            graphics.fillRect(0, 0, CrunchifyWidth, CrunchifyHeight);
            graphics.setColor(color);
            for (int i = 0; i < CrunchifyWidth; i++) {
                for (int j = 0; j < CrunchifyHeight; j++) {
                    if (byteMatrix.get(i, j)) {
                        graphics.fillRect(i, j, 1, 1);
                    }
                }
            }
            graphics.drawImage(logoImage, Math.round(deltaWidth / 2), Math.round(deltaHeight / 2), null);
            final ByteArrayOutputStream bos = new ByteArrayOutputStream();
            try {
                ImageIO.write(combined, fileType, bos);
                log.info("FILE WRITE SUCCCESS..");
                imageString = Base64.getEncoder().encodeToString(bos.toByteArray());
            } catch (IOException e) {
                log.error(e.getMessage());
                e.printStackTrace();
            }
        } catch (WriterException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        log.info("getBase64EncodedQRImage, service completed");
        return imageString;
    }
}
