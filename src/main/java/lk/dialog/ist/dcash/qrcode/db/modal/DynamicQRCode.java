package lk.dialog.ist.dcash.qrcode.db.modal;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import lk.dialog.ist.dcash.qrcode.controller.request.DynamicQRCodeRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.GenerateDynamicQRRequest;
import lk.dialog.ist.dcash.qrcode.util.QRStatus;
/**
 * This Entity Bean used to Dynamic QR
 * Code Generation
 * @author Veranga Sooriyabandara
 * @since  10-11-2017
 * @version 1.0
 * @jira task : 
 *
 */
@Entity
@Audited
public class DynamicQRCode {
	
	@Id
	//for MySQL @GeneratedValue(strategy = GenerationType.AUTO, generator = "dynamic_qr_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dynamic_qr_seq")
    @SequenceGenerator(name = "dynamic_qr_seq", sequenceName = "dynamic_qr_seq")
	private long id;
	
	@Column
	@NotNull
	private String appCode;
	
	@Column
	@NotNull
	private String requestId;
	
	@Column
	@NotNull
	private String qrContent;
	
	@Column
	private String qrType;
	
	@Column
	private int expirationPeriodInSeconds;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date requestTime;
	
	@Column
	private String generatedQRCode;
	
	@Lob
    private byte[] qrImage;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date addedTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date expiryTIme;
	
	@Column
	private String txStatus;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAppCode() {
		return appCode;
	}

	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getQrContent() {
		return qrContent;
	}

	public void setQrContent(String qrContent) {
		this.qrContent = qrContent;
	}

	public String getQrType() {
		return qrType;
	}

	public void setQrType(String qrType) {
		this.qrType = qrType;
	}

	public int getExpirationPeriodInSeconds() {
		return expirationPeriodInSeconds;
	}

	public void setExpirationPeriodInSeconds(int expirationPeriodInSeconds) {
		this.expirationPeriodInSeconds = expirationPeriodInSeconds;
	}

	public Date getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(Date requestTime) {
		this.requestTime = requestTime;
	}
	public DynamicQRCode() {
		
	}
	public String getGeneratedQRCode() {
		return generatedQRCode;
	}
	public void setGeneratedQRCode(String generatedQRCode) {
		this.generatedQRCode = generatedQRCode;
	}
	
	public byte[] getQrImage() {
		return qrImage;
	}

	public void setQrImage(byte[] qrImage) {
		this.qrImage = qrImage;
	}
	

	public Date getAddedTime() {
		return addedTime;
	}

	public void setAddedTime(Date addedTime) {
		this.addedTime = addedTime;
	}

	public Date getExpiryTIme() {
		return expiryTIme;
	}

	public void setExpiryTIme(Date expiryTIme) {
		this.expiryTIme = expiryTIme;
	}
	
	public String getTxStatus() {
		return txStatus;
	}

	public void setTxStatus(String txStatus) {
		this.txStatus = txStatus;
	}

	public DynamicQRCode(DynamicQRCodeRequest request) {
		this.appCode = request.getAppCode();
		this.requestId = request.getRequestId();
		this.qrContent = request.getQrContent();
		this.qrType = request.getQrType();
		this.expirationPeriodInSeconds = Integer.parseInt(request.getExpirationInSeconds());
		this.requestTime = request.getRequestTime();
		this.addedTime = new Date(System.currentTimeMillis());
		this.expiryTIme = new Date(System.currentTimeMillis()+(Integer.parseInt(request.getExpirationInSeconds())*1000));
				
	}
	
	public DynamicQRCode(GenerateDynamicQRRequest request) {
		this.appCode = request.getAppCode();
		this.requestId = request.getRequestId();
		this.qrContent = request.getQrContent();
		this.qrType = request.getQrType();
		this.requestTime = request.getRequestTime();
		this.addedTime = new Date(System.currentTimeMillis());
				
	}
}
