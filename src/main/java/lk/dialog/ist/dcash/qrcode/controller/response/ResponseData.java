package lk.dialog.ist.dcash.qrcode.controller.response;

public class ResponseData {
	private String App_Code;
	private String App_Name;
	private String Merchant_ID;
	private String QR_Code_ID;
	private String URL;
	private String PDF_Url;
	private String QR_ID;
	private String QR_STATUS;
	private String expiryTime;
	private byte[] byteStream;
	
	public ResponseData() {
		super();
	}
	public ResponseData(String app_Code, String app_Name) {
		super();
		App_Code = app_Code;
		App_Name = app_Name;
	}
	
	public ResponseData(String merchantID) {
		super();
		Merchant_ID = merchantID;
	}
	
	public ResponseData(String qR_Code_ID, String uRL, String pDF_Url) {
		super();
		QR_Code_ID = qR_Code_ID;
		URL = uRL;
		PDF_Url = pDF_Url;
	}
	public ResponseData(String qR_category, String merchantId, String QRId, String QRStatus) {
		super();
		QR_Code_ID = qR_category;
		Merchant_ID = merchantId;
		QR_ID = QRId;
		QR_STATUS = QRStatus;
	}
	public String getApp_Code() {
		return App_Code;
	}
	public void setApp_Code(String app_Code) {
		App_Code = app_Code;
	}
	public String getApp_Name() {
		return App_Name;
	}
	public void setApp_Name(String app_Name) {
		App_Name = app_Name;
	}
	public String getMerchant_ID() {
		return Merchant_ID;
	}
	public void setMerchant_ID(String merchant_ID) {
		Merchant_ID = merchant_ID;
	}
	public String getQR_Code_ID() {
		return QR_Code_ID;
	}
	public void setQR_Code_ID(String qR_Code_ID) {
		QR_Code_ID = qR_Code_ID;
	}
	public String getURL() {
		return URL;
	}
	public void setURL(String uRL) {
		URL = uRL;
	}
	public String getPDF_Url() {
		return PDF_Url;
	}
	public void setPDF_Url(String pDF_Url) {
		PDF_Url = pDF_Url;
	}
	public String getQR_ID() {
		return QR_ID;
	}
	public void setQR_ID(String qR_ID) {
		QR_ID = qR_ID;
	}
	public String getQR_STATUS() {
		return QR_STATUS;
	}
	public void setQR_STATUS(String qR_STATUS) {
		QR_STATUS = qR_STATUS;
	}
	public String getExpiryTime() {
		return expiryTime;
	}
	public void setExpiryTime(String expiryTime) {
		this.expiryTime = expiryTime;
	}
	public byte[] getByteStream() {
		return byteStream;
	}
	public void setByteStream(byte[] byteStream) {
		this.byteStream = byteStream;
	}
	
	
}
