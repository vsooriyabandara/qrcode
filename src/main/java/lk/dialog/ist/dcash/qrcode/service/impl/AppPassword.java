package lk.dialog.ist.dcash.qrcode.service.impl;

import java.util.Random;
import java.util.ResourceBundle;

public class AppPassword {
	
	protected static ResourceBundle res = ResourceBundle.getBundle("data");
	
	private static final String CAPITAL = res.getString("app.password.generator.capital.letters");
	private static final String SMALL = res.getString("app.password.generator.small.letterschars");
	private static final String NUMBERS = res.getString("app.password.generator.NUMBERS");
	private static final String SYMBOLS = res.getString("app.password.generator.SYMBOLS");
	private static final int LENGTH = Integer.parseInt(res.getString("app.password.generator.password.LENGTH"));
	
    public String generatedPassword (boolean isCaps,boolean isSmall , boolean isSymbol , boolean isNumber) {
    	
    	String fullText =""; 
    	StringBuilder generatedPassword =new StringBuilder();
    	Random rndm = new Random();
    	if(isCaps && isSmall  && isNumber && isSymbol) {
    		fullText =CAPITAL +SMALL+NUMBERS+SYMBOLS ;
    	}
    	else if(isCaps && isSmall  && isNumber) {
    		fullText =CAPITAL +SMALL+NUMBERS+SYMBOLS ;
    	}
    	else if(isSmall  && isNumber && isSymbol) {
    		fullText =SMALL+NUMBERS+SYMBOLS ;
    	}
    	else if( isNumber && isSymbol && isCaps) {
    		fullText =NUMBERS+SYMBOLS+CAPITAL ;
    	}
    	else if(isCaps && isSmall) {
    		fullText =CAPITAL +SMALL;
    	}
    	else if(isSmall  && isNumber) {
    		fullText =SMALL +NUMBERS;
    	}
    	else if(isNumber && isSymbol) {
    		fullText =NUMBERS+SYMBOLS;
    	}
    	else if(isSymbol && isCaps) {
    		fullText =SYMBOLS+CAPITAL;
    	}
    	else if(isNumber && isCaps) {
    		fullText =NUMBERS+CAPITAL;
    	}
    	else if(isCaps) {
    		fullText = CAPITAL;
    	}
    	else if(isSmall) {
    		fullText = SMALL;
    	}
    	else if(isSymbol) {
    		fullText = SYMBOLS;
    	}
    	else if(isNumber) {
    		fullText = NUMBERS;
    	}
    	else {
    		fullText = SMALL;
    	}
    	
    	for(int i=0;i<LENGTH;i++) {
    		generatedPassword.append(fullText.charAt(rndm.nextInt(fullText.length()-1)));
    		
    	}
    	
    	
    	return generatedPassword.toString();
    }

    
}
	

