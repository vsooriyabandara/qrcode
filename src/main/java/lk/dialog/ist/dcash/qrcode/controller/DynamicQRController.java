package lk.dialog.ist.dcash.qrcode.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import lk.dialog.ist.dcash.qrcode.controller.request.AcknowledgeDynamicQRCodeRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.DynamicQRCodeRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.GenerateDynamicQRRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.LoginRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.UpdateDynamicQRCodeRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.ValidateDynamicQRCodeRequest;
import lk.dialog.ist.dcash.qrcode.controller.response.AcknowledgeDynamicQRCodeResponse;
import lk.dialog.ist.dcash.qrcode.controller.response.DynamicQRResponse;
import lk.dialog.ist.dcash.qrcode.controller.response.LoginValidationResponse;
import lk.dialog.ist.dcash.qrcode.controller.response.ResponseDetails;
import lk.dialog.ist.dcash.qrcode.controller.response.ResponseObject;
import lk.dialog.ist.dcash.qrcode.controller.response.ValidateDynamicQRResponse;
import lk.dialog.ist.dcash.qrcode.db.modal.DynamicQRCode;
import lk.dialog.ist.dcash.qrcode.db.repository.DynamicQRCodeRepository;
import lk.dialog.ist.dcash.qrcode.service.DynamicQRService;
import lk.dialog.ist.dcash.qrcode.util.QRType;
/**
 * Controller class related to the Dynamic QR Code
 * @author Veranga Sooriyabandara
 * @since 14-11-2017
 * @version 1.0
 * jira id :
 * 
 *
 */
@RestController
@CrossOrigin(origins="*")
public class DynamicQRController {
	
	@Autowired 
	private DynamicQRCodeRepository dynamicQRCodeRepository;
	
	@Autowired
	private DynamicQRService dynamicQRService;
	
	private final static String code="code";
	private final static String description="description";
	private final static String failed="failed";
	private final static String qr_dynamic_dynamicQR_addFailed_code="qr.dynamic.dynamicQR.addFailed.code";
	private final static String qr_dynamic_dynamicQR_addFailed_info="qr.dynamic.dynamicQR.addFailed.info";
	private static final String ERRORPREFIX="DynamicQRServiceImpl | acknowledegeDynamicQRCode |";
	private static final String EMPTY_QR_CONTENT="EMPTY_QR_CONTENT";
	private static final String EMPTY_APP_CODE="EMPTY_APP_CODE";
	private static final String REQUEST_TIME_EMPTY="REQUEST_TIME_EMPTY";
	
	static final String DESCRIPTION="description";
	
	private static final String CODE="code";
	
	private static final String FAILED="failed";
	
	//@Autowired
	private ResourceBundle resourse = ResourceBundle.getBundle("data");
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@RequestMapping(value = "/qrcode/generateDynamicQRID", method = RequestMethod.POST)
    public ResponseEntity<ResponseObject> generateDynamicQRID(@RequestBody DynamicQRCodeRequest request) {
		
		long startTime = System.currentTimeMillis();
		logger.info("createDynamicQR  API Service Started");
		
		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseDetails resDetails = new ResponseDetails();
		ResponseObject res = new ResponseObject();
        try {
        	if (request.getAppCode() == null || request.getAppCode().trim().isEmpty()) {
				logger.info(ERRORPREFIX+"  requestId " + request.getRequestId() + " | Error Message EMPTY_APP_CODE");
				throw new NullPointerException(EMPTY_APP_CODE);
			} else if (request.getRequestId() == null || request.getRequestId().isEmpty()) {
				logger.info("DynamicQRServiceImpl | addDynamicQRCodes |  requestId " + request.getRequestId() + " | Error Message EMPTY_REQUEST_ID");
				throw new NullPointerException("EMPTY_REQUEST_ID");
			} else if (request.getQrContent() != null && (request.getRequestId().length() > Integer.parseInt(resourse.getString("qr.dynamic.dynamicQR.requestIdMaxLength")) || (request.getRequestId().length() < Integer.parseInt(resourse.getString("qr.dynamic.dynamicQR.requestIdMinLength"))))) {
				logger.info("DynamicQRServiceImpl | addDynamicQRCodes |  requestId " + request.getRequestId() + " | Error Message INVALIDE_REQUEST_ID");
				throw new NullPointerException("INVALIDE_REQUEST_ID");
			} else if (request.getQrContent() == null || request.getQrContent().trim().isEmpty()) {
				logger.info("DynamicQRServiceImpl | addDynamicQRCodes |  requestId " + request.getRequestId() + " | Error Message EMPTY_QR_CONTENT");
				throw new NullPointerException(EMPTY_QR_CONTENT);
			} else if (request.getQrContent() != null && request.getQrContent().length() > Integer.parseInt(resourse.getString("qr.dynamic.dynamicQR.qrContentLength"))) {
				logger.info("DynamicQRServiceImpl | addDynamicQRCodes |  requestId " + request.getRequestId() + " | Error Message QR_CONTENT_EXCEED");
				throw new NullPointerException("QR_CONTENT_EXCEED");
			} else if (request.getRequestTime() == null) {
				logger.info("DynamicQRServiceImpl | addDynamicQRCodes |  requestId " + request.getRequestId() + " | Error Message REQUEST_TIME_EMPTY");
				throw new NullPointerException(REQUEST_TIME_EMPTY);
			} 
			else if (request.getExpirationInSeconds() == null || request.getExpirationInSeconds().length() == 0) {
				logger.info("DynamicQRServiceImpl | addDynamicQRCodes |  requestId " + request.getRequestId() + " | Error Message EXPIRY_SECONDS_EMPTY");
				throw new NullPointerException("EXPIRY_SECONDS_EMPTY");
			}
        	res = dynamicQRService.generateDynamicQRID(request);
		}catch (Exception e) {
			switch (e.getMessage()) {
			case EMPTY_APP_CODE:
				res.setStatus(resourse.getString(FAILED));
				resDetails.setCode(resourse.getString("qr.dynamic.dynamicQR.emptyAppCode.code"));
				resDetails.setDescription(resourse.getString("qr.dynamic.dynamicQR.emptyAppCode.info"));
				break;
			case "EMPTY_REQUEST_ID":
				res.setStatus(resourse.getString(FAILED));
				resDetails.setCode(resourse.getString("qr.dynamic.dynamicQR.emptyRequestId.code"));
				resDetails.setDescription(resourse.getString("qr.dynamic.dynamicQR.emptyRequestId.info"));
				break;
			case "INVALIDE_REQUEST_ID":
				res.setStatus(resourse.getString(FAILED));
				resDetails.setCode(resourse.getString("qr.dynamic.dynamicQR.invalidRequestId.code"));
				resDetails.setDescription(resourse.getString("qr.dynamic.dynamicQR.invalidRequestId.info"));
				break;
			case EMPTY_QR_CONTENT:
				res.setStatus(resourse.getString(FAILED));
				resDetails.setCode(resourse.getString("qr.dynamic.dynamicQR.emptyQRContent.code"));
				resDetails.setDescription( resourse.getString("qr.dynamic.dynamicQR.emptyQRContent.info"));
				break;
			case "QR_CONTENT_EXCEED":
				res.setStatus(resourse.getString(FAILED));
				resDetails.setCode(resourse.getString("qr.dynamic.dynamicQR.invalidQRContent.code"));
				resDetails.setDescription(resourse.getString("qr.dynamic.dynamicQR.invalidQRContent.info"));
				break;
			case REQUEST_TIME_EMPTY:
				res.setStatus(resourse.getString(FAILED));
				resDetails.setCode(resourse.getString("qr.dynamic.dynamicQR.emptyRequestTime.code"));
				resDetails.setDescription(resourse.getString("qr.dynamic.dynamicQR.emptyRequestTime.info"));
				break;
			case "EMPTY_QR_SIZE":
				res.setStatus(resourse.getString(FAILED));
				resDetails.setCode(resourse.getString("qr.dynamic.dynamicQR.emptyQR.code"));
				resDetails.setDescription(resourse.getString("qr.dynamic.dynamicQR.emptyQR.info"));
				break;
			case "INVALID_EXPIRY_TIME":
				res.setStatus(resourse.getString(FAILED));
				resDetails.setCode(resourse.getString("qr.dynamic.dynamicQR.invalidExpriyTime.code"));
				resDetails.setDescription(resourse.getString("qr.dynamic.dynamicQR.invalidExpriyTime.info"));
				break;
			case "EXPIRY_SECONDS_EMPTY":
				res.setStatus(resourse.getString(FAILED));
				resDetails.setCode( resourse.getString("qr.dynamic.dynamicQR.emptyExpiryTime.code"));
				resDetails.setDescription(resourse.getString("qr.dynamic.dynamicQR.emptyExpiryTime.info"));
				break;
			case "INVALID_QR_TYPE":
				res.setStatus(resourse.getString(FAILED));
				StringBuilder sb = new StringBuilder();
				for (QRType s : QRType.values()) {
					sb.append(s + " ,");
				}
				resDetails.setCode(resourse.getString("qr.dynamic.dynamicQR.invalidQRType.code"));
				resDetails.setDescription(resourse.getString("qr.dynamic.dynamicQR.invalidQRType.info") + " " + sb.toString());
				break;
			default:
				res.setStatus(resourse.getString(failed));
				resDetails.setCode(resourse.getString(qr_dynamic_dynamicQR_addFailed_code));
				resDetails.setDescription(resourse.getString(qr_dynamic_dynamicQR_addFailed_info));
				res.setMessage(resDetails);
				logger.error("createDynamicQR  API exception occured | appCode "+request.getAppCode()+"|  qrContent "+request.getQrContent()+"|"+e.getMessage() ,e);
			}
			res.setMessage(resDetails);
		}
        responseEntity = ResponseEntity.status(HttpStatus.OK).body(res);
        long endTime = System.currentTimeMillis();
        logger.info("createDynamicQR  API Service Ended | avg_resp= "+(endTime-startTime));
        return responseEntity;
    }
	
	@RequestMapping(value = "/qrcode/generateDynamicQRString", method = RequestMethod.POST)
    public ResponseEntity<ResponseObject> generateDynamicQRString(@RequestBody GenerateDynamicQRRequest request) {
		
		long startTime = System.currentTimeMillis();
		logger.info("createDynamicQR  API Service Started | " + request.toString());
		ResponseEntity<ResponseObject> responseEntity = null;
		ResponseDetails resDetails = new ResponseDetails();
		ResponseObject res = new ResponseObject();
        try {
        	if (request.getAppCode() == null || request.getAppCode().trim().isEmpty()) {
				logger.info(ERRORPREFIX+"  requestId " + request.getRequestId() + " | Error Message EMPTY_APP_CODE");
				throw new NullPointerException(EMPTY_APP_CODE);
			} else if (request.getRequestId() == null || request.getRequestId().isEmpty()) {
				logger.info("DynamicQRServiceImpl | addDynamicQRCodes |  requestId " + request.getRequestId() + " | Error Message EMPTY_REQUEST_ID");
				throw new NullPointerException("EMPTY_REQUEST_ID");
			} else if (request.getQrContent() != null && (request.getRequestId().length() > Integer.parseInt(resourse.getString("qr.dynamic.dynamicQR.requestIdMaxLength")) || (request.getRequestId().length() < Integer.parseInt(resourse.getString("qr.dynamic.dynamicQR.requestIdMinLength"))))) {
				logger.info("DynamicQRServiceImpl | addDynamicQRCodes |  requestId " + request.getRequestId() + " | Error Message INVALIDE_REQUEST_ID");
				throw new NullPointerException("INVALIDE_REQUEST_ID");
			} else if (request.getQrContent() == null || request.getQrContent().trim().isEmpty()) {
				logger.info("DynamicQRServiceImpl | addDynamicQRCodes |  requestId " + request.getRequestId() + " | Error Message EMPTY_QR_CONTENT");
				throw new NullPointerException(EMPTY_QR_CONTENT);
			} else if (request.getQrContent() != null && request.getQrContent().length() > Integer.parseInt(resourse.getString("qr.dynamic.dynamicQR.qrContentLength"))) {
				logger.info("DynamicQRServiceImpl | addDynamicQRCodes |  requestId " + request.getRequestId() + " | Error Message QR_CONTENT_EXCEED");
				throw new NullPointerException("QR_CONTENT_EXCEED");
			}
        	res = dynamicQRService.generateDynamicQRString(request);
		}catch (Exception e) {
			res.setStatus(resourse.getString(FAILED));
			res.setData(e.getMessage());
			resDetails.setDescription(e.getMessage());
		}
        responseEntity = ResponseEntity.status(HttpStatus.OK).body(res);
        long endTime = System.currentTimeMillis();
        logger.info("createDynamicQR  API Service Ended | avg_resp= "+(endTime-startTime));
        return responseEntity;
        
    }

	
	@RequestMapping(value = "/qrcode/updateDynamicQR", method = RequestMethod.POST)
    public ResponseEntity<DynamicQRResponse> updateDynamicQR(@RequestBody UpdateDynamicQRCodeRequest request) {
		long startTime = System.currentTimeMillis();
		logger.info("updateDynamicQR  API Service Started");
		ResponseEntity<DynamicQRResponse> responseEntity = null;
		Map<String, String> message = new HashMap<>();
    	DynamicQRResponse res = new DynamicQRResponse();
        try {
        	res = dynamicQRService.updateExistingDynamicQRCode(request);
		}catch(Exception e) {
			res.setStatus(resourse.getString(failed));
			message.put(code, resourse.getString(qr_dynamic_dynamicQR_addFailed_code));
			message.put(description, resourse.getString(qr_dynamic_dynamicQR_addFailed_info));
			res.setMessage(message);
		}
        responseEntity = ResponseEntity.status(HttpStatus.OK).body(res);
        long endTime = System.currentTimeMillis();
        logger.info("updateDynamicQR  API Service Ended | avg_resp= "+(endTime-startTime));
        return responseEntity;
    }
	
	
	@RequestMapping(value = "/dynamicQR/{ref}", method = RequestMethod.GET)
	public ResponseEntity<?> viewQrImage(@PathVariable("ref") int ref) {
	    ResponseEntity<?> responseEntity = null;
	    try {           
	    	DynamicQRCode qr = dynamicQRCodeRepository.findOne((long) ref);
	        byte[] imageBytes = qr.getQrImage();
	        if (imageBytes != null) {
	            responseEntity = ResponseEntity.ok().lastModified(System.currentTimeMillis())
	                    .header("max-age", "86400").header("Content-Type", "image/png")
	                    .body(imageBytes);
	        } else {
	            
	        }
	    } catch (Exception ex) {
	       logger.error(ex.getMessage(),ex);;
	    }
	    return responseEntity;
	}
	
	@RequestMapping(value="/qrcode/validateDynamicQR" , method=RequestMethod.POST)
	public ResponseEntity<ValidateDynamicQRResponse> validateQRCode(@RequestBody ValidateDynamicQRCodeRequest request){
		long startTime = System.currentTimeMillis();
		logger.info("validateDynamicQR  API Service Started");
		ResponseEntity<ValidateDynamicQRResponse> responseEntity = null;
		Map<String, String> message = new HashMap<String, String>();
    	ValidateDynamicQRResponse res = new ValidateDynamicQRResponse();
        try {
        	res = dynamicQRService.validateDynamicQRCode(request);
		}catch(Exception e){
			res.setStatus(resourse.getString(failed));
			message.put(code, resourse.getString(qr_dynamic_dynamicQR_addFailed_code));
			message.put(description, resourse.getString(qr_dynamic_dynamicQR_addFailed_info));
			
		}
        responseEntity = ResponseEntity.status(HttpStatus.OK).body(res);
        long endTime = System.currentTimeMillis();
        logger.info("validateDynamicQR  API Service Ended | avg_resp= "+(endTime-startTime));
        return responseEntity;
	}
	
	@RequestMapping(value="/qrcode/acknowledgeQRCode" , method=RequestMethod.POST)
	public ResponseEntity<AcknowledgeDynamicQRCodeResponse> acknowledgeQRCode(@RequestBody AcknowledgeDynamicQRCodeRequest request){
		long startTime = System.currentTimeMillis();
		logger.info("acknowledgeQRCode  API Service Started");
		ResponseEntity<AcknowledgeDynamicQRCodeResponse> responseEntity = null;
		Map<String, String> message = new HashMap<>();
		AcknowledgeDynamicQRCodeResponse res = new AcknowledgeDynamicQRCodeResponse();
        try {
        	res = dynamicQRService.acknowledegeDynamicQRCode(request);
		}catch(Exception e){
			res.setStatus(resourse.getString(failed));
			message.put(code, resourse.getString(qr_dynamic_dynamicQR_addFailed_code));
			message.put(description, resourse.getString(qr_dynamic_dynamicQR_addFailed_info));
		}
        responseEntity = ResponseEntity.status(HttpStatus.OK).body(res);
        long endTime = System.currentTimeMillis();
        logger.info("acknowledgeQRCode  API Service Ended | avg_resp= "+(endTime-startTime));
        return responseEntity;
	}
	


}

