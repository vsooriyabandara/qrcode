/**
 *
 */
package lk.dialog.ist.dcash.qrcode.db.repository;

import lk.dialog.ist.dcash.qrcode.controller.response.QueryQRCodes;
import lk.dialog.ist.dcash.qrcode.db.modal.QRCode;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.validation.constraints.Size;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface QRCodeRepository extends CrudRepository<QRCode, Long> {

	@Query("select q from QRCode q where q.merchantId = ?1 and q.status=?2 order by q.id desc")
	List<QRCode> findByMemIdAndStatus(String memId,int status);
	
	@Query("select q from QRCode q where q.merchantId = ?1 order by q.id desc")
	List<QRCode> findByMemId(String memId);
	
	@Modifying
	@Query("update QRCode set qrId = ?1 where id = ?2")
	void updateQrID(String qrid, long id);
	
	@Query("select qr from QRCode qr where qr.merchantId = ?1 and lower(qr.qrContent)=lower(?2)" )
	List<QRCode> checkMerchantQRContent(String merchantID, String content);
	
	@Query("select qr from QRCode qr where qr.qrId = ?1 and qr.merchantId=?2" )
	QRCode checkQRId(String QRId, String merchantId);
	
	@Modifying
	@Query("update QRCode set appCode = ?1 where id = ?2")
	void updateAppCode(String appCode, long id);
	
	@Modifying
	@Query("update QRCode set status = ?1 where qrId = ?2 and merchantId =?3")
	void disableQRCode(int status, String qrID, String merchantID);
	
	@Modifying
	@Query("update QRCode set status = ?1 where qrId = ?2 and merchantId =?3")
	void enableQRCode(int status, String qrID, String merchantID);
	
	@Query("select qr from QRCode qr where qr.merchantId = ?1 and qr.addedDate between ?2 and ?3 order by qr.id desc" )
	List<QRCode> getQRCodesByDate(String merchantID, Date fromDate,Date toDate);
	
	@Query("select qr from QRCode qr where qr.merchantId ='-' and qr.addedDate between ?1 and ?2 order by qr.id desc" )
	List<QRCode> getBulkQRCodesByDate(Date fromDate,Date toDate);
	
	/*@Query("Select new lk.dialog.ist.dcash.qrcode.util.TestClass(qr.id,qr.qrId,qr.merchantId,qr.appCode,qr.qrContent,qr.alias,qr.locaton,qr.counter,m.name,m.other) from QRMerchant m,QRCode qr where m.merchantId=qr.merchantId and m.merchantId = ?1")
	public List<TestClass> testFind(String merchant);*/
	
	@Query("Select new lk.dialog.ist.dcash.qrcode.controller.response.QueryQRCodes(qr.id,qr.qrContent,qr.locaton,qr.counter,m.name,m.other) from QRMerchant m,QRCode qr where m.merchantId=qr.merchantId and  lower(qr.qrContent) like %:qrId% and  lower(m.name) like %:name% and  lower(qr.locaton) like %:location% and  lower(m.other) like %:other% and  lower(qr.counter) like %:counter% and to_char(qr.id) like %:id%")
	public List<QueryQRCodes> queryQRCodes(@Param("id") String id, @Param("qrId") String qrId,@Param("name") String name,@Param("location") String location,@Param("counter") String counter,@Param("other") String other);
	
	@Modifying
	@Query("update QRCode set appCode = ?1 where qrContent = ?2")
	int tagQRToApp(String appCode, String qrSerial);
	
	@Modifying
	@Query("update QRCode set appCode = ?1  where qrContent = ?2")
	int untagQRToApp(String appCode, String qrSerial);
	
	@Modifying
	@Query("update QRCode set appCode = ?1 where qrContent = ?2")
	int tagBulkQRToApp(String appCode, String qrSerial);
	
	@Modifying
	@Query("update QRCode set appCode = ?1,alias = ?2  where qrContent = ?3")
	int untagBulkQRToApp(String appCode,String alias, String qrSerial);
	
	@Modifying
	@Query("update QRCode set status = ?1,modifiedDate = ?2,modifiedBy = ?3, modifiedReason = ?4 where qrId = ?5 and merchantId =?6")
	void modifyQRCodeStatus(int status, Date modifiedDate, String modifiedBy, String modifiedReason, String qrID,
			String merchantID);

}
