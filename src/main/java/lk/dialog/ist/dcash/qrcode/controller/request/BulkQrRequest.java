package lk.dialog.ist.dcash.qrcode.controller.request;

public class BulkQrRequest {
	private int numberOfItems;
	private String color;
	private int size;
	private String addedBy;
	private boolean logoRequired;
	private String logo;
	
	
	public int getNumberOfItems() {
		return numberOfItems;
	}
	public void setNumberOfItems(int numberOfItems) {
		this.numberOfItems = numberOfItems;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public String getAddedBy() {
		return addedBy;
	}
	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}
	public boolean isLogoRequired() {
		return logoRequired;
	}
	public void setLogoRequired(boolean logoRequired) {
		this.logoRequired = logoRequired;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	@Override
	public String toString() {
		return "BulkQrRequest [numberOfItems=" + numberOfItems + ", color="
				+ color + ", size=" + size + ", addedBy=" + addedBy + "logoRequired"+logoRequired+"]";
	}
}
