package lk.dialog.ist.dcash.qrcode.db.modal;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

import lk.dialog.ist.dcash.qrcode.controller.request.ApplicationRequest;

@Entity
@Audited
public class QRApplication {
	
	@Id
	//for My Sql @GeneratedValue(strategy = GenerationType.AUTO, generator = "application_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "application_seq")
    @SequenceGenerator(name = "application_seq", sequenceName = "application_seq")
	long id;
	
	@Column
	private String appCode;
	
	@Column
	@NotNull
	@Size(min = 8,max=12)
	private String contactNo;
	
	@Column
	@Pattern(regexp = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}")
	private String email;
	
	@Column(unique=true)
	@NotNull
	@Size(max=100)
	private String appName;
	
	@Column
	@Size(max=200)
	private String description;
	
	@Column
	@NotNull
	@Size(max=500)
	private String notifyURL;
	
	@Lob
	private byte[] logo;
	
	@Column
	private String cpName;
	
	@Column
	private String addedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date addedDate;
	
	@Column
	private String modifiedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedDate;
	
	@Column
	private String modifiedReason;

	public QRApplication() {
	
	}
	
	public QRApplication(ApplicationRequest appRequest) {
		this.contactNo = appRequest.getContactNo();
		this.email = appRequest.getEmail();
		this.appName = appRequest.getAppName();
		this.description = appRequest.getDescription();
		this.notifyURL = appRequest.getNotifyURL();
		this.addedBy =appRequest.getAddedBy();
		this.cpName = appRequest.getCpName();
		//this.logo = appRequest.getLogo();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAppCode() {
		return appCode;
	}

	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNotifyURL() {
		return notifyURL;
	}

	public void setNotifyURL(String notifyURL) {
		this.notifyURL = notifyURL;
	}

	public byte[] getLogo() {
		return logo;
	}

	public void setLogo(byte[] logo) {
		this.logo = logo;
	}

	public String getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}

	public Date getAddedDate() {
		return addedDate;
	}

	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedReason() {
		return modifiedReason;
	}

	public void setModifiedReason(String modifiedReason) {
		this.modifiedReason = modifiedReason;
	}
	
}
