package lk.dialog.ist.dcash.qrcode.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;


@Configuration
@ComponentScan("lk.dialog.ist.dcash.qrcode.*")
public class ApplicationConfig {

    @Bean
    public ReloadableResourceBundleMessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames("classpath:messages");
        return messageSource;
    }
    
//    @Bean
//    public FilterChain mifeTokenManager() {
//    	SpringSecurityFilterChain mifeTokenMgr = new SpringSecurityFilterChain();
//    return mifeTokenMgr;
//    }
//    
    

   
}
