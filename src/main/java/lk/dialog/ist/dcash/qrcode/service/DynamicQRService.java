package lk.dialog.ist.dcash.qrcode.service;

import lk.dialog.ist.dcash.qrcode.controller.request.AcknowledgeDynamicQRCodeRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.DynamicQRCodeRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.GenerateDynamicQRRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.UpdateDynamicQRCodeRequest;
import lk.dialog.ist.dcash.qrcode.controller.request.ValidateDynamicQRCodeRequest;
import lk.dialog.ist.dcash.qrcode.controller.response.AcknowledgeDynamicQRCodeResponse;
import lk.dialog.ist.dcash.qrcode.controller.response.DynamicQRResponse;
import lk.dialog.ist.dcash.qrcode.controller.response.ResponseObject;
import lk.dialog.ist.dcash.qrcode.controller.response.ValidateDynamicQRResponse;

/**
 * Service Abstraction
 * @author Veranga Sooriyabandara
 * @since 10-11-2017
 * @version 1.0
 * Jira id :
 *
 */
public interface DynamicQRService {

	
	/**
	 * Used to get dynamic QR code response
	 * @param request
	 * @return
	 */

	public DynamicQRResponse updateExistingDynamicQRCode(UpdateDynamicQRCodeRequest request);
	
	public ValidateDynamicQRResponse validateDynamicQRCode(ValidateDynamicQRCodeRequest request);
	
	public AcknowledgeDynamicQRCodeResponse acknowledegeDynamicQRCode(AcknowledgeDynamicQRCodeRequest request);

	public ResponseObject generateDynamicQRID(DynamicQRCodeRequest request);

	public ResponseObject generateDynamicQRString(GenerateDynamicQRRequest request);

	
	
	
	
	
	
}
