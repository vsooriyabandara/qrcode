package lk.dialog.ist.dcash.qrcode.controller.response;

import java.util.Map;
/**
 * Dynamic QR Response 
 * @author Veranga Sooriyabandara
 * @since 17-11-2017
 * @version 1.0
 * Jira Id : QR-61
 */
public class ValidateDynamicQRResponse {
	private String status;
	private Map<String, String> message;
	private String qrContent;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Map<String, String> getMessage() {
		return message;
	}
	public void setMessage(Map<String, String> message) {
		this.message = message;
	}
	public String getQrContent() {
		return qrContent;
	}
	public void setQrContent(String qrContent) {
		this.qrContent = qrContent;
	} 
	
	
	
}
