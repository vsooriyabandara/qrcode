package lk.dialog.ist.dcash.qrcode.util;


import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.EnumMap;
import java.util.Map;
import javax.imageio.ImageIO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

/**
 * Created by Harsha on 29/10/2017.
 */
@org.springframework.stereotype.Component
public class QrImageProcessor2 {
    private static final Logger log = LoggerFactory.getLogger(QrImageProcessor2.class);

    public String getBase64EncodedQRImage(String qrText, byte[] logo,String colorName,int height,int width, boolean logoRequired) throws WriterException {
        log.info("getBase64EncodedQRImage service initiated");
        Color color = Color.decode(colorName);
        
        String fileType = "png";
        String imageString = null;
        try {
            Map<EncodeHintType, Object> hintMap = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");
            hintMap.put(EncodeHintType.MARGIN, 1); /* default = 4 */
            hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix byteMatrix = qrCodeWriter.encode(qrText, BarcodeFormat.QR_CODE, width,
            		height, hintMap);
            int CrunchifyWidth = byteMatrix.getWidth();
            int CrunchifyHeight = byteMatrix.getHeight();
            BufferedImage image = new BufferedImage(CrunchifyWidth, CrunchifyHeight,
                    BufferedImage.TYPE_INT_RGB);
            image.createGraphics();
            ClassLoader classLoader = getClass().getClassLoader();
            //log.info("IMAGE NAME "+logoName);
            //File file = new File(classLoader.getResource(imageName).getFile());
            //BitMap bitmap = bitmap;
            BufferedImage qrImage;
            ByteArrayInputStream bais = null;
           
            if(logoRequired) {
            	bais = new ByteArrayInputStream(logo);
                BufferedImage logoImage=null;
                try {
                	logoImage = ImageIO.read(bais);
                } catch (IOException e) {
                	log.info(""+e);
                }
                //BufferedImage logoImage = ImageIO.read(classLoader.getResource(imageName).openStream());
                log.info("FILE READ SUCCCESS..");
                // Calculate the delta height and width between QR code and logo
                int deltaHeight = image.getHeight() - logoImage.getHeight();
                int deltaWidth = image.getWidth() - logoImage.getWidth();
                // Initialize combined image
                qrImage = new BufferedImage(image.getHeight(), image.getWidth(), BufferedImage.TYPE_INT_ARGB);
                Graphics2D graphics = (Graphics2D) qrImage.getGraphics();
                graphics.setColor(Color.WHITE);
                graphics.fillRect(0, 0, CrunchifyWidth, CrunchifyHeight);
                graphics.setColor(color);
                for (int i = 0; i < CrunchifyWidth; i++) {
                    for (int j = 0; j < CrunchifyHeight; j++) {
                        if (byteMatrix.get(i, j)) {
                            graphics.fillRect(i, j, 1, 1);
                        }
                    }
                }
                graphics.drawImage(logoImage, Math.round(deltaWidth / 2), Math.round(deltaHeight / 2), null);
            }else {            
                qrImage = new BufferedImage(image.getHeight(), image.getWidth(), BufferedImage.TYPE_INT_ARGB);
                Graphics2D graphics = (Graphics2D) qrImage.getGraphics();
                graphics.setColor(Color.WHITE);
                graphics.fillRect(0, 0, CrunchifyWidth, CrunchifyHeight);
                graphics.setColor(color);
                for (int i = 0; i < CrunchifyWidth; i++) {
                    for (int j = 0; j < CrunchifyHeight; j++) {
                        if (byteMatrix.get(i, j)) {
                            graphics.fillRect(i, j, 1, 1);
                        }
                    }
                }
            }
            
            final ByteArrayOutputStream bos = new ByteArrayOutputStream();
            try {
                ImageIO.write(qrImage, fileType, bos);
                log.info("FILE WRITE SUCCCESS..");
                imageString = Base64.getEncoder().encodeToString(bos.toByteArray());
            } catch (IOException e) {
                log.error(e.getMessage());
                e.printStackTrace();
            }
        } catch (WriterException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        log.info("getBase64EncodedQRImage, service completed");
        return imageString;
    }
}
