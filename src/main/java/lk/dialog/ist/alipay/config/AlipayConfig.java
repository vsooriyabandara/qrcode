package lk.dialog.ist.alipay.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class AlipayConfig {

	public static String partner = null;
	public static String key = null;
	public static String log_path = null;
	public static String input_charset = null;
	public static String sign_type = null;
	public static String notify_url = null;
	public static String alipay_gateway_new_url = null;
	public static String currency_type = null;
	public static String country_code = null;

	private static final Logger log = Logger.getLogger(AlipayConfig.class.getName());

	static {
		try {
			Properties prop = new Properties();
			InputStream inputStream = AlipayConfig.class.getClassLoader()
					.getResourceAsStream("external_config.properties");
			try {
				prop.load(inputStream);
				partner = prop.getProperty("alipay.config.partner.id");
				key = prop.getProperty("alipay.config.key");
				log_path = prop.getProperty("alipay.config.log.path");
				input_charset = prop.getProperty("alipay.config.input.char.set");
				sign_type = prop.getProperty("alipay.config.sign.type");
				notify_url = prop.getProperty("alipay.config.notify.url");
				alipay_gateway_new_url = prop.getProperty("alipay.config.gateway.new.url");
				currency_type = prop.getProperty("alipay.config.gateway.currency.type");
				country_code = prop.getProperty("alipay.config.gateway.country.code");

				log.info("Utility Payment : AlipayConfig Initial Details : partner=" + partner + " | key=" + key
						+ " log_path=" + log_path + " | input_charset=" + input_charset + " | sign_type=" + sign_type
						+ " | notify_url=" + notify_url + " | alipay_gateway_new_url=" + alipay_gateway_new_url
						+ " | currency_type=" + currency_type + " | country_code=" + country_code);

			} catch (IOException e) {
				log.error(e.getMessage());
			}

		} catch (Exception ex) {
			log.error(ex.getMessage());
		}
	}

}
