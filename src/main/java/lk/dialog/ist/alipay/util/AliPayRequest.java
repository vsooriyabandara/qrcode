package lk.dialog.ist.alipay.util;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AliPayRequest {
	
	@JsonProperty("secondary_merchant_name")
	private String secondaryMerchantName;
	@JsonProperty("secondary_merchant_id")
	private String secondaryMerchantId;
	@JsonProperty("secondary_merchant_industry")
	private String secondaryMerchantIndustry;
	@JsonProperty("store_id")
	private String storeId;
	@JsonProperty("store_name")
	private String storeName;
	@JsonProperty("trans_currency")
	private String transCurrency;
	@JsonProperty("currency")
	private String currency;
	@JsonProperty("country_code")
	private String countryCode;
	@JsonProperty("address")
	private String address;
	
	public String getSecondaryMerchantName() {
		return secondaryMerchantName;
	}
	public void setSecondaryMerchantName(String secondaryMerchantName) {
		this.secondaryMerchantName = secondaryMerchantName;
	}
	public String getSecondaryMerchantId() {
		return secondaryMerchantId;
	}
	public void setSecondaryMerchantId(String secondaryMerchantId) {
		this.secondaryMerchantId = secondaryMerchantId;
	}
	public String getSecondaryMerchantIndustry() {
		return secondaryMerchantIndustry;
	}
	public void setSecondaryMerchantIndustry(String secondaryMerchantIndustry) {
		this.secondaryMerchantIndustry = secondaryMerchantIndustry;
	}
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public String getTransCurrency() {
		return transCurrency;
	}
	public void setTransCurrency(String transCurrency) {
		this.transCurrency = transCurrency;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	

	
	
	
	
	
	
	
	
	

}
