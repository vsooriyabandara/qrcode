package lk.dialog.ist.alipay.util.httpClient;

public enum HttpResultType {
    
    STRING,
    BYTES
}
