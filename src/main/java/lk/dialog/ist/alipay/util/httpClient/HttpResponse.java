package lk.dialog.ist.alipay.util.httpClient;

import org.apache.commons.httpclient.Header;

import lk.dialog.ist.alipay.config.AlipayConfig;

import java.io.UnsupportedEncodingException;

/* *
 *ç±»å��ï¼šHttpResponse
 *åŠŸèƒ½ï¼šHttpè¿”å›žå¯¹è±¡çš„å°�è£…
 *è¯¦ç»†ï¼šå°�è£…Httpè¿”å›žä¿¡æ�¯
 *ç‰ˆæœ¬ï¼š3.3
 *æ—¥æœŸï¼š2011-08-17
 *è¯´æ˜Žï¼š
 *ä»¥ä¸‹ä»£ç �å�ªæ˜¯ä¸ºäº†æ–¹ä¾¿å•†æˆ·æµ‹è¯•è€Œæ��ä¾›çš„æ ·ä¾‹ä»£ç �ï¼Œå•†æˆ·å�¯ä»¥æ ¹æ�®è‡ªå·±ç½‘ç«™çš„éœ€è¦�ï¼ŒæŒ‰ç…§æŠ€æœ¯æ–‡æ¡£ç¼–å†™,å¹¶é�žä¸€å®šè¦�ä½¿ç”¨è¯¥ä»£ç �ã€‚
 *è¯¥ä»£ç �ä»…ä¾›å­¦ä¹ å’Œç ”ç©¶æ”¯ä»˜å®�æŽ¥å�£ä½¿ç”¨ï¼Œå�ªæ˜¯æ��ä¾›ä¸€ä¸ªå�‚è€ƒã€‚
 */

public class HttpResponse {

    /**
     * è¿”å›žä¸­çš„Headerä¿¡æ�¯
     */
    private Header[] responseHeaders;

    /**
     * Stringç±»åž‹çš„result
     */
    private String   stringResult;

    /**
     * btyeç±»åž‹çš„result
     */
    private byte[]   byteResult;

    public Header[] getResponseHeaders() {
        return responseHeaders;
    }

    public void setResponseHeaders(Header[] responseHeaders) {
        this.responseHeaders = responseHeaders;
    }

    public byte[] getByteResult() {
        if (byteResult != null) {
            return byteResult;
        }
        if (stringResult != null) {
            return stringResult.getBytes();
        }
        return null;
    }

    public void setByteResult(byte[] byteResult) {
        this.byteResult = byteResult;
    }

    public String getStringResult() throws UnsupportedEncodingException {
        if (stringResult != null) {
            return stringResult;
        }
        if (byteResult != null) {
            return new String(byteResult, AlipayConfig.input_charset);
        }
        return null;
    }

    public void setStringResult(String stringResult) {
        this.stringResult = stringResult;
    }

}
